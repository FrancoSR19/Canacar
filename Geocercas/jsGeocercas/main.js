var map;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "luciad/view/PaintRepresentation",
    
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    
    "./painters/paisPainter",
    "./painters/CelularesPainter",
    "./DefaultBalloon",
    
    "recursos/js/Shapes",
    "./painters/historialPainter",
    "./painters/GeocercasPainter",
    
    "dojo/request",
    "luciad/util/Promise",
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on,PaintRepresentation,
        DojoMap, ReferenceProvider, LayerConfigUtil, paisPainter, CelularesPainter, DefaultBalloon,
        Shapes, historialPainter, GeocercasPainter, request, Promise) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    
    //      LAYERS
    var celulares,radioCelular,  geoCercas, mexico, historial;
    var trayectorias = [];
    var timer = 0;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "AerialWithLabels"});
        } 
        CrearCapas();
    }
    
    function CrearCapas() {
        
        radioCelular = LayerFactory.createMemoryLayer(referenceC, {label:"Radio Celular", selectable: true, editable: true, painter: new CelularesPainter()});
        map.layerTree.addChild(radioCelular);
        radioCelular.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        
        celulares = LayerFactory.createMemoryLayer(referenceC, {label:"Celulares", selectable: true, editable: true, painter: new CelularesPainter()});
        map.layerTree.addChild(celulares);
        celulares.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        celulares.balloonContentProvider = function (features) {
            return DefaultBalloon.getBallon(features);
        };
        
        geoCercas = LayerFactory.createMemoryLayer(referenceC, {label: "Geo Cercas", selectable: true, editable: true, painter: new GeocercasPainter()});
        map.layerTree.addChild(geoCercas);
        geoCercas.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        geoCercas.balloonContentProvider = function (features) {
            return DefaultBalloon.getBallon(features);
        };
        historial = LayerFactory.createMemoryLayer(referenceC, {label: "Historial", selectable: true, editable: false, painter: new historialPainter()});
        map.layerTree.addChild(historial);
                
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        mexico = LayerFactory.createFeatureLayer(referenceC, {label: "Mexico", selectable: false, painter: new paisPainter(), editable: false}, "../recursos/estados.json");
        map.layerTree.addChild(mexico);
        var queryFinishedHandle = mexico.workingSet.on("QueryFinished", function() {
            if(mexico.bounds) 
                map.mapNavigator.fit({bounds: mexico.bounds, animate: true});
            queryFinishedHandle.remove();
        });
        
        
        
    }
    
    // Reloj
    timeUpdated();
    function timeUpdated() {
        $('#timelabel').text(formatTime());
        if(timer === 10) {
            actualizarCapaGPS();
            actualizarTabla();
            actualizarGeocerca();
            calcularDistancias();
            timer = 0;
        } else {
            timer++;
        }
        setTimeout(timeUpdated,1000);
        
     }
     /*
      * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
      * @returns {String}
      */
     function formatTime(newDate) {
        var date = newDate ? new Date(newDate) : new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
    
    actualizarCapaGPS();
    function actualizarCapaGPS() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "https://www.olly.science/piloto/position"
            //url: "https://www.olly.science/gps"
        }).done(function(data) {
            var features = data.features;
            var nf = features.length;
            for(var i =0; i<nf; i++) {
                var feature = features[i];
                var properties = feature.properties;
                var id = properties.id;
                var hora = properties.checkin;
                var coordinates = feature.geometry.coordinates;
                //var fecha = properties.fecha, hora = properties.hora;
                var date = formatDate(obtenerFechaActual(), hora);
                var numDate = new Date(date), nowDate = new Date();
                var dif = nowDate - numDate;
                /*if(dif > 30000) {
                    properties.status = "Desconectado";
                } else {
                    properties.status = "Conectado";
                }*/
                
                var anterior;
                try {
                    anterior = celulares.model.get(id);
                    properties = anterior.properties;
                    //properties.Distancia = anterior.properties.Distancia;
                } catch(e) {};
                coordinates[1] = coordinates[1]>0? (coordinates[1]*(-1)):coordinates[1];
                var point = Shapes.createPoint(referenceC, coordinates[1], coordinates[0],0, id, properties);
                var circle = Shapes.createCircleByCenterPoint(referenceC, coordinates[1], coordinates[0],0, "circulo"+id, properties, 10 );
                radioCelular.model.put(circle);
                celulares.model.put(point);
                var trayectoria = trayectorias[i];
                if(!trayectoria) {
                    trayectoria= {x: [], y: [], j: 0, date: 0};
                }
                var j = trayectoria.j, newPoint = true;
                /*if(j>0) {
                    var ox = trayectoria.x[j-1], oy = trayectoria.y[j-1];
                    var distancia = Shapes.distancia(ox, oy, coordinates[1], coordinates[0]);
                    if(distancia < 5) {
                        newPoint = false;
                    } else {
                        newPoint = true;
                    }
                }*/
                if(newPoint === true) {
                    trayectoria.x[j] = coordinates[1];
                    trayectoria.y[j] = coordinates[0];
                    var line = Shapes.createPolyline(referenceC, trayectoria.x, trayectoria.y, 0, i, {});
                    historial.model.put(line);
                    var point2 = Shapes.createPoint(referenceC, coordinates[1], coordinates[0],0, i +"."+j, properties);
                    historial.model.add(point2);
                    trayectoria.j ++;
                    trayectorias[i] = trayectoria;
                }
                actualizarTabla();
            }
            
            console.log("Actualizacion Recivida ");
        }).fail(function(e) {
            console.log("No se contacto con el server");
            console.log(e);
        });
        
    }
    actualizarTabla();
    function actualizarTabla() {
        var data = celulares.model.query();
        var features = data.array, n = features.length;
        var tabla = '<table>#contenido</table>';
        var etiqueta = '';
        var fila = '<tr><td>Estado</td><td>ID</td><td>CHECKIN</td><td>CHECKOUT</td><td>GEOCERCA</td></tr>';
        etiqueta += fila;
        
        for(var i=0; i<n; i++) {
            var id = features[i].id;
            var estado = features[i].properties.status;
            var checkout = features[i].properties.checkout;
            var checkin = features[i].properties.checkin;
            var geometry = features[i].geometry;
            var geocerca = features[i].properties.Geocerca;
            if(geometry.x)
                etiqueta += fila.replace("Estado", estado).replace("ID", id).replace("CHECKIN", checkin).replace("CHECKOUT", checkout).replace("GEOCERCA", geocerca);
        }
        tabla = tabla.replace("#contenido", etiqueta);
        document.getElementById("tablaDispositivos").innerHTML = tabla;
    }
    
    actualizarGeocerca();
    function actualizarGeocerca() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "https://www.olly.science/piloto/localities"
        }).done(function(data) {
            var localites = data.localites;
            var nL = localites.length;
            for(var i=0; i<nL; i++) {
                var localite = localites[i];
                var id = localite.id;
                var x = localite.lng;
                var y = localite.lat;
                var r = localite.radio;
                var locality = localite.locality;
                var status = localite.status;
                var circle = Shapes.createCircleByCenterPoint(referenceC, x, y, 0, id, localite, r);
                geoCercas.model.put(circle);
            }
        }).fail(function (e) {
            console.log("No se contacto con el server");
            console.log(e);
        });
    }
    
    
    function calcularDistancias() {
        var data1 = celulares.model.query();
        var features = data1.array, nf = features.length;
        var data2 = geoCercas.model.query();
        var geocercas = data2.array, ng = geocercas.length;
        var menorDistancia= -1, nGeocerca = "ninguna", distancia;
        for(var i=0; i<nf; i++) {
            var feature = features[i];
            for(var j=0; j<ng; j++) {
                var geocerca = geocercas[j];
                var radio = geocerca.geometry.bounds.height/2;
                distancia = Shapes.distancia2Puntos(feature, geocerca);
                if((menorDistancia<0 || distancia<menorDistancia) && distancia <= radio) {
                    menorDistancia = distancia;
                    nGeocerca = geocerca.properties.locality;
                }
            }
            
            if(feature.properties.status === "espera" || !feature.properties.Geocerca) {
                var estado = nGeocerca==="ninguna"? "afuera": "adentro";
                feature.properties.status = estado;
                feature.properties.Geocerca = nGeocerca;
            } else {
                var estado = feature.properties.Geocerca==="ninguna"? "afuera": "adentro";
                feature.properties.status = estado;

                if(feature.properties.Geocerca === "ninguna" && nGeocerca !== "ninguna") {
                    feature.properties.status = "entrando";
                    feature.properties.Geocerca = nGeocerca;
                    feature.properties.checkin = obtenerHoraActual();
                    //feature.properties.fecha = obtenerFechaActual();
                    actualizarTabla();
                    //actualizarEndpoint();
                    console.log("Se a entrado en una Geocerca\nGeocerca: "+nGeocerca);
                }
                if(feature.properties.Geocerca !== "ninguna" && nGeocerca !== feature.properties.Geocerca) {
                    feature.properties.status = "saliendo";
                    feature.properties.Geocerca = nGeocerca;
                    feature.properties.checkout = obtenerHoraActual();
                    //feature.properties.fecha = obtenerFechaActual();
                    actualizarTabla();
                    //actualizarEndpoint();
                    console.log("Se a salido en una Geocerca\nGeocerca: "+nGeocerca);
                }
                celulares.model.put(feature);
            }
            //feature.properties.Distancia = menorDistancia <0? null: menorDistancia;
            
            menorDistancia = -1;
            nGeocerca = "ninguna";
        }
        
    }
    /*
     * 
     */
    function formatDate( fecha, hora) {
        var i, j=0, y, m, d, c, newDate="";
        var baseDate = "aa/mes/dia hora:00";
        for(i=0; i<fecha.length; i++) {
            c = fecha.charAt(i);
            if(c ==='/' || c===' ' || c==='/n' || c===':') {
                switch(j) {
                    case 0: d = newDate; break;
                    case 1: m = newDate; break;
                    case 2: y = newDate; break;
                }
                newDate = "";
                j++;
            } else {
                newDate+= c;
            }
        }
        y = newDate;
        newDate = baseDate.replace("aa", y).replace("mes", m).replace("dia", d).replace("hora:00", hora);
        return newDate;
    }
    
    $("#actualizar").click(function () {
        actualizarCapaGPS();
        actualizarGeocerca();
        calcularDistancias();
        actualizarEndpoint();
        timer=0;
    });
    
    
    function obtenerFechaActual() {
        var date = new Date();
        var y = date.getFullYear();
        var me = date.getMonth()<10? "0"+date.getMonth(): date.getMonth();
        var d = date.getDay() <10? "0"+date.getDay(): date.getDay();
        
        return d+"/"+me+"/"+y;
    }
    function obtenerHoraActual() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
    
    function actualizarEndpoint() {
        var features = celulares.model.query().array;
        var n = features.length;
        var data;
        for(var i=0; i<n; i++) {
            var properties = features[i].properties;
            var status = properties.status.toLowerCase();
            var ci = properties.checkin;
            var co = properties.checkout;
            var t = properties.token;
            var l = properties.Geocerca;
            data = {
                status: status,
                tokendevice: t,
                checkin: ci,
                checkout: co,
                locality: l
            };
            console.log(data);
            var requestOptions = {
                method: "POST",
                data: data,
                handleAs: "text",
                headers: {
                  Accept: "application/javascript, application/json",
                  "If-Match": "*",
                  //We only add if-none-match if we try to put a feature that already has an id
                  //otherwise, the request will fail if any resource already exists
                  "If-None-Match": "*"
                }
            };
            /*
            var requestPromise = request("https://www.olly.science/piloto/tokendevice", requestOptions);
            Promise.resolve(requestPromise.response)
            .then(function(response) {    
                    console.log(response);
            });
            
            $.ajax({
                type:'Post',
                dataType: "application/javascript, application/json",
                data: data,
                url: "https://www.olly.science/piloto/tokendevice"
            }).done(function(response) {
                console.log(response);
            }).fail(function (e) {
                console.log(e);
            });
            /*
            $.post('https://www.olly.science/piloto/tokendevice', data, function(response) {
                console.log(response);
            }, 'json').fail(function(e) {
                console.log(e);
            });
            */
            var http = new XMLHttpRequest();
            var url = "https://www.olly.science/piloto/tokendevice";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && http.status === 200) { 
                   //aqui obtienes la respuesta de tu peticion
                   alert(http.responseText);
                }
            };
            http.send(JSON.stringify(data));
            
        }
        
    }
});




