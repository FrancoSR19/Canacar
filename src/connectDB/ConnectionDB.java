package connectDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
	
	private final String DB_DRIVER = "org.postgresql.Driver";
	private final String DB_CONNECTION = "jdbc:postgresql://10.138.252.250/test";
	private final String DB_USER="postgres";
	private final String DB_PASSWORD="postgres";
	private Connection con;
	
	public ConnectionDB(){
		try {
			Class.forName(DB_DRIVER);
			con = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			System.out.println("Conexion Exitosa");
		} catch (ClassNotFoundException e) {
			System.out.println("Error al cargar el driver de la base de datos ");
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println("Error al conectarse a la database ");
			System.out.println(e.getMessage());
		}
	}
	
	public Connection getDBConnection() {
		return con;
	}
	
	public static void main(String[] args) throws SQLException{
		ConnectionDB con = new ConnectionDB();
		System.out.println(con.getDBConnection());
		
	}
	
	

}
