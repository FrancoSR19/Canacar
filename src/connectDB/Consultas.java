package connectDB;

import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Consultas extends ConnectionDB {

	public boolean Autenthication(String user, String pwd) {
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			String query = "select * from customers where first_name = ? and last_name= ?";
			pstm = getDBConnection().prepareStatement(query);
			pstm.setString(1, user);
			pstm.setString(2, pwd);
			
			rs = pstm.executeQuery();
			int cont=0;
			if(rs.next()) {
				cont++;
			}
			if(cont>0) {
				return true;
			}
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally {
			try {
				if(getDBConnection() != null) {
					getDBConnection().close();
				}
				if(pstm != null) {
					pstm.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}
		return false;
	}
	
	public void Visualizar() {
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			String query = "select * from customers";
			pstm = getDBConnection().prepareStatement(query);
			rs = pstm.executeQuery();
			
			while(rs.next()) {
				System.out.println("Name "+ rs.getString("first_name"));
			}
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally {
			try {
				if(getDBConnection() != null) {
					getDBConnection().close();
				}
				if(pstm != null) {
					pstm.close();
				}
				if(rs != null) {
					rs.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}
	}
	
	
	public static void main(String[] args) {
		Consultas co = new Consultas();
		//co.Visualizar();
		System.out.println(co.Autenthication("Anne", "Watson"));
	}
}
