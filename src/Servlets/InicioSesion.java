package Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import connectDB.Consultas;

/**
 * Servlet implementation class InicioSesion
 */
@WebServlet("/Iniciar")
public class InicioSesion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InicioSesion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			String user = request.getParameter("email");
			String pwd = request.getParameter("password");
			Consultas consult = new Consultas();
			boolean con = consult.Autenthication(user, pwd);
			try {
				if(con){
					response.sendRedirect("web/proyecto/index.html");
				}else {
					response.sendRedirect("message.jsp");
				}
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
