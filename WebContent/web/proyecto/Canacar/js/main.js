var map, municipios = [], tablaIncidentes = null;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "dojo/query",
    "luciad/view/PaintRepresentation",
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    
    
    'luciad/util/ColorMap',
    "./createTimeLayer",
    "recursos/js/Graficas/Graficas",
    "recursos/js/Shapes",
    
    "./painters/layerPainter",
    "./painters/EventTimePainter",
    "./painters/municipiosPainter",
    "./painters/localidadesPainter",
    "./painters/rutasPainter",
    "./painters/painterTrayectorias",
    "./painters/agebPainter",
    "./painters/nuevosPainter",
    "./painters/paisPainter",
    
    "./BalloonStyles/defaultBalloon",
    "luciad/view/controller/BasicCreateController",
    "luciad/shape/ShapeType",
    "./updateHistogram",
    "./painters/heatMapPainter",
    "./OnHoverController",
    "recursos/js/Cluster/ClusterPainter",
    
    "./painters/CelularesPainter",
    "recursos/js/Graficas/Tablas",
    "./painters/sectoresPainter",
    "luciad/shape/ShapeFactory",
    
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on,query, PaintRepresentation, DojoMap, ReferenceProvider, LayerConfigUtil,
        ColorMap, createTimeLayer, Graficas, Shapes,
        layerPainter,  EventTimePainter, municipiosPainter, localidadesPainter, rutasPainter, painterTrayectorias, agebPainter, nuevosPainter, paisPainter,
        defaultBalloon, BasicCreateController, ShapeType, updateHistogram, heatMapPainter, OnHoverController, ClusterPainter,
        CelularesPainter, Tablas, sectoresPainter, ShapeFactory
) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    //Layer list
    var patrulla, zonas, sectores, ageb,manzanas, eventos = null, features =null, rutas = [], puntos =[], editable, mexico;
    var nPuntos, celulares, simularAlerta;
    
    var selected = null, tablaDatos, menuAbierto=true, menuInteligencia = false;
    //Graficas
    var graficaTipo, graficaVictima, graficaZona, graficaDia, graficaTurno;
     /*
     * Creacion de la barra de tiempo
     */
    var nuevoRegistro = 0;
    var timeFilter, timeSlider, startTime, endTime, Year, fitColumns = false;
    var cargarDatos = true;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: true, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Road"});
        } 
        patrulla=null; zonas=null; sectores=null; eventos = null; features =null; rutas=[];
        graficaTipo=null; graficaVictima=null; graficaZona=null; graficaDia=null; graficaTurno=null;
        selected = null; tablaDatos = null; menuAbierto=true; menuInteligencia = false;
        CrearCapas();
        onHoverController = new OnHoverController();
        map.controller = onHoverController;
    }
    /*
     * Funcion encagrada de la creacion de todas las capas que seran mostradas en el mapa principal.
     * ademas aqui se encuentra una funcion ajax con la que leemos la informacion necesaria para crear la capa de eventos.
     * 
     * @returns {undefined}
     */
    function CrearCapas() {
        
        var urls =["CDMX", "EdoMex","Guanajuato","Hidalgo","Michoacan","Queretaro", "NuevoLeon"];
        for(var j =0; j<urls.length; j++){
            municipios[j]  = LayerFactory.createMemoryLayer(referenceC, {label: urls[j], selectable: false, 
                editable: false, painter: new municipiosPainter()}, "data/municipios/"+urls[j]+".json");
            map.layerTree.addChild(municipios[j]);
            municipios[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        /*
        municipios[0] = LayerFactory.createMemoryLayer(referenceC, {label: "Municipios", selectable: true, 
                editable: false, painter: new municipiosPainter()}, "../recursos/json/Municipios2.json");
            map.layerTree.addChild(municipios[0]);
            municipios[0].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        */
        var urlRutas=["Toluca-Quer�taro","Toluca-Quer�taro","Toluca-Quer�taro","Azcapotzalco-Quer�taro","Azcapotzalco-Quer�taro","Iztapalapa-Quer�taro","Iztapalapa-Quer�taro"];
        
        for(j=0; j<7; j++) {
            rutas[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: urlRutas[j]+" R"+(j+1), selectable: true, painter: new rutasPainter(), visible: false, editable:false
            }, "data/Rutas/Ruta"+(j+1)+".json");
            map.layerTree.addChild(rutas[j]);
            rutas[j].setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
            rutas[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        var urlPuntos = ["puntos1","puntos2","puntos3"];
        for(j=0;j<urlPuntos.length; j++){
            puntos[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: urlPuntos[j], selectable: true, painter: new localidadesPainter(), visible: false, editable:false
            }, "data/Rutas/"+urlPuntos[j]+".json");
            map.layerTree.addChild(puntos[j]);
            puntos[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        
        var urlPuntos = ["puntos1","puntos2","puntos3"];
        for(j=0;j<urlPuntos.length; j++){
            puntos[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: urlPuntos[j], selectable: true, painter: new localidadesPainter(), visible: false, editable:false
            }, "data/Rutas/"+urlPuntos[j]+".json");
            map.layerTree.addChild(puntos[j]);
            puntos[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        nPuntos = LayerFactory.createMemoryLayer(referenceC, {label:"Nuevos Incidentes", selectable: true, painter: new nuevosPainter()});
        map.layerTree.addChild(nPuntos);
        nPuntos.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        celulares = LayerFactory.createMemoryLayer(referenceC, {label:"Celulares", visible: false, selectable: true, editable: true, painter: new CelularesPainter()});
        map.layerTree.addChild(celulares);
        celulares.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        celulares.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        simularAlerta = LayerFactory.createMemoryLayer(referenceC, {label:"Simulacion", selectable: true, editable: false, painter: new CelularesPainter()});
        map.layerTree.addChild(simularAlerta);
        simularAlerta.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        simularAlerta.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        var proxpol = LayerFactory.createFeatureLayer(referenceC, {label: "Sectores", selectable: false, editable: false, painter: new sectoresPainter()}, "data/cuadrantesEscobedo.json");
        map.layerTree.addChild(proxpol);
        
            
        editable = LayerFactory.createMemoryLayer(referenceC, {label: "Rutas", selectable: true, painter: new rutasPainter(), editable: true});
        map.layerTree.addChild(editable);
        editable.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        //var mexicoLayer = LayerFactory.createKmlLayer({label:"Mexico", selectable:false, painter: new paisPainter()},'../recursos/estados.kml');
        //map.layerTree.addChild(mexicoLayer);
        mexico = LayerFactory.createFeatureLayer(referenceC, {label: "Mexico", painter: new paisPainter(), editable: false}, "../recursos/estados.json");
        map.layerTree.addChild(mexico);
        var queryFinishedHandle = mexico.workingSet.on("QueryFinished", function() {
            if(mexico.bounds) 
                map.mapNavigator.fit({bounds: mexico.bounds, animate: true});
            queryFinishedHandle.remove();
        });
        
        /*
         * 
         * Aqui creamos una variable la cual usaremos para identificar el a�o del cual mostraremos la informacion
         */
        
        var sStart = "2016/01/01 00:00:00", sEnd = "2019/01/01 00:00:00";
        startTime = Date.parse(sStart)/1000, endTime = Date.parse(sEnd)/1000;
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/allData.json"
        }).done(function(data) {
            console.log("archivo leido");
            features = createTimeLayer.createTimeFeature(data.features);
            var graficas = crearGraficas(features);
            
            var store = LayerFactory.MemoryStore({data: features});
            var model = LayerFactory.FeatureModel(store, {reference: referenceC});
            eventos = LayerFactory.FeatureLayer(model, {
                label: "Incidentes", 
                id: "eventos", 
                editable:false, 
                painter: new heatMapPainter(), 
                //painter: ClusterPainter.painter,
                //transformer: ClusterPainter.transformer,
                visible: true,
                selectable: true
            });
            
            eventos.balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
            map.layerTree.addChild(eventos);
            updateHistogram.updateHistogram(map, features, eventos, referenceC, startTime, endTime, tablaDatos, graficas,municipios );
            
        }).fail(function(e) {
            console.log(e);
        });
    }
    
   
    /*
     * botones superiores en el mapa
     */
    $("#botonGraficas").click(function () {
        if(menuAbierto) {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            menuAbierto=false;
        }else {
            //$("#menuInteligencia").fadeOut("slow");
            $("#menuGraficas").fadeIn("slow");
            $("#panelHorario").fadeOut("slow");
            $("#afterafter").fadeIn("slow");
            menuAbierto=true;
            menuInteligencia = false;
        }
    });
    $("#botonInteligencia").click(function () {
        if(menuInteligencia) {
            $("#panelHorario").fadeOut("slow");
            //$("#menuInteligencia").fadeOut("slow");
            menuInteligencia = false;
        }else {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#panelHorario").fadeIn("slow");
            //$("#menuInteligencia").fadeIn("slow");
            menuAbierto=false;
            menuInteligencia = true;
        }
    });
    /*$("#botonRutas").click(function () {
        if(menuAbierto) {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            menuAbierto = false;
        } else {
            $("#menuGraficas").fadeIn("slow");
            $("#afterafter").fadeIn("slow");
            menuAbierto = true;
        }
        
    });*/
    $("#restart").click(function () {
        alert("Se Reiniciara el mapa");
        MapFactory.destroyMapAndComponents(map);
        //timeSlider.resetOverview();
        MapFactory.destroyMapAndComponents(timeSlider.overView);
        MapFactory.destroyMapAndComponents(timeSlider);
        Start();
    });
    
    var registro = false;
    $("#crearRuta").click(function() {
        if(registro===false) {
            $("#panelRegistro").fadeIn("slow");
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#pTablaToken").fadeOut("slow");
            registro=true;
            menuAbierto=false;
        } else {
            $("#panelRegistro").fadeOut("slow");
            registro=false;
        }
        
            
    });
   /*
    * Aqui utilizamos un arreglo de cadenas de caracteres para crear una serie 
    * de datos de informacion y meterlos en un select
    * <select class="selectPanel" id="zonaFiltro">
    * en el index, utilizando su id.
    * @param {type} div
    * @param {type} opciones
    * @returns {undefined}
    */
    function setOptions(div, opciones) {
        var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", "Todos").replace("$LABEL", "Todos");
        var  n = opciones.length;
        for(var i =0; i<n; i++) {
            etiqueta += base.replace("$VALUE", opciones[i]).replace("$LABEL", opciones[i]);
        }
        document.getElementById(div).innerHTML = etiqueta;
        
    }
    
    
    var X = -1, x=0, y = 30, y2 = 0;
    // Reloj
    timeUpdated();
    function timeUpdated() {
        $('#timelabel').text(formatTime());
        
        y++;
        if(y>5) {
            y=0;
            getNumber();
        }
        /*
        if(nuevoRegistro ===1) {
            y2++;
            if(y2 >=20) {
                alert("El veh�culo ha sido comprometido, la informaci�n ha sido guardada");
                y2=0;
                nuevoRegistro ++;
                folios[nf-1][2] = "Registrado";
                Tablas.crearTablaDIV("tablaToken", folios);
            }
        }
        if(nuevoRegistro === 3){
            y2++;
            if(y2 >=20) {
                alert("El viaje se ha realizado con �xito, la informaci�n ha sido eliminada");
                console.log("Mensaje mostrado");
                nuevoRegistro = 0;
                y2 = 0;
                folios[nf-1][2] = "Borrado";
                Tablas.crearTablaDIV("tablaToken", folios);
            }
            
        }*/
        setTimeout(timeUpdated,500);
        
        for(var nm=0; nm<municipios.length; nm++) {
            municipios[nm].filter = function(feature) {
                return true;
            };
        }
     }
     /*
      * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
      * @returns {String}
      */
     function formatTime() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
   
    var botones = ["ruta1","ruta2","ruta3"];
    botones.forEach(function(event) {
        $("#"+event).click(function() {
            
            switch(event) {
                case "ruta1":
                    puntos[0].visible = !puntos[0].visible;
                    switch(X) {
                        case 0: 
                            rutas[0].visible = !rutas[0].visible; 
                            rutas[1].visible = false;
                            rutas[2].visible = false;
                            break;
                        case 1:
                            rutas[1].visible = !rutas[1].visible; 
                            rutas[0].visible = false;
                            rutas[2].visible = false;
                            break;
                        default: 
                            rutas[2].visible = !rutas[2].visible;
                            rutas[0].visible = false;
                            rutas[1].visible = false;
                            break;
                    }
                    break;
                case "ruta2":
                    puntos[1].visible = !puntos[1].visible;
                    if(X === 0 ) {
                        rutas[3].visible = !rutas[3].visible;
                        rutas[4].visible = false;
                    } else {
                        rutas[3].visible = false;
                        rutas[4].visible = !rutas[4].visible;
                    }
                    break;
                case "ruta3":
                    puntos[2].visible = !puntos[2].visible;
                    if(X === 0 ) {
                        rutas[5].visible = !rutas[5].visible;
                        rutas[6].visible = false;
                    } else {
                        rutas[5].visible = false;
                        rutas[6].visible = !rutas[6].visible;
                    }
                    break;
            }
        });
    });
    ////========================================================================================///
    
    function getNumber() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "https://www.olly.science/gps" //report/verify  report/gps
        }).done(function(data) {
            var features = data.features;
            var nf = features.length;
            for(var i =0; i<nf; i++) {
                var feature = features[i];
                var properties = feature.properties;
                var id = properties.id;
                var coordinates = feature.geometry.coordinates;
                var fecha = properties.fecha, hora = properties.hora;
                var date = formatDate(fecha, hora);
                var numDate = new Date(date), nowDate = new Date();
                var dif = nowDate - numDate;
                
                //var circle = Shapes.createCircleByCenterPoint(referenceC, coordinates[1], coordinates[0],0, "circulo"+id, properties, 10 );
                //celulares.model.put(circle);
                switch(properties.status) {
                    case "Conectado":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Esperando";
                            Tablas.crearTablaDIV("tablaToken", folios);
                        }
                        break;
                    case "Desconectado":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Esperando";
                            Tablas.crearTablaDIV("tablaToken", folios);
                        }/*
                        if(datos) {
                            var nombres = datos[0].nombres;
                            var p =  Object.assign(properties, datos[0].Autos);
                            properties = Object.assign(properties, p);
                            
                        }*/
                        var point = Shapes.createPoint(referenceC, coordinates[1], coordinates[0],0, id, properties);
                        celulares.model.put(point);
                        break;
                    case "Alerta":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Registrado";
                            Tablas.crearTablaDIV("tablaToken", folios);
                        }
                        var point = Shapes.createPoint(referenceC, coordinates[1], coordinates[0],0, id, properties);
                        celulares.model.put(point);
                        alert("El veh�culo ha sido comprometido, la informaci�n ha sido guardada");
                        break;
                }
            }
            //actualizarTabla(features);
        }).fail(function(e) {
            console.log("No se contacto con el server");
            console.log(e);
        });
    }
    
    function formatDate( fecha, hora) {
        var i, j=0, y, m, d, c, newDate="";
        var baseDate = "aa/mes/dia hora:00";
        for(i=0; i<fecha.length; i++) {
            c = fecha.charAt(i);
            if(c ==='/' || c===' ' || c==='/n' || c===':') {
                switch(j) {
                    case 0: d = newDate; break;
                    case 1: m = newDate; break;
                    case 2: y = newDate; break;
                }
                newDate = "";
                j++;
            } else {
                newDate+= c;
            }
        }
        y = newDate;
        newDate = baseDate.replace("aa", y).replace("mes", m).replace("dia", d).replace("hora:00", hora);
        return newDate;
    }
    
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function actualizarRutas() {
        if(X>0) {
            if(rutas[0].visible && X ===1) {
                rutas[0].visible = false;
                rutas[1].visible = true;
            }else {
                if(X>1 && rutas[1].visible) {
                    rutas[1].visible = false;
                    rutas[2].visible = true;
                }
            }
            
            if(rutas[3].visible) {
                rutas[3].visible = false;
                rutas[4].visible = true;
            }
            if(rutas[5].visible) {
                rutas[5].visible = false;
                rutas[6].visible = true;
            }
        }
    }
    
    /*
     * linkCreateButton: encargado de crear los controles de creacion de poligonos nuevos para la capa de editable.
     * @param {Object} map
     * @param {String} domId
     * @param {int} shapeType
     * @returns {undefined}
     */
    function linkCreateButton(map, domId) {
        try {
          on(dom.byId(domId), "click", function() {
              $("#panelRegistro").fadeOut("slow");
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            query("#" + domId).addClass(stylePressed);
            map.selectObjects([]);
            var createController = new BasicCreateController(ShapeType.POINT, {}, {finishOnSingleClick: true});
            
            createController.onObjectCreated = function() {
                BasicCreateController.prototype.onObjectCreated.apply(this, arguments);
                query(".pressed").removeClass(stylePressed);
                var newFeature = arguments[2];
                
                n++;
                if(n===1) {
                    //console.log("Primer punto creado");
                    arguments[2].id = nrutas+"A";
                    fPoint = newFeature.geometry.coordinates;
                    document.getElementById(domId).innerHTML = "Seleccionar Destino";
                    document.getElementById("longitud1").value = fPoint[0];
                    document.getElementById("latitud1").value = fPoint[1];
                    editable.model.put(arguments[2]);
                }
                else {
                    if(n === 2){
                        arguments[2].id = nrutas + "B";
                        sPoint = newFeature.geometry.coordinates;
                        n=0;
                        buscarRuta(fPoint, sPoint);
                        document.getElementById(domId).innerHTML = "Seleccionar Salida";
                        document.getElementById("longitud2").value = sPoint[0];
                        document.getElementById("latitud2").value = sPoint[1];
                        editable.model.put(arguments[2]);
                    }
                }
                $("#panelRegistro").fadeIn("slow");
            };
            map.controller = createController;
          });
        }catch(e) {
            console.log("no se creo el control para el boton "+ domId);
            console.log(e);
        }
    }
    var n = 0, fPoint, sPoint, nrutas = 0;
    var trayectorias = new Array(), posiciones = new Array();
    //linkCreateButton(map, "setCoordenada");
    
    function buscarRuta (startPoint, endPoint) {
        var coordenadasOrigen = [[startPoint.y, startPoint.x]];
        var coordenadasDestino = [[endPoint.y, endPoint.x]];
        console.log(JSON.stringify(coordenadasOrigen));
        console.log(JSON.stringify(coordenadasDestino));
        $.getJSON('http://api.sintrafico.com/matrix', {
            key: '6a7f5ed1692ca13dd1940d0b020781b10ce4ad9a1b0b507739490d69d8d76e93',
            start:  JSON.stringify(coordenadasOrigen),
            end:  JSON.stringify(coordenadasDestino)
        })
        .done(function(data, textStatus, jqXHR) {
            console.log(data);
            var features = Shapes.crearRuta(referenceC, data, nrutas);
            if(features.length ===1) {
                var feature = features[0];
                var properties = feature.properties;
                /*var di = properties.distance, du = properties.duration, s = properties.sumary;
                properties = {
                    Duracion: getDistance(properties.duration),
                    Dustancia: getDuration(properties.distance),
                    Sumario: s
                };
                feature.properties = properties;
                */
                feature.id = nrutas;
                editable.model.put(feature);
                nrutas ++;
            }
            console.log(features);
        });
    }
    
    $("#labels").click(function () {
        var domId = document.getElementById("labels");
        var value;
        if(domId.value === "true") {
            value = false;
        } else
            value = true;
        for(var i=0; i<municipios.length; i++) {
            municipios[i].setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
            municipios[i].selectable = !value;
        }
        if(value) {
            domId.innerHTML = "Ocultar Etiquetas";
        } else {
            domId.innerHTML = "Ver Etiquetas";
        }
        domId.value = value;
    });
    
    crearHorario("horario");
    function crearHorario(div) {
        var nivel = [3,3,2,1,0,0,0,1,1,2,2,3,3,1,1,1,2,0,0,0,0,1,1,2,2,3,3,3,2,2,0,0,0,0,0,0,1,1,1,1,3,3,3,3,3,2,2,0,0,0,0,1,1,1,,3,3,3,2,2,1,3,3,1,1,0,0,0,0,0,0,0];
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center" bgcolor="$COLOR">$dato</td >';
            var columnas ='';
            var fila = '<tr>$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n , m ;
    
            var datos, k=0;
            for(var i=0; i<7; i++) {
                //datos = tabla[i];
                columnas += columna.replace('$dato', "<b>"+getDia(i)+"</b>").replace("$COLOR", getColor(4));
                for(var j=1;j<24;j++) {
                    var color;
                    if(k< nivel.length)
                        color= getColor(nivel[j+k]);
                    else
                        color = "";
                    k++;
                    columnas += columna.replace('$dato', j+" Hrs").replace("$COLOR", color);
                }
                k=Math.round(getRandom(0,15));
                //columna = '<td align="center">$dato</td >';
                filas += fila.replace('$columna', columnas);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    
    function getColor(id) {
        switch(id) {
            case 0 : return "#2ecc71";
            case 1: return "#f7dc6f";
            case 2: return "#f39c12";
            case 3: return "#d35400";
            case 4: return "#85929e";
            default: return "#19232c";
        }
    }
    function getDia(id) {
        switch(id) {
            case 0: return "Domingo";
            case 1: return "Lunes";
            case 2: return "Martes";
            case 3: return "Miercoles";
            case 4: return "Jueves";
            case 5: return "Viernes";
            case 6: return "Sabado";
            default: return "No Dia";
        }
    }
    
    /*
     * =================================================================================================
     *                          REGISTRAR RUTA  
     * =================================================================================================
     */
    var folios = [["CURP","Folio", "Estado"]], nf = 1;
    $("#registrar").click(function () {
        
        var date = new Date();
        var y = date.getFullYear();
        var mo = date.getMonth()<10? "0"+date.getMonth(): date.getMonth();
        var d = date.getDay() <10? "0"+date.getDay(): date.getDay();
        var n = nf<10? "00"+nf: "0"+ nf;
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        var fecha = d+"/"+mo+"/"+y, hora = h+":"+m+":"+s;
        var folio = "28062018"+n;
        var curpNode = document.getElementById("infoConductor");
        var curp = curpNode.selectedOptions[0].innerHTML;
        var placaNode = document.getElementById("placas");
        var placa = placaNode.selectedOptions[0].innerHTML;
        var rutaNode = document.getElementById("sruta");
        var ruta = rutaNode.selectedOptions[0].innerHTML;
        folios[nf] = [curp, folio, "Espera"];
        Tablas.crearTablaDIV("tablaToken", folios);
        nf++;
        
        $('#panelRegistro').fadeOut('slow');
        $("#pTablaToken").fadeIn("slow");
        nuevoRegistro ++;
        var data = {
            folio: folio,
            status: "espera",
            curp: curp,
            transportista: document.getElementById("nombre").value,
            camion: document.getElementById("modelo").value,
            placa: placa,
            marca: document.getElementById("marca").value,
            carga: document.getElementById("tipo").value,
            valor: document.getElementById("valor").value,
            remolques: "1",
            ruta: ruta,
            fecha_salida: document.getElementById("fecha1").value,
            fecha_llegada: document.getElementById("fecha2").value,
            hora_salida: document.getElementById("hora1").value,
            hora_llegada: document.getElementById("hora2").value
            
        };
        
        var divs = ["nombre","paterno","materno","sexo","edad","marca","submarca","modelo","serie","gps","fecha1","hora1","fecha2","hora2",
                /*"longitud1","latitud1","longitud2","latitud2",*/"tipo","empresa","valor","descripcion","cantidadefectivo","propietoria","razonsocial"];
        /*for(var i=0;i<divs.length;i++) {
                data[divs[i]] = document.getElementById(divs[i]).value;
        }*/
        initCuestionario(divs);
        console.log(data);
        registrarRuta(data, folio);
    });
    
    /*
     *============================================================================================================
     *                                      CONEXIONES CON ENDPOINTS
     *============================================================================================================
     */
    function initCuestionario(divs) {
        divs.forEach(function(div) {
            document.getElementById(div).value = "";
        });/*
        $.ajax({
            type:'get',
            dataType: "json",
            url: "https://www.olly.science/users/curp_status",
            success: function (response) {
                var users = response.users;
                var nUsers = users.length;
                var curps = [], status, j=0;
                for(var i=0; i<nUsers; i++) {
                    status = users[i].status;
                    if(status === "espera") {
                        curps[j] = users[i].curp;
                        j++;
                    }
                }
                if(curps.length<1) 
                    Graficas.setOptions("infoConductor", curps, "No CURP disponibles");
                else 
                    Graficas.setOptions("infoConductor", curps, "Seleccionar Curp");
            },
            error: function (error) {
                console.log(error);
            }
        });*/
    }
    
    function registrarRuta(datos, folio) {
        var http = new XMLHttpRequest();
            var url = "https://www.olly.science/report/register";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && http.status === 200) { 
                   //aqui obtienes la respuesta de tu peticion
                   alert('Los datos fueron enviados\nFolio:  '+folio); 
                   alert(http.responseText);
                }
            };
            http.send(JSON.stringify(datos));
    }
    getTokenStatus();
    function getTokenStatus() {
        $.ajax({
            type:'get',
            dataType: "json",
            url: "https://www.olly.science/users/token_status",
            success: function (response) {
                var users = response.users;
                var nUsers = users.length;
                var datos = [["Usuario (Email)", "Alerta", "Confirmacion","Cuestionario"]];
                for(var i=0; i<nUsers; i++) {
                    var al = users[i].alertapp;
                    var co = users[i].confirmapp;
                    var cu = users[i].questionaryapp;
                    var em = users[i].user;
                    datos [i+1] = [em, al, co, cu];
                }
                Tablas.crearTablaDIV("tablaTokenStatus", datos);
            }
        }).fail(function(e) {
            //console.log("No se contacto con el server");
            console.log(e);
        });
    }
    
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = ["empresasFiltro", "estadosFiltro"], lab =["EMPRESA", "NOMBRE", "MOTIVO"];
    var titles = ["Empresas","Estados","Motivo del Incidentes"];
    var types = ["piechart",  "piechart", "piechart"];
    var gEmpresas={}, gNombres={}, gMotivo={};
    function crearGraficas(features) {
        var datos = Graficas.crearGraficasFeatures(features, "Graficas", lab, types, fil, "tablaCompleta", titles);
        var graficas = datos.graficas;
        tablaDatos = datos.tablaDatos;
        datos = null;
        return graficas;
        
    }
    /*
     *      Control de Densidad del mapa de calor
     */
    $("#Densidad").on("change input", function() {
        var x = 1, y = 0;
        x = Math.round(parseFloat($("#Densidad").val()));
        if(x<=10 && x >0) {
            eventos.painter.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 0.5)"},
                {level: y+=x, color: "rgba(  0, 100,   255, 0.5)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
        } else {
            eventos.painter.density = null;
        }
    });
    
    //actualizarTabla();
    function actualizarTabla(f) {
        var features;
        if(f) {
           features = f; 
        } else {
            var data = celulares.model.query();
            features = data.array;
        }
        
        var n = features.length;
        var tabla = '<table>#contenido</table>';
        var etiqueta = '';
        var fila = '<tr bgcolor="$COLOR"><td>Estado</td><td>Hora</td><td>Fecha</td><td style="max-width: 270px;">Token</td></tr>';
        etiqueta += fila;
        var j=0;
        for(var i=0; i<n; i++) {
            var id = features[i].properties.id;
            //var id = Shapes.getRandom(8000, 9999);
            var estado = features[i].properties.status;
            var hora = features[i].properties.hora;
            var fecha = features[i].properties.fecha;
            var color;
            var geometry = features[i].geometry;
            if(geometry.x || geometry.type === "Point") {
                if(j%2 === 0 )
                    color = '';
                else
                    color = ' #566573 ';
                etiqueta += fila.replace("Estado", estado).replace("Hora", hora).replace("Fecha", fecha).replace("Token", id).replace("$COLOR", color);
                j++;
            }
        }
        tabla = tabla.replace("#contenido", etiqueta);
        document.getElementById("tablaToken").innerHTML = tabla;
    }
    
    var pTokens = false;
    $("#bTokens").click(function () {
        $("#pTablaToken").fadeIn("slow");
    });
    $("#bAlertas").click(function () {
        var bAlerta = document.getElementById("bAlertas");
        var value = (bAlerta.value=== "true")? false : true;
        celulares.visible = value;
        bAlerta.value = value;
    });
    var nS=0;
    ////================================================================================================================
    $("#bSimulacion").click(function() {
        var coordinates = [[ -100.32175, 25.78995 ], [ -100.32854, 25.77114 ],[ -100.37165, 25.80416 ]];
        var proxpol = [1,4,8];
        var point = Shapes.createPoint(referenceC, coordinates[nS][0], coordinates[nS][1], 0, nS, {Proxpol: proxpol[nS]});
        simularAlerta.model.put(point);
        alert("Se a detectado un nuevo Incidente\nEn el Proxpol "+proxpol[nS]);
        var coordenadas = [coordinates[nS][0],0.5,coordinates[nS][1], 0.5, 1, 0];
        map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, coordenadas),
                animate: true
        });
        nS++;
        if(nS>coordinates.length){
            nS=0;
        }
    });
    ////================================================================================================================
    
    var selectCurp = document.getElementById("infoConductor");
    var datos;
    selectCurp.addEventListener("change", function (data) {
        var valueCurp = selectCurp.selectedOptions[0].value;
        var nombre, paterno, materno, edad, sexo;
        var nombres = datos.nombres;
        switch(valueCurp) {
            case "1":
                nombre = nombres[0].name;
                paterno = nombres[0].paterno;
                materno = nombres[0].materno;
                edad = nombres[0].edad;
                sexo = nombres[0].sexo;
                break;
            case "2":
                nombre = nombres[1].name;
                paterno = nombres[1].paterno;
                materno = nombres[1].materno;
                edad = nombres[1].edad;
                sexo = nombres[1].sexo;
                break;
            case "3":
                nombre = nombres[2].name;
                paterno = nombres[2].paterno;
                materno = nombres[2].materno;
                edad = nombres[2].edad;
                sexo = nombres[2].sexo;
                break;
        }
        document.getElementById("nombre").value = nombre;
        document.getElementById("paterno").value = paterno;
        document.getElementById("materno").value = materno;
        document.getElementById("edad").value = edad;
        document.getElementById("sexo").value = sexo;
    });
    var selectPlaca = document.getElementById("placas");
    selectPlaca.addEventListener("change", function (data) {
        var valueCurp = selectPlaca.selectedOptions[0].value;
        var  marca, submarca, modelo, serie, gps;
        var nombres = datos.Autos;
        switch(valueCurp) {
            case "U12DNA":
                marca = nombres[0].marca;
                submarca = nombres[0].submarca;
                modelo = nombres[0].modelo;
                serie = nombres[0].serie;
                gps = nombres[0].gps;
                break;
            case "ZMT136":
                marca = nombres[1].marca;
                submarca = nombres[1].submarca;
                modelo = nombres[1].modelo;
                serie = nombres[1].serie;
                gps = nombres[1].gps;
                break;
            case "A47NOT":
                marca = nombres[2].marca;
                submarca = nombres[2].submarca;
                modelo = nombres[2].modelo;
                serie = nombres[2].serie;
                gps = nombres[2].gps;
                break;
        }
        document.getElementById("marca").value = marca;
        document.getElementById("submarca").value = submarca;
        document.getElementById("modelo").value = modelo;
        document.getElementById("serie").value = serie;
        document.getElementById("gps").value = gps;
    });
    leerDatos();
    function leerDatos () {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/datos.json" //report/verify  report/gps
        }).done(function(data) {
            datos = data;
        }).fail(function(e) {
            console.log("No se encontro el archivo");
        });
    }
    
    var selectRuta = document.getElementById("sruta");
    selectRuta.addEventListener("change", function (data) {
        var valueRuta = selectRuta.selectedOptions[0].value;
        switch(valueRuta) {
                case "1":
                    puntos[0].visible = !puntos[0].visible;
                    switch(X) {
                        case 0: 
                            rutas[0].visible = !rutas[0].visible; 
                            rutas[1].visible = false;
                            rutas[2].visible = false;
                            break;
                        case 1:
                            rutas[1].visible = !rutas[1].visible; 
                            rutas[0].visible = false;
                            rutas[2].visible = false;
                            break;
                        default: 
                            rutas[2].visible = !rutas[2].visible;
                            rutas[0].visible = false;
                            rutas[1].visible = false;
                            break;
                    }
                    break;
                case "2":
                    puntos[1].visible = !puntos[1].visible;
                    if(X === 0 ) {
                        rutas[3].visible = !rutas[3].visible;
                        rutas[4].visible = false;
                    } else {
                        rutas[3].visible = false;
                        rutas[4].visible = !rutas[4].visible;
                    }
                    break;
                case "3":
                    puntos[2].visible = !puntos[2].visible;
                    if(X === 0 ) {
                        rutas[5].visible = !rutas[5].visible;
                        rutas[6].visible = false;
                    } else {
                        rutas[5].visible = false;
                        rutas[6].visible = !rutas[6].visible;
                    }
                    break;
                default: break;
            }
    });
    
});


var hacerFit = false;
function filtrarMunicipio(nombre) {
    hacerFit = true;
    for(var id in municipios) {
        municipios[id].filter = function (feature) {
            var name = feature.properties.NOMBRE;
            if(name ===nombre && hacerFit === true) {
                map.mapNavigator.fit({bounds: feature.geometry.bounds, animate: true});
                hacerFit = false;
            } 
            return true;
        };
    }
}

function borrarFiltro () {
    for(var layer in municipios) {
        municipios[layer].filter = null;
    };
}

function getIncidentes() {
    return tablaIncidentes;
}
