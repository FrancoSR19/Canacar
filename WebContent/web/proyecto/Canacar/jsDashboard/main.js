
define([
    "recursos/js/Graficas/Graficas",
    "recursos/js/Graficas/Tablas",
    "./Util"
], function (Graficas, Tablas, Util
) {
    
     /*
     * Creacion de la barra de tiempo
     */
    var graficas;
    
    function leerData() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "../CanacarLuciad/data/allData.json"
        }).done(function(data) {
            console.log("archivo leido");
            features = Util.getFeatures(data.features);
            graficas = crearGraficas(features);
            
        }).fail(function(e) {
            console.log(e);
        });
    }
    
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = [], labels =["EMPRESA", "NOMBRE", "MOTIVO"];
    var titles = ["Empresas","Estados","Motivo del Incidentes"];
    var types = ["piechart",  "piechart", "piechart"];
    var divs = ["gMotivo", "gEmpresas", "gNombres"];
    var formatAnotattion = null;
    var options = {
            color1: '#f2f3f4',
            color2: ''
        };
    function crearGraficas(features) {
        var tablaDatos = Tablas.crearArreglo(features, "tablaCompleta", options);
        var chartOptions = getPieOptions();
        console.log(tablaDatos[0]);
        var g = [], t, i, ng = divs.length;
        for(i=0; i<ng; i++) {
            if(types[i]) {
                t = types[i];
            } else {
                t = types[0];
            }
            if( typeof labels[i] === "string") {
                g[i] = Graficas.crearGraficaIdTexto(divs[i], tablaDatos, t, labels[i], chartOptions);
            } else {
                g[i] = Graficas.crearGraficaLabelValor(divs[i], tablaDatos, t, labels[i][0], labels[i][1], formatAnotattion, chartOptions);
            }
           
            g[i] = {
                id: g[i].idGrafica,
                label: labels[i],
                tipo: t,
                div: divs[i]
            };
        }
        return  {
            graficas: g,
            tablaDatos: tablaDatos
        };
        
    }
    
    function getPieOptions() {
        return {
        //width: 350,
        //height: 220,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 25
          },
        legend: {
            position: "left",
          textStyle: {
              frontSize: 25
            //color: '#FFFFFF'
          }
        },
        /*backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 50
            //width: 270,
            //height: 180,
            //backgroundColor: "#17202a"
        },*/
        //pieSliceBorderColor: "#17202a"
      };
    }
    /*
    //actualizarTabla();
    function actualizarTabla(f) {
        var features;
        if(f) {
           features = f; 
        } else {
            var data = celulares.model.query();
            features = data.array;
        }
        
        var n = features.length;
        var tabla = '<table>#contenido</table>';
        var etiqueta = '';
        var fila = '<tr bgcolor="$COLOR"><td>Estado</td><td>Hora</td><td>Fecha</td><td style="max-width: 270px;">Token</td></tr>';
        etiqueta += fila;
        var j=0;
        for(var i=0; i<n; i++) {
            var id = features[i].properties.id;
            //var id = Shapes.getRandom(8000, 9999);
            var estado = features[i].properties.status;
            var hora = features[i].properties.hora;
            var fecha = features[i].properties.fecha;
            var color;
            var geometry = features[i].geometry;
            if(geometry.x || geometry.type === "Point") {
                if(j%2 === 0 )
                    color = '';
                else
                    color = ' #566573 ';
                etiqueta += fila.replace("Estado", estado).replace("Hora", hora).replace("Fecha", fecha).replace("Token", id).replace("$COLOR", color);
                j++;
            }
        }
        tabla = tabla.replace("#contenido", etiqueta);
        document.getElementById("tablaToken").innerHTML = tabla;
    }
*/
    leerData();
   
    var registro = false;
    $("#crearRuta").click(function() {
        if(registro===false) {
            $("#panelRegistro").fadeIn("slow");
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#pTablaToken").fadeOut("slow");
            registro=true;
            menuAbierto=false;
        } else {
            $("#panelRegistro").fadeOut("slow");
            registro=false;
        }
        
            
    });
   /*
    * Aqui utilizamos un arreglo de cadenas de caracteres para crear una serie 
    * de datos de informacion y meterlos en un select
    * <select class="selectPanel" id="zonaFiltro">
    * en el index, utilizando su id.
    * @param {type} div
    * @param {type} opciones
    * @returns {undefined}
    */
    function setOptions(div, opciones) {
        var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", "Todos").replace("$LABEL", "Todos");
        var  n = opciones.length;
        for(var i =0; i<n; i++) {
            etiqueta += base.replace("$VALUE", opciones[i]).replace("$LABEL", opciones[i]);
        }
        document.getElementById(div).innerHTML = etiqueta;
        
    }
    
    
    var X = -1, x=0, y = 30, y2 = 0;
    // Reloj
    timeUpdated();
    function timeUpdated() {
        $('#timelabel').text(formatTime());
        
        y++;
        if(y>5) {
            y=0;
            //getNumber();
        }
        setTimeout(timeUpdated,500);
        
     }
     /*
      * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
      * @returns {String}
      */
     function formatTime() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
   
    ////========================================================================================///
    var folios = [];
    function getNumber() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "https://www.olly.science/gps" 
        }).done(function(data) {
            var features = data.features;
            var nf = features.length;
            for(var i =0; i<nf; i++) {
                var feature = features[i];
                var properties = feature.properties;
                var id = properties.id;
                var coordinates = feature.geometry.coordinates;
                var fecha = properties.fecha, hora = properties.hora;
                var date = formatDate(fecha, hora);
                var numDate = new Date(date), nowDate = new Date();
                var dif = nowDate - numDate;
                
                //var circle = Shapes.createCircleByCenterPoint(referenceC, coordinates[1], coordinates[0],0, "circulo"+id, properties, 10 );
                //celulares.model.put(circle);
                switch(properties.status) {
                    case "Conectado":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Esperando";
                            Tablas.crearTablaDIV("tablaToken", folios, options);
                        }
                        break;
                    case "Desconectado":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Esperando";
                            Tablas.crearTablaDIV("tablaToken", folios, options);
                        }
                        break;
                    case "Alerta":
                        if(folios[nf-1]) {
                            folios[nf-1][2] = "Registrado";
                            Tablas.crearTablaDIV("tablaToken", folios, options);
                        }
                        alert("El veh�culo ha sido comprometido, la informaci�n ha sido guardada");
                        break;
                }
            }
            //actualizarTabla(features);
        }).fail(function(e) {
            console.log("No se contacto con el server");
            console.log(e);
        });
    }
    
    function formatDate( fecha, hora) {
        var i, j=0, y, m, d, c, newDate="";
        var baseDate = "aa/mes/dia hora:00";
        for(i=0; i<fecha.length; i++) {
            c = fecha.charAt(i);
            if(c ==='/' || c===' ' || c==='/n' || c===':') {
                switch(j) {
                    case 0: d = newDate; break;
                    case 1: m = newDate; break;
                    case 2: y = newDate; break;
                }
                newDate = "";
                j++;
            } else {
                newDate+= c;
            }
        }
        y = newDate;
        newDate = baseDate.replace("aa", y).replace("mes", m).replace("dia", d).replace("hora:00", hora);
        return newDate;
    }
    
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    crearHorario("horario");
    function crearHorario(div) {
        var nivel = [3,3,2,1,0,0,0,1,1,2,2,3,3,1,1,1,2,0,0,0,0,1,1,2,2,3,3,3,2,2,0,0,0,0,0,0,1,1,1,1,3,3,3,3,3,2,2,0,0,0,0,1,1,1,,3,3,3,2,2,1,3,3,1,1,0,0,0,0,0,0,0];
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center" bgcolor="$COLOR">$dato</td >';
            var columnas ='';
            var fila = '<tr>$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n , m ;
    
            var datos, k=0;
            for(var i=0; i<7; i++) {
                //datos = tabla[i];
                columnas += columna.replace('$dato', "<b>"+getDia(i)+"</b>").replace("$COLOR", getColor(4));
                for(var j=1;j<24;j++) {
                    var color;
                    if(k< nivel.length)
                        color= getColor(nivel[j+k]);
                    else
                        color = "";
                    k++;
                    columnas += columna.replace('$dato', j+" Hrs").replace("$COLOR", color);
                }
                k=Math.round(getRandom(0,15));
                //columna = '<td align="center">$dato</td >';
                filas += fila.replace('$columna', columnas);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    
    function getColor(id) {
        switch(id) {
            case 0 : return "#2ecc71";
            case 1: return "#f7dc6f";
            case 2: return "#f39c12";
            case 3: return "#d35400";
            case 4: return "#85929e";
            default: return "#19232c";
        }
    }
    function getDia(id) {
        switch(id) {
            case 0: return "Domingo";
            case 1: return "Lunes";
            case 2: return "Martes";
            case 3: return "Miercoles";
            case 4: return "Jueves";
            case 5: return "Viernes";
            case 6: return "Sabado";
            default: return "No Dia";
        }
    }
    
    /*
     *============================================================================================================
     *                                      CONEXIONES CON ENDPOINTS
     *============================================================================================================
     */
    
    
    getTokenStatus();
    function getTokenStatus() {
        $.ajax({
            type:'get',
            dataType: "json",
            url: "https://www.olly.science/users/token_status",
            success: function (response) {
                var users = response.users;
                var nUsers = users.length;
                var datos = [["Usuario (Email)", "Alerta", "Confirmacion","Cuestionario"]];
                for(var i=0; i<nUsers; i++) {
                    var al = users[i].alertapp;
                    var co = users[i].confirmapp;
                    var cu = users[i].questionaryapp;
                    var em = users[i].user;
                    datos [i+1] = [em, al, co, cu];
                }
                Tablas.crearTablaDIV("tablaTokenStatus", datos, options);
            }
        }).fail(function(e) {
            //console.log("No se contacto con el server");
            console.log(e);
        });
    }
    
    
});

