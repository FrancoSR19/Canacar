/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/model/feature/Feature",
], function (ShapeFactory, ReferenceProvider, Feature) {
    var baseFecha = "fecha 00:00";
    var reference = ReferenceProvider.getReference("CRS:84");
    function getFeatures(features) {
        var f = [], key = "FECHA INCI", key2 = "FECHA ROBO", fecha;
        features = corregirPropiedades(features);
        for(var i=0; i<features.length; i++) {
            var p = features[i].properties;
            var longitud = features[i].geometry.coordinates[0][0], latitud = features[i].geometry.coordinates[0][1];
            
            if(p[key]) {
                fecha = corregirFecha(p[key]);
                //fecha = p[key];
            } else {
                if(p[key2])
                    fecha = corregirFecha(p[key2]);
                else
                    console.log("No se encontro fecha para este evento "+i);
            }
            var eventTime = baseFecha.replace("fecha", fecha);
            p.EventTime = eventTime;
            f[i] = createPoint(longitud, latitud, 0, i, p);
        }
        return f;
    }
    
    function createPoint(x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    
    function corregirPropiedades(features) {
        var p, names = [[]], key, j=0, i, k=0;
        p = features[0].properties;
        for(key in p) {
            names[k][j] = key;
            j++;
        }
        k++;
        names[k] = [];
        j=0;
        for(i =1; i<features.length;i++) {
            p = features[i].properties;
            for(key in p) {
                names[k][j] = key;
                j++;
            }
            if(names[k].length !== names[k-1].length) {
                k++;
            }
            j=0;
            names[k] = [];
        }
        
        if(k===1) {
            return features;
        } else {
            var newP = {}, newNames = names[0], name, existe = false;
            k=1;
            for(i=1; i<names[k].length-1; i++) {
                name = names[k][i];
                for(j=0; j<newNames.length; j++) {
                    if(name === newNames[j] ) {
                        existe = true;
                        break;
                    } else {
                        existe = false;
                    }
                }
                if(existe === false) {
                    newNames[j] = name;
                }
            }
            var newKey;
            for(i=0; i<features.length; i++) {
                p = features[i].properties;
                for(j=0; j<newNames.length;j++) {
                    newKey = newNames[j];
                    for(key in p) {
                        if(key === newNames[j]) {
                            existe = true;
                            break;
                        } else {
                            existe = false;
                        }
                    }
                    if(existe===true) {
                        newP[newKey] = p[newKey];
                    } else {
                        newP[newKey] = null;
                    }
                }
                
                features[i].properties = newP;
                newP = {};
            }
            return features;
        }
    }
    
    function corregirFecha(fecha) {
        var nfecha = "", n = 0;
        var c, a = "", m = "", d = "";
        
        for(var j=0; j<fecha.length;j++) {
            c = fecha.charAt(j);
            if(c === '/' || (j=== (fecha.length-1))) {
                if(n===0) 
                    d = nfecha;
                else {
                    if(n===1)
                        m = nfecha;
                    else {
                        if(n===2) {
                            a = nfecha+c;
                            n=0;
                        }
                    }
                }
                nfecha = "";
                n++;
            } else {
                nfecha+=c;
            }
            
        }
        return a+'/'+m+'/'+d;
    }
    return {
        getFeatures: getFeatures
    };
});
