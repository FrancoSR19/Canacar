//#snippet runrequire
require({
  baseUrl: "../../",

  packages: [
    {name: "dojo", location: "./samples/lib/dojo"},
    //{name: "dojox", location: "./samples/lib/dojox"},
    //{name: "dijit", location: "./samples/lib/dijit"},
    {name: "luciad", location: "./luciad"},
    {name: "proyecto", location: "./proyecto/Canacar/jsDashboard"},
    {name: "js", location: "./proyecto/Canacar/js"},
    {name: "datos", location: "./proyecto/CanacarLuciad/data"},
    {name: "recursos", location: "./proyecto/recursos"}
    
    //{name: "samplecommon", location: "./samples/common"}
    
  ]
//#endsnippet runrequire
  , cache: {}
//#snippet runrequirecont
}, ["proyecto"]);