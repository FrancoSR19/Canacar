//#snippet runrequire
require({
  baseUrl: "../",

  packages: [
    {name: "dojo", location: "./samples/lib/dojo"},
    {name: "dojox", location: "./samples/lib/dojox"},
    {name: "dijit", location: "./samples/lib/dijit"},
    {name: "luciad", location: "./luciad"},
    
    {name: "proyecto", location: "./proyecto/CanacarLuciad/js"},
    {name: "data", location: "./proyecto/Canaca/data"},
    {name: "recursos", location: "./proyecto/recursos"},
    {name: "time", location: "./proyecto/recursos/js/AdvancedTimeSlider/js/time"},
    
    {name: "template", location: "./samples/template"},
    {name: "samplecommon", location: "./samples/common"},
    {name: "samples", location: "./samples"},
    {name: "file", location: "./samples/model/File/js/file"},
    {name: "d3", location: "./samples/lib/d3"}
  ]
//#endsnippet runrequire
  , cache: {}
//#snippet runrequirecont
}, ["proyecto"]);