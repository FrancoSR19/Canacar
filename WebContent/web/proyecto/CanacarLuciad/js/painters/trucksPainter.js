/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
    ], function (FeaturePainter, ShapeFactory, IconFactory) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        
        
        //console.log("dibujando ...");
        if(!feature.geometry.x) {
            geoCanvas.drawShape(shape,{ 
                    stroke: {
                        color: sAzul,
                        width: 2} 
                });
        }else {
        var icon, dimension= 50;
        switch(feature.properties.Estado) {
            case "Peligro"://alert("Ajustador Juan Carlos en Riesgo!");
                geoCanvas.drawIcon(shape.focusPoint,{
                    width: "64px",
                    height: "64px",
                    image: "data/img/alarma2.png",
                    draped: false
                });
                break;
            case "Taller":
                geoCanvas.drawIcon(shape.focusPoint,{
                    width: "64px",
                    height: "64px",
                    image: "data/img/precaucion.png",
                    draped: false
                });
                break;
            default:
            if(state.selected) 
                icon = IconFactory.circle({stroke: sBlanco, fill: sRojo, width: dimension, height: dimension});
            else 
                icon  = IconFactory.circle({stroke: sBlanco, fill: sAzul, width: dimension, height: dimension});
            geoCanvas.drawIcon(shape.focusPoint,{
                width: dimension+"px",
                height: dimension+"px",
                image: "data/img/persona.png",
                draped: false
            });
            break;
        }
        }   
    };
    layerPainter.prototype.paintLabel = function(labelCanvas, feature, shape, layer, map, paintState)
        {
            if(feature.geometry.x) {
            var texto;
            if(feature.properties.Ajustador)
                texto = feature.properties.Ajustador;
            else
                texto = "GPS"+feature.id;
            var label = "<span class='label' style='color: $color'>" + texto + "</span>";
            if(paintState.selected)
                labelCanvas.drawLabel(label.replace("$color", "rgb(200,0,200"),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", "rgb(252,255,255"),shape.focusPoint, {});
            }
        };
    
    return layerPainter;
});
                
