/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
  "luciad/geodesy/LineType",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/model/tileset/FusionTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/TrajectoryPainter",
  "luciad/view/LayerType",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/kml/KMLLayer",
  "luciad/model/kml/KMLModel",
  "luciad/view/feature/FeaturePainter",
  "luciad/util/ColorMap",
  'samplecommon/IconFactory',
  "luciad/view/style/PointLabelPosition",
  
  "recursos/js/Shapes",
  "luciad/model/store/MemoryStore",
  "./main",
  "samplecommon/LayerConfigUtil",
], function(LineType, GeoJsonCodec, FeatureModel, UrlStore, FusionTileSetModel, ReferenceProvider, ShapeFactory,
            FeatureLayer, TrajectoryPainter, LayerType, RasterTileSetLayer, KMLLayer, KMLModel, FeaturePainter,
            ColorMap, IconFactory, PointLabelPosition,
            Shapes, MemoryStore, main, LayerConfigUtil) {
    
    
    
    
    function createGlowSlopesLayer(url, dataSetStartTime, dataSetEndTime, glowColor, label) {
      var trajectoriesPainter = new TrajectoryPainter({
        properties: ["origin", "airline", "destination"],
        defaultColor: glowColor,
        selectionColor: glowColor,
        lineWidth: 6,
        lineType: LineType.SHORTEST_DISTANCE,
        timeWindow: [0, dataSetEndTime - dataSetStartTime],
        timeProvider: function(feature, shape, pointIndex) {
          return feature.properties.timestamps[pointIndex];
        },
        draped: true
      });
      //al =0;
      var trajectoryStore;
      if (url ===null) {
            trajectoryStore = new MemoryStore({});
        } else {
            trajectoryStore = new MemoryStore({ data: url });
        }
      var reference = ReferenceProvider.getReference("CRS:84");
      var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
      return new FeatureLayer(trajectoryModel, {
        label: label,
        editable: false,
        selectable: true,
        visible: true,
        painter: trajectoriesPainter
      });
    }
    
    function createGlowSlopesLayerData(data, glowColor, label, tiempoActual) {
        var features = null;
        var dataSetStartTime = tiempoActual + 600000;
        var dataSetEndTime = dataSetStartTime + 12*60*60*1000;
        var layer;
        if(data!== null) {
            features = crearFeatureTime(data, ReferenceProvider.getReference("CRS:84"), 
                dataSetStartTime);
        }
        layer = createGlowSlopesLayer(features, dataSetStartTime,dataSetEndTime, glowColor, label);
        //map.layerTree.addChild(layer);
                    //console.log(features);
        return layer;      
        //setTrayectoryGlowLayer(layer, features);
        
    }
    
    
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function crearFeatureTime (data, id) {
        var i=0, j, n,m=0,v=getRandom(0,10),x=[],y=[], features,timestamps=[], eventTime=[], velocities =[],
                properties, feature, coordenada, distancia, tiempoAtencion, avance, startTime = getActualTime();
        var sFecha = new Date(startTime).toGMTString(), eFecha, nFecha = startTime, reference = ReferenceProvider.getReference("CRS:84");
        var sTime, eTime, duration, sDuration;
        
        //for(i=0; i<data.length;i++) {
            feature = data;
            avance =0;
            //if(feature.geometry.type === 'LineString') {
                coordenada = feature.geometry.coordinates;
                n= coordenada.length;
                sTime =startTime ;
                duration = feature.properties.duration*1000000;
                sDuration = formatDate(new Date(duration));
                
                eTime = sTime + duration;
                
                tiempoAtencion = duration;
                sFecha = formatDate(new Date(sTime));
                eFecha = formatDate(new Date(eTime));
                //sFecha = new Date(sTime).toGMTString();
                nFecha = sTime;
                avance = duration/n;
                properties = feature.properties;
                for(j=0; j<n; j++) {
                    if(j<n) {
                        x[j] = coordenada[j][0];
                        y[j] = coordenada[j][1];
                    }
                    eventTime [j]= sFecha;
                    timestamps[j] = nFecha;
                    velocities[j] = v;
                    v = getRandom(5, 15);
                    nFecha += avance;
                    sFecha = formatDate(new Date(nFecha));
                    //sFecha = new Date(nFecha).toGMTString();
                }
                var hours = Math.floor( tiempoAtencion / 3600000 );  
                var minutes = Math.floor( (tiempoAtencion % 3600000) / 60000 );
                var seconds = tiempoAtencion % 60000;
                //console.log(eventTime);
                var propiedades ={
                    Summary: properties.summary,
                    Distancia: properties.distance,
                    Ajustador: properties.Ajustador,
                    Duracion: duration,
                    timestamps: timestamps,
                    EventTime: eventTime,
                    velocities: velocities,
                    endTime: eventTime[eventTime.length -1]
                };
                //properties = properties.menu.concat(feature.properties);
                
                features = Shapes.createPolyline(reference, x, y, 0, id, propiedades);
                eventTime = [];
                timestamps =[];
                velocities =[];
                x=[];
                y=[];
                v=getRandom(0,10);
            
        
        return features;
    }
    
    function formatDate(date) {
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
    }
    
    return {
        createGlowSlopesLayerData: createGlowSlopesLayerData,
        crearFeatureTime: crearFeatureTime
    };
});

