var map, actualTime;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "recursos/js/Shapes",
    "luciad/shape/ShapeFactory",
    
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    "./painters/layerPainter",
    "./painters/MunicipiosPainter",
    "./DefaultBalloon",
    "recursos/js/Util",
    'luciad/util/Promise',
    
    "recursos/js/TimeChart",
    "recursos/js/Graficas/Graficas",
    "./tablas",
    "./painters/EstadosPainter",
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on, Shapes, ShapeFactory,
        DojoMap, ReferenceProvider, LayerConfigUtil, layerPainter, MunicipiosPainter, DefaultBalloon, util, Promise,
        TimeChart, Graficas, tablas, EstadosPainter) {
    
    var referenceC = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    
    
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Aerial"});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Aerial"});
        } 
        CrearCapas();
    }
    //=================================== CAPAS ===============================================
    var estados15, estados16, estados17, municipiosVeracruz;
    var Features;
    function CrearCapas() {
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        //var mexicoLayer = LayerFactory.createKmlLayer({label: "Mexico", selectable: false}, '../recursos/estados.kml');
        /*var mexicoLayer = LayerFactory.createWFSLayer(referenceC, 
            {label:"Mexico", painter: new layerPainter()}, 
            "http://localhost:8081/ogc/wfs/estadosmexicowfs", "546");
        map.layerTree.addChild(mexicoLayer);
        
        
        municipios = LayerFactory.createWFSLayer(referenceC, 
            {label:"Municipios", painter: new MunicipiosPainter(), selectable: true}, 
            "http://localhost:8081/ogc/wfs/municipiosmexicowfs", "555");
        map.layerTree.addChild(municipios);
        municipios.balloonContentProvider = function(feature) {
            return DefaultBalloon.getBalloon(feature);
        };
        */
        
        estados15 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2015", id: "estados5", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados15);
        datosEstados(estados15, 2015);
        estados16 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2016", id: "estados6", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados16);
        datosEstados(estados16, 2016);
        estados17 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2017", id: "estados7", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados17);
        datosEstados(estados17, 2017);
        
        municipiosVeracruz = LayerFactory.createMemoryLayer(referenceC, {
            label: "Veracruz", id: "municipios", visible: false, selectable: true, editable: true, painter: new MunicipiosPainter() 
        });
        map.layerTree.addChild(municipiosVeracruz);
        
        addProperties(municipiosVeracruz);
        var queryFinishedHandle = municipiosVeracruz.workingSet.on("QueryFinished", function() {
            if(municipiosVeracruz.bounds) 
                map.mapNavigator.fit({bounds: municipiosVeracruz.bounds, animate: true});
            else {
                var coordenadas = [-96.94186764996043,0, 16.8100862892485, 5.2];
                util.fitCoordinates(map, coordenadas);
            }
            queryFinishedHandle.remove();
        });
    }
    var ti = 0, p = false;
    /* 
     * =============================================================================================================
     *                                          FUNCION PARA EL RELOJ   
     * @returns {undefined}
     * =============================================================================================================
     */
    //timeUpdated();
    function timeUpdated() {
        ti++;
        if(ti >= 10 && p ===false) {
            p= true;
            addProperties(municipiosVeracruz);
        }
        $('#timelabel').text(util.formatDate() +" "+util.formatTime());
        setTimeout(timeUpdated,1000);
     }
     
     function addProperties(layer) {
         $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/Veracruz.json"
        }).done(function(data) {
         /*
         var featuresPromise = layer.model.query();
         console.log("Features ");
         Promise.when(featuresPromise, function(cursor) {
            var features = [];
            var index = 0;
            while (cursor.hasNext()) {
                var quake = cursor.next();
                features[index] = quake;
                index++;
            }
            console.log("Loading " + index + " events");
                    */
            Features = data.features;
            var index = Features.length;
            var delitos = ["DelitoA","DelitoB","DelitoC","DelitoD", "DelitoE","DelitoF","DelitoG"];
            for(var i=0; i<index; i++) {
                var feature = Features[i];
                var properties = feature.properties;
                var coordenates = feature.geometry.coordinates;
                for(var j=0; j<delitos.length; j++) {
                    var values = [];
                    for(var k=0;k<12;k++) {
                        values[k] = Math.round(util.getRandom(0, 100));
                    }
                    properties[delitos[j]] = values;
                    properties["year"] = 2017;
                }
                feature = Shapes.createPolygonCoordinates(referenceC, coordenates, i, properties);
                layer.model.add(feature);
                Features[i] = feature;
                
                //crearGraficas();
            }
         }).fail(function(e) {
            console.log(e);
        });
     }
     function datosEstados(layer, year){
         $.ajax({
            type:'Get',
            dataType: "json",
            url: "../recursos/estados.json"
        }).done(function(data) {
            var features = data.features;
            var nFeatures = features.length;
            $.ajax({
                type:'Get',
                dataType: "json",
                url: "data/Nacional"+year+".json"
            }).done(function(data) {
                var datos = data.features;
                var nDatos = datos.length;
                var newFeature, coordinates, properties;
                for(var i=0; i<nDatos; i++) {
                    var id = (parseInt(datos[i].id)+1)+"";
                    id = id<10? "0"+id: id;
                    for(var j=0; j<nFeatures; j++) {
                        var featureName = features[j].properties.name;
                        if(featureName === id) {
                            break;
                        }
                    }
                    coordinates = features[j].geometry.coordinates;
                    if(!coordinates){
                        var f = features[j];
                        var g = f.geometry;
                        if(g.geometries.length>1) {
                            g = g.geometries;
                            var mayor = g[0].coordinates[0].length; 
                            var l=0;
                            for(var k=1;k<g.length; k++) {
                                var n = g[k].coordinates[0].length;
                                if(n > mayor) {
                                    mayor = n;
                                    l=k;
                                }
                            }
                            coordinates = g[l].coordinates;
                        } else {
                            coordinates = g.geometries[0].coordinates;
                        }
                        //alert("Geometria desconosida");
                    }
                    properties = datos[i].properties;
                    properties["year"] = year;
                    newFeature = Shapes.createPolygonCoordinates(referenceC, coordinates, id, properties);
                    layer.model.add(newFeature);
                }
            });
        }).fail(function(e) {
            console.log(e);
        });
     }
     /*
     *============================================================================================================
     *                                      SELECCION EN EL MAPA
     *============================================================================================================
     */
    var selectedFeature, selectedChart = null;
    map.on("SelectionChanged", function (event) {
        selectedFeature = null;
        if (event.selectionChanges.length > 0 && event.selectionChanges[0].selected.length > 0 ) {
            selectedFeature = event.selectionChanges[0].selected[0];
            $("#BalloonPanel").fadeIn("slow");
            //var content = DefaultBalloon.getBalloon(selectedFeature);
            var content = '<label>'+selectedFeature.properties.name+'</label> \n\
<div id="graficaContenido"></div>';
            document.getElementById("contentBalloon").innerHTML = content;
            var datos = tablas.tablaActual(selectedFeature, getDetailTime());
            if(datos.sumaTotal ===0) {
                document.getElementById("contentBalloon").innerHTML += "<p>No ocurrieron delitos en este periodo</p>";
            }else 
                selectedChart = Graficas.crearGraficaTabla("graficaContenido", datos.tabla, "piechart", "Delitos");
        } else {
            $("#BalloonPanel").fadeOut("slow");
            selectedChart = null;
        }
    });
    $('#cerrarBalloon').click(function () {
        $('#BalloonPanel').fadeOut('slow');
        selectedChart = null;
    });
     
     /*
     * 
     * @type TimeChart
     */
    var dataSetStartTime = Date.parse("2015/01/01 00:00:00") / 1000;//1421143477; // Tue Jan 13 11:04:37 CET 2015
    var dataSetEndTime = Date.parse("2018/01/01 00:00:00") / 1000;
    var speedup = 3000; //speedup
    var lasttime = Date.now();
    var playing = false;
    var timeChart = new TimeChart(dom.byId("timeChart"), new Date(dataSetStartTime * 1000), new Date(dataSetEndTime * 1000));
    wireListeners();
    setTime(0);
    
    function timeUpdated() {
        actualTime = timeChart.getCurrentTime().getTime();
        $('#timelabel').text(util.formatDate(actualTime));
        municipiosVeracruz.filter = function (feature) {
            return true;
        };
        estados15.filter = function (feature) {
            return true;
        };
        estados16.filter = function (feature) {
            return true;
        };
        estados17.filter = function (feature) {
            return true;
        };
        if(selectedChart && selectedFeature) {
            var nuevaTabla = tablas.tablaActual(selectedFeature, getDetailTime());
            Graficas.actualizarGrafica(selectedChart.idGrafica, nuevaTabla, "piechart");
        }
     }
     
     function wireListeners() {
    //timechart
        timeChart.addInteractionListener(timeUpdated);
        timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
            speedup *= prevZoomLevel / currZoomLevel;
        });

        $("#play").click(function() {
            lasttime = Date.now();
            play(!playing);
        });

        //expose navigateTo on window so it can be called from index.html
        window.navigateTo = function(moveTo) {
            var wgs84 = ReferenceProvider.getReference("EPSG:4326");
            var center = ShapeFactory.createPoint(wgs84, [moveTo.lon, moveTo.lat]);
            var radius = moveTo.radius || 5.0;
            var bounds = ShapeFactory.createBounds(wgs84, [center.x - radius, 2 * radius, center.y - radius, 2 * radius]);
            return map.mapNavigator.fit(bounds);
        };
    }
    
    function playStep() {
        if (playing) {
            var currTime = Date.now();
            var deltaTime = (currTime - lasttime);
            lasttime = currTime;
            var timeChartTime = timeChart.getCurrentTime().getTime();
            var newTime = timeChartTime + (deltaTime) * speedup;
            if (newTime >= timeChart.endDate.getTime()) {
            newTime = timeChart.startDate.getTime();
        }
        timeChart.setCurrentTime(new Date(newTime));
        window.requestAnimationFrame(playStep);
        }
    }

    function play(shouldPlay) {
        playing = shouldPlay;
        if (playing) {
            playStep();
        }
        var playBtn = $('#play');
        playBtn.toggleClass("active", playing);
        playBtn.find("> span").toggleClass("glyphicon-pause", playing);
        playBtn.find("> span").toggleClass("glyphicon-play", !playing);
    }

    function setTime(milliseconds) {
        timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + milliseconds));
        timeUpdated();
        
    }
    
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = ["filtroDelitos", "filtroMunicipios"], lab =["DelitoA","DelitoB","DelitoC","DelitoD", "DelitoE","DelitoF","DelitoG"];
    var titles = ["Empresas","Estados","Motivo del Incidentes"];
    var types = ["piechart",  "piechart", "piechart"];
    var gEmpresas={}, gNombres={}, gMotivo={};
    function crearGraficas(features) {
        var datos = Graficas.crearGraficasFeatures(features, "Graficas", lab, types, fil, null, titles);
        var graficas = datos.graficas;
        tablaDatos = datos.tablaDatos;
        datos = null;
        return graficas;
        
    }
    
});


function getTime() {
    return actualTime;
}

function getDetailTime() {
    var date = new Date(actualTime);
    var m = date.getMonth();
    var y = date.getFullYear();
    return {month: m, year: y};
}
