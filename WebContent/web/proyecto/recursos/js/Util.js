/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
], function (ShapeFactory, ReferenceProvider) {
    var referenceC = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
    return {
        /*
         * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
         * @returns {String} Hora : Minutos : Segundos
         */
        formatTime: function (time) {
            var date = time? new Date(time) : new Date();
            var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
            var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
            var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
            return h+":"+m+":"+s;
        },
        /*
         * 
         * @returns {String} Mes / Dia / Año
         */
        formatDate: function (time) {
            var date = time? new Date(time) : new Date();
            var y = date.getFullYear();
            var mo = date.getMonth()<10? "0"+date.getMonth(): date.getMonth();
            var d = date.getDate() <10? "0"+date.getDate(): date.getDate();
            return mo+"/"+d+"/"+y;
        },
        
        fitBounds: function (map, bounds, animate) {
            map.mapNavigator.fit({bounds: bounds, animate: animate});
        },
        /*
         * Formato de las coordenadas:
         *      [x, ancho, y, altura, z, profundidad] o [x, ancho, y, altura]
         */
        fitCoordinates: function (map, coordinates, animate) {
            var bounds = ShapeFactory.createBounds(referenceC, coordinates);
            map.mapNavigator.fit({bounds: bounds, animate: animate});
        }, 
        getRandom: function (min, max) {
            return Math.random() * (max - min) + min;
        }
    };
});

