define([
  "luciad/model/tileset/FusionTileSetModel",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/LayerType"
], function(FusionTileSetModel, RasterTileSetLayer, LayerType) {

  return {

    /**
     * Creates a layer for LuciadFusion raster tile coverages.
     *
     * Summary:
     * - Create a {@link luciad/model/tileset/FusionTileSetModel} with all details about the coverage
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     */

    createLayer: function(layerName, options) {

      var model = new FusionTileSetModel(options);

      var layer = new RasterTileSetLayer(model, {label: layerName + " (Fusion)", layerType: LayerType.BASE});

      return layer;
    }
  };

});