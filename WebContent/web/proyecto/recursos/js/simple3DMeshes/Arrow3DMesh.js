define(["./Cone3DMesh",
        "./Cylinder3DMesh"], function(Cone3DMesh, Cylinder3DMesh) {

  /**
   * Creates a 3D arrow mesh with the given dimensional parameters.
   * A 3D arrow is composed of two parts:
   *   - A stick (cylindrical shape)
   *   - A tip (conic shape)
   *
   * @param stickRadius the radius of the arrow stick
   * @param stickHeight the height of the arrow stick
   * @param tipBaseRadius the radius of the arrow tip's bottom base
   * @param tipTopRadius the radius of the arrow tip's top base
   * @param tipHeight the height of the arrow tip
   * @param sliceCount the number of slices (subdivisions) of the side surface of the stick and the tip
   */

  function Arrow3DMesh(stickRadius, stickHeight, tipBaseRadius, tipTopRadius, tipHeight, sliceCount) {
    this._cylinder3DMesh = new Cylinder3DMesh(stickRadius, stickHeight, sliceCount);
    this._cone3DMesh = new Cone3DMesh(tipBaseRadius, tipTopRadius, tipHeight, sliceCount);

    // Adjust the z value of the stick and tip so that they are properly connected
    this._cylinder3DMesh.zOffset = -(stickHeight / 2);
    this._cone3DMesh.zOffset = tipHeight / 2;
  }

  Arrow3DMesh.prototype.createVertices = function() {
    return this._cylinder3DMesh.createVertices().concat(this._cone3DMesh.createVertices());
  };

  Arrow3DMesh.prototype.createIndices = function() {
    var arrowIndices = this._cylinder3DMesh.createIndices();
    var firstIndexForArrowTip = Math.max.apply(null, arrowIndices) + 1;

    var tipIndices = this._cone3DMesh.createIndices();
    for (var i = 0; i < tipIndices.length; i++) {
      arrowIndices.push(tipIndices[i] + firstIndexForArrowTip);
    }
    return arrowIndices;
  };

  Arrow3DMesh.prototype.createNormals = function() {
    return this._cylinder3DMesh.createNormals().concat(this._cone3DMesh.createNormals());
  };

  Arrow3DMesh.prototype.createTextureCoordinates = function() {
    return this._cylinder3DMesh.createTextureCoordinates().concat(this._cone3DMesh.createTextureCoordinates());
  };

  return Arrow3DMesh;
});
