/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/shape/ShapeType",
    "luciad/model/feature/Feature"
], function (ShapeFactory, ShapeType, Feature) {
    
    function createPolygonRandom (reference, rangoX, rangoY, rangoZ, n) {
        var x=[], y=[], z=[], poligonos = [], puntas;
        if(rangoZ === 0 || rangoZ === "undefined") {
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        for(var i=0;i<n;i++) {
            puntas = getRandom(4, 11);
            for(var j=0;j<puntas;j++) {
                x[j] = getRandom(rangoX.min, rangoX.max);
                y[j] = getRandom(rangoY.min, rangoY.max);
            }
            poligonos[i] = createPolygon(reference, x, y, z, i, {});
        }
        return poligonos;
    }
    
    function createRandomPointWithTime(reference, rangoX, rangoY, rangoZ, n, startDate, endDate) {
        var x, y, z, k,a, poligonos = [], properties, fechas=startDate, fechaS, avance =0;
        if(rangoZ === 0 || rangoZ === "undefined") {
            z =0;
        }
        if(n>30)
            a=30;
        else
            a=n;
        fechaS = new Date(fechas*1000).toGMTString();//.toUTCString();
        avance = (endDate - startDate)/a;
        //console.log(fechaS + "\n"+ fechas+"\n"+avance);
        k=0;
        
        for(var j=0;j<a;j++) {
            for(var i=0;i<n;i++) {
                x = getRandom(rangoX.min, rangoX.max);
                y = getRandom(rangoY.min, rangoY.max);
                properties={EventTime: fechaS};
                poligonos[k] = createPoint(reference, x, y, z, k, properties);
                k++;
            }
            fechas+=avance;
            fechaS = new Date(fechas*1000).toGMTString();
        }
        return poligonos;
    }
    /*
     * 
     * @param {type} min
     * @param {type} max
     * @returns {Number}
     */
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function createRandomPoint(reference, rangoX, rangoY, rangoZ, id, properties, n) {
        var x, y, z, k,a, poligonos = [];
        
        if(!n || n ===0) {
            n=1;
        }
        k=0;
        for(var i=0;i<n;i++) {
                x = getRandom(rangoX.min, rangoX.max);
                y = getRandom(rangoY.min, rangoY.max);
                if(rangoZ === 0 || rangoZ === "undefined") {
                    z =0;
                } else {
                    z = getRandom(rangoZ.min, rangoZ.max);
                }
                poligonos[i] = createPoint(reference, x, y, z, id, properties);
                id++;
            
        }
        return poligonos;
    }
    
    function movePolygon(polygon, direction) {
        
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createPolygon(reference, x, y, z, i, properties) {
        if(z === 0) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolygon(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createPolyline(reference, x, y, z, i, properties) {
        if(z === 0 || z === undefined) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolyline(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @returns {Array}
     */
    function multiPoints(reference, x, y, z) {
        var points = [];
        for(var i=0; i<x.length;i++) {
            points[i] = ShapeFactory.createPoint(reference, [x[i], y[i], z[i]]);
        }
        return points;
    }
    
    function createPoint(reference, x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    
    function crearFeatureTime (data, reference, startTime, endTime) {
        var i, j, n,m=0,v=getRandom(0,10),x=[],y=[], features = [],timestamps=[], eventTime=[], velocities =[],
                properties, feature, coordenada, distancia, tiempoAtencion, avance;
        var sFecha = new Date(startTime).toGMTString(), nFecha = startTime;
        var sTime, eTime;
        
        for(i=0; i<data.length;i++) {
            feature = data[i];
            avance =0;
            if(feature.geometry.type === 'LineString') {
                //avance = 1200000;
                coordenada = feature.geometry.coordinates;
                n= coordenada.length;
                if(feature.properties.startTime ) 
                    sTime = Date.parse(feature.properties.startTime);
                else 
                    sTime =startTime;
                if(feature.properties.endTime)
                    eTime = Date.parse(feature.properties.endTime);
                else {
                    eTime = endTime;
                }
                tiempoAtencion = eTime - sTime;
                sFecha = formatDate(new Date(sTime));
                //sFecha = new Date(sTime).toGMTString();
                nFecha = sTime;
                avance = (eTime - sTime)/n;
                properties = feature.properties;
                for(j=0; j<n; j++) {
                    if(j<n) {
                        x[j] = coordenada[j][0];
                        y[j] = coordenada[j][1];
                    }
                    eventTime [j]= sFecha;
                    timestamps[j] = nFecha;
                    velocities[j] = v;
                    v = getRandom(5, 15);
                    nFecha += avance;
                    sFecha = formatDate(new Date(nFecha));
                    //sFecha = new Date(nFecha).toGMTString();
                }
                var hours = Math.floor( tiempoAtencion / 3600000 );  
                var minutes = Math.floor( (tiempoAtencion % 3600000) / 60000 );
                var seconds = tiempoAtencion % 60000;
                //console.log(eventTime);
                var propiedades = Object.assign({
                    timestamps: timestamps,
                    EventTime: eventTime,
                    TiempoTotal: hours+" hr. "+minutes+" min. "+seconds+" seg.",
                    velocities: velocities,
                    //startTime: sTime,
                    endTime: eventTime[eventTime.length -1]
                }, properties);
                //properties = properties.menu.concat(feature.properties);
                
                features[i] = createPolyline(reference, x, y, 0, i, propiedades);
                eventTime = [];
                timestamps =[];
                velocities =[];
                x=[];
                y=[];
                v=getRandom(0,10);
            }
        }
        return features;
    }
    function formatDate(date) {
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
    }
    
    function distancia(x1, y1, x2, y2 ) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }
    
    function createCircleByCenterPoint (reference, x, y, z, i, properties) {
        return ShapeFactory.createCircleByCenterPoint(reference, ShapeFactory.createPoint(reference, [x, y]), 1000);
    }
    
    function distancia(x1, y1, x2, y2 ) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }
    /*
     * 
     * @param {type} reference
     * @param {type} data
     * @param {type} ids
     * @returns {Array}
     */
    function crearRuta(reference, data, ids) {
        var i,j, k, x=[],y=[];
        var route, coordenadas, properties, features = [];
        console.log("creando ruta");
        if(data.routes) {
            for(i=0; i<data.routes.length;i++) {
                route = data.routes[i];
                for(j=0;j<route.length;j++) {
                    coordenadas = route[j].geometry.coordinates;
                    for(k=0;k<coordenadas.length;k++){ 
                        x[k] = coordenadas[k][0];
                        y[k] = coordenadas[k][1];
                    }
                    properties = {
                        cost: route[j].cost,
                        distance: route[j].distance,
                        duration: route[j].duration,
                        summary: route[j].summary
                    };
                }
                
                features[i] = createPolyline(reference, x, y, 0, i, properties);
                ids++;
                properties = {};
                x=[];
                y=[];
                route=[];
            }
        }
        return features;
    }
    /*
     * 
     * @param {type} reference
     * @param {type} coordinates
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createFeaturePolygon(reference, coordinates, i, properties) {
        var x = new Array(),y= new Array(),z = new Array(), c;
        //console.log("creado feature");
        if(coordinates.length === 1) {
            try {
                x[0] = coordinates[0][0][0];
                c = coordinates[0];
            }catch(e) {
                c = coordinates;
            }
            
        } else
            c = coordinates;
        for(var l=0; l<c.length; l++) {
            x[l] = c[l][0];
            y[l] = c[l][1];
            if(c[l].length ===3)
                z[l] = c[l][2];
            else
                z[l] = 0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolygon(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    
    return {
        createPoint: createPoint,
        createPolygon: createPolygon,
        createPolygonRandom: createPolygonRandom,
        createRandomPoint: createRandomPoint,
        createRandomPointWithTime: createRandomPointWithTime,
        crearFeatureTime: crearFeatureTime,
        createCircleByCenterPoint: createCircleByCenterPoint,
        crearRuta: crearRuta,
        createFeaturePolygon: createFeaturePolygon
    };
});

