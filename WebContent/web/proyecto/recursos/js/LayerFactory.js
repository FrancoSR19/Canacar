
define([
    "luciad/view/LayerType",
    "luciad/model/kml/KMLModel",
    "luciad/view/kml/KMLLayer",
    "luciad/view/feature/BasicFeaturePainter",
    
    "luciad/model/store/UrlStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "luciad/view/feature/FeaturePainter",
    
    "luciad/model/tileset/UrlTileSetModel",
    "luciad/view/tileset/RasterTileSetLayer",
    "dojo/request",
    "dojo/_base/lang",
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    
    "luciad/view/tileset/WMSTileSetLayer",
    "./CustomJsonCodec",
    //"cop/store/RestStore",
    "samples/common/store/RestStore",
    
    "luciad/view/feature/TrajectoryPainter",
    "luciad/geodesy/LineType",
    "luciad/model/codec/GeoJsonCodec",
    "./LocalFileStore",
    "luciad/model/codec/GMLCodec",
    "luciad/model/tileset/FusionTileSetModel",
    
    "./FusionFactory/WFSDataLoader",
    "./FusionFactory/WMSDataLoader",
    "./FusionFactory/WMTSDataLoader",
    "./Shapes",
    "luciad/model/store/MemoryStore"
    
], function (LayerType, KMLModel, KMLLayer, BasicFeaturePainter, 
        UrlStore, FeatureModel, FeatureLayer,FeaturePainter,
        UrlTileSetModel, RasterTileSetLayer, dRequest, dLang, ShapeFactory, ReferenceProvider,
        WMSTileSetLayer, CustomJsonCodec, RestStore,
        TrajectoryPainter, LineType, GeoJsonCodec, LocalFileStore, GMLCodec, FusionTileSetModel,
        WFSDataLoader, WMSDataLoader, WMTSDataLoader, Shapes, MemoryStore) {
    /*
     * createKmlLayer
     * Como indica su nombre esta funcion creara una capa de una url o un archivo con formato KML.
     * @param {type} urlOrFile
     * @param {type} label
     * @param {type} selectable
     * @returns {KMLLayer}
     */
    function createKmlLayer(urlOrFile, label, selectable) {
        var store;
        var codec = new GeoJsonCodec({generateIDs: true});
        if (typeof urlOrFile === 'string') {
            store = urlOrFile;
        } else {
            store = new LocalFileStore(urlOrFile, codec);
        }
        var kmlModel= new KMLModel(store);
        return new KMLLayer(kmlModel, {layerType: LayerType.DYNAMIC,
            label: label,
            selectable: selectable,
            editable: false});
    }
    /*
     * createXMLLayer
     * Como su nombrelo dice esta funcion se encarga de crear una capa utilizando una url o archivo en formato XML.
     * @param {type} reference
     * @param {type} urlOrFile
     * @param {type} options
     * @returns {FeatureLayer}
     */
    function createXMLLayer(reference, urlOrFile, options) {
        var codec = new GMLCodec({});
        var store;
        if (typeof urlOrFile === 'string') {
            store = new UrlStore({target: urlOrFile, codec: codec});
        } else {
            store = new LocalFileStore(urlOrFile, codec);
        }
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    /*
     * createFeatureLayer
     * Esta funcion esta encargada de crar una capa utilizando una url de un archivo json o directamente un archivo JSON.
     * al no tener un archivo ni una url, la capa se creara utilizando el MemoryStore, al cual se le podra ingresar informacion despues.
     * 
     * @param {type} reference
     * @param {type} options
     * @param {type} urlOrFile
     * @returns {FeatureLayer}
     */
    function createFeatureLayer(reference, options, urlOrFile) {
        var codec = new CustomJsonCodec();
        var store;
        if(urlOrFile) {
            if (typeof urlOrFile === 'string') {
                store = new RestStore({target: urlOrFile, codec: codec});
                if(!options.label)
                    options.label = urlOrFile.toLowerCase().replace(/.*\./, "");;
            } else {
                store = new LocalFileStore(urlOrFile, codec);
            }
        } else {
            store = new MemoryStore({codec: codec});
        }
    
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    /*
     * createMemoryLayer
     * esta funcion crea una nueva capa utilizando unicamente MemoryStore, por lo 
     * que no es posible utilizar una urle, si no que debe utilizarce un arreglo con features.
     * Tambien es posible crear esta capa vacia, sin ningun dato.
     * @param {type} reference
     * @param {type} options
     * @param {type} data
     * @returns {LayerFactoryL#38.FeatureLayer}
     */
    function createMemoryLayer(reference, options, dataorUrl) {
        var codec = new CustomJsonCodec();
        var store, features = false;
        if(dataorUrl) {
            if (typeof dataorUrl === 'string') {
                features = true;
                store = new MemoryStore({codec: codec});
            } else {
                store = new MemoryStore({codec: codec, data: dataorUrl});
            }
        } else  {
            store = new MemoryStore({codec: codec});
        }
        var featureModel = new FeatureModel(store, {reference: reference});
        var layer = new FeatureLayer(featureModel, options);
        if(features === false) {
            return layer;
        } else {
            createFeaturesFromUrl(dataorUrl, reference, layer);
            return layer;
        }
    }
    function createFeaturesFromUrl(url, reference, layer) {
        $.ajax({
                type:'Get',
                dataType: "json",
                url: url
        }).done(function(data) {
            console.log("archivo leido " + url);
            if(data.features) {
                var feature, i,n = data.features.length, type, shape, properties, coordinates;
                var x,y,z;
                for(i=0; i<n;i++ ) {
                    feature = data.features[i];
                    type = feature.geometry.type;
                    properties = feature.properties;
                    coordinates = feature.geometry.coordinates;
                    if(coordinates[0].length) {
                        coordinates = coordinates[0];
                        x = new Array();
                        y = new Array();
                        z = new Array();
                        for(var j=0;j<coordinates.length;j++) {
                            x[j] = coordinates[j][0];
                            y[j] = coordinates[j][1];
                            z[j] = 0;
                        }
                    } else {
                        if(coordinates.length<3) {
                            x=coordinates[0];
                            y = coordinates[1];
                            z = coordinates[2];
                        }
                    }
                    
                    switch(type) {
                        case "Polygon": shape = Shapes.createPolygon(reference, x,y,z,i,properties);
                            break;
                        case "Polyline": shape = Shapes.createPolyline(reference, x,y,z,i,properties);
                            break;
                        case "Point": shape = Shapes.createPoint(reference, coordinates[0],coordinates[1],0,i, properties);
                            break;
                        default: console.log("no se reconoce el tipo de figura "+type);
                    }
                    layer.model.add(shape);
                }
            }
            
        }).fail(function(e) {
            console.log("No se leyo el archivo  "+url);
            console.log(e);
        });
    }
    /*
     * createCustomMapaLayer
     * Esta funcion como su nombre lo dice se encarga de crear una nueva capa con un set de imagenes personalizadas,
     * de preferencia estas imagenes deben tener un tamaño de 256 por 256 pixeles, y estas seran acopladas en el mapa 
     * de luciad cubriendo todo el mapa.
     * @param {type} reference
     * @param {type} url
     * @param {type} label
     * @returns {LayerFactoryL#38.RasterTileSetLayer}
     */
    function createCustomMapaLayer(reference, url, label) {
        var options = {
            baseURL: url+"/{z}_{x}_{y}.png", // "../recursos/Maps/SanPedro/{z}_{x}_{y}.png",
            name: "CustomMap",
            forma: "image/png",
            levelCount:17,
            level0Rows:2,
            level0Columns:4,
            tileWidth:256,
            tileHeight:256,
            reference: reference,
            bounds:[-180,360,-90,180],
            elevation: true
        };
        options.bounds = ShapeFactory.createBounds(options.reference, options.bounds);
        var model = new UrlTileSetModel(options);
        return new RasterTileSetLayer(model, //WMSTileSetLayer   RasterTileSetLayer
                {label: label, layerType: LayerType.BASE}
        );
    }
    /*
     * createMapImage
     * Con esta funcion podremos crerar un nuevo mapa personalizado, utilizando un set 
     * de imagenes personalizadas, las cuales pueden tener las dimenciones necesarias 
     * y el numero de imagenes que se necesite.
     * Un punto importante es que para utilizar esta funcion es necesario el parametro 
     * image, es un objeto el cual debe de contener la siguiente informacion:
     *  image = {
     *      heigth,
     *      width,
     *      x,
     *      y,
     *      x2,
     *      y2
     *  }
     * heigth y eidth representan las dimensiones de las imagenes utilizadas en pixeles.
     * X representa la longitud y Y la latitud el punto superior derecho del mapa y X2 y Y2
     * representan la longitud y latitud del punto inferior derecho del mapa.
     * @param {type} reference
     * @param {type} url
     * @param {type} label
     * @param {type} image
     * @returns {LayerFactoryL#38.RasterTileSetLayer}
     */
    function createMapImage(reference, url, label, image) {
        var x = (image.x - image.x2), y = (image.y - image.y2);
        if(x<0)
            x = x*(-1);
        if(y<0)
            y = y*(-1);
        var options = {
            baseURL: url+"/{z}_{x}_{y}.png", // "../recursos/Maps/SanPedro/{z}_{x}_{y}.png",
            name: "Massachusetts image@terrain",
            forma: "image/png",
            levelCount:1,
            level0Rows:1,
            level0Columns:1,
            tileWidth: image.width ? image.width : 256,
            tileHeight:image.heigth ? image.height : 256,
            reference: reference,
            bounds:[image.x, x, image.y, y],
            elevation: true
        };
        options.bounds = ShapeFactory.createBounds(options.reference, options.bounds);
        var model = new UrlTileSetModel(options);
        return new RasterTileSetLayer(model, //WMSTileSetLayer   RasterTileSetLayer
                {label: label, layerType: LayerType.BASE}
        );
    }
    /*
     * createLayerImage
     * Con esta funcion podremos crerar un nuevo mapa personalizado, utilizando una imagen, 
     * a diferencia de las funciones anteriores donde se utilizaba un set de imagenes, aqui solo se utiliza una imagen 
     * la cual puede tener las dimenciones necesarias .
     * Un punto importante es que para utilizar esta funcion es necesario el parametro 
     * image, es un objeto el cual debe de contener la siguiente informacion:
     *  image = {
     *      heigth,
     *      width,
     *      x,
     *      y,
     *      x2,
     *      y2
     *  }
     * heigth y eidth representan las dimensiones de las imagenes utilizadas en pixeles.
     * X representa la longitud y Y la latitud el punto superior derecho de la imagen y X2 y Y2
     * representan la longitud y latitud del punto inferior derecho de la imagen.
     * @param {type} reference
     * @param {type} url
     * @param {type} label
     * @param {type} image
     * @returns {LayerFactoryL#38.RasterTileSetLayer}
     */
    function createLayerImage(reference, url, label, image) {
        var x = (image.x - image.x2), y = (image.y - image.y2);
        if(!image.bounds) {
            if(x<0)
                x = x*(-1);
            if(y<0)
                y = y*(-1);
        }
        var options = {
            baseURL: url,
            name: "Map Image",
            forma: "image/png",
            levelCount:1,
            level0Rows:1,
            level0Columns:1,
            tileWidth: image.width ? image.width : 256,
            tileHeight:image.heigth ? image.height : 256,
            reference: reference,
            bounds: image.bounds? image.bounds : [image.x, x, image.y, y],
            elevation: true
        };
        if(!image.bounds)
            options.bounds = ShapeFactory.createBounds(options.reference, options.bounds);
        
        var model = new UrlTileSetModel(options);
        return new RasterTileSetLayer(model, //WMSTileSetLayer   RasterTileSetLayer
                {label: label, layerType: LayerType.BASE}
        );
    }
    
    
    function createTrackLayer(reference, url)
    {
        var tracksMemoryStore = new MemoryStore({});
        var tracksModel = new FeatureModel(tracksMemoryStore, {});
        var tracksPainter = new FeaturePainter();        
        tracksPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
            geoCanvas.drawIcon(shape, {});
        };
        var tracksLayer = new FeatureLayer(tracksModel, {
            label: "Tracks",
            layerType: LayerType.STATIC,
            painter: tracksPainter
        });
    }
    
    function createTrajectoryLayer(reference, url, options, dataSetStartTime, dataSetEndTime)
    {
        var trajectoryPainter = new TrajectoryPainter({
            properties: ["origin", "airline", "destination"],
            defaultColor: "rgb(82, 116, 255)",
            selectionColor: "rgb(255,0,0)",
            lineWidth: 3,
            lineType: LineType.SHORTEST_DISTANCE,
            timeWindows: [0, dataSetStartTime - dataSetEndTime],
            timeProvider: function(feature, shape, pointIndex) {
                return feature.properties.timestamps[pointIndex];
            }
        });
        /*
        trajectoryPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
            geoCanvas.drawShape(shape, {});
        };*/
        var trajectoryStore = new UrlStore({target: url , codec: new GeoJsonCodec()});
        var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
        return new FeatureLayer(trajectoryModel, {
            label: options.label,
            selectable: options.selectable,
            painter: trajectoryPainter
        });
    }
    
    function createUrlOrFileLayer(urlOrFile, reference, options)
    {
        var codec = new GeoJsonCodec({generateIDs: true});
        var store;

        if (typeof urlOrFile === 'string') {
            store = new UrlStore({target: urlOrFile, codec: codec});
        } else {
            store = new LocalFileStore(urlOrFile, codec);
        }
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    
    
    function createWFSLayer (reference, options, url, id) {
        return WFSDataLoader.createLayer(reference, options, url, id);
    }
    
    function createWMSLayer (url, layerName, layerId, queryable) {
        return WMSDataLoader.createLayer(url, layerName, layerId, queryable);
    }
    
    function createWMTSLayer (layerName, options) {
        return WMTSDataLoader.createLayer(layerName, options);
    }
    
    function createFusionBackgroundLayer(url, id) {
        var tileSetReference = ReferenceProvider.getReference("EPSG:4326");
        //var url = "/lts/earthimageryhdr";
        var imageryParameters = {
            reference: tileSetReference,
            level0Columns: 4,
            level0Rows: 2,
            levelCount: 24,
            bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
            url: url,
            coverageId: id,
            tileWidth: 256,
            tileHeight: 256
        };

        return new RasterTileSetLayer(new FusionTileSetModel(imageryParameters),
          {label: "Elevation", layerType: LayerType.BASE});
  }
  
    function createGlowSlopesLayer(url, dataSetStartTime, dataSetEndTime, glowColor, label) {
      var trajectoriesPainter = new TrajectoryPainter({
        properties: ["origin", "airline", "destination"],
        defaultColor: glowColor,
        selectionColor: glowColor,
        lineWidth: 3,
        lineType: LineType.SHORTEST_DISTANCE,
        timeWindow: [0, dataSetEndTime - dataSetStartTime],
        timeProvider: function(feature, shape, pointIndex) {
          return feature.properties.timestamps[pointIndex];
        },
        draped: true
      });
      //al =0;
      var trajectoryStore;
      if (typeof url === 'string') {
            trajectoryStore = new UrlStore({target: url, codec: new GeoJsonCodec()});
        } else {
            trajectoryStore = new MemoryStore({ data: url });
        }
      var reference = ReferenceProvider.getReference("CRS:84");
      var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
      return new FeatureLayer(trajectoryModel, {
        label: label || url,
        selectable: true,
        visible: true,
        painter: trajectoriesPainter
      });
    }
    function createGlowSlopesLayerData(url, dataSetStartTime, dataSetEndTime, glowColor, label, map, x) {
        var features = null, intervalo =0;
        $.ajax({
            type:'Get',
            dataType: "json",
            url: url
        }).done(function(data) {
                console.log("archivo leido");
                var layer;
                if(data.features) {
                    features = Shapes.crearFeatureTime(data.features, ReferenceProvider.getReference("CRS:84"), 
                        dataSetStartTime, dataSetEndTime);
                    console.log("Feature con tiempo creado");
                    layer = createGlowSlopesLayer(features, dataSetStartTime, dataSetEndTime, glowColor, label);
                    map.layerTree.addChild(layer);
                    //console.log(features);
                    if(x)
                        setLayer2(layer, features);
                    else
                        setLayer(layer, features);
                    
                }
                else {
                    console.log("el archivo no se puede leer, no features");
                    return null;
                }
            }
        ).fail(function(errMsg) {
                console.log("no se leyo el archivo\n" + errMsg);
                return null;
        });
    }
  
  	function createMapaLayer(reference, url, label) {
        var options = {
            baseURL: url+"/{z}_{x}_{y}.png", // "../recursos/Maps/SanPedro/{z}_{x}_{y}.png",
            name: "Massachusetts image@terrain",
            forma: "image/png",
            levelCount:17,
            level0Rows:2,
            level0Columns:4,
            tileWidth:256,
            tileHeight:256,
            reference: reference,
            bounds:[-180,360,-90,180],
            elevation: true
        };
        options.bounds = ShapeFactory.createBounds(options.reference, options.bounds);
        var model = new UrlTileSetModel(options);
        return new RasterTileSetLayer(model, //WMSTileSetLayer   RasterTileSetLayer
                {label: label, layerType: LayerType.BASE}
        );
    }

    return {
        createKmlLayer: createKmlLayer,
        createFeatureLayer: createFeatureLayer,
        createMemoryLayer: createMemoryLayer,
        createTrackLayer: createTrackLayer,
        createTrajectoryLayer: createTrajectoryLayer,
        createUrlOrFileLayer: createUrlOrFileLayer,
        createCustomMapaLayer: createCustomMapaLayer,
        createLayerImage: createLayerImage,
        createMapImage: createMapImage,
        createXMLLayer: createXMLLayer,
        createWFSLayer: createWFSLayer,
        createWMSLayer: createWMSLayer,
        createWMTSLayer: createWMTSLayer,
        createFusionBackgroundLayer: createFusionBackgroundLayer,
        createGlowSlopesLayerData: createGlowSlopesLayerData,
        createGlowSlopesLayer: createGlowSlopesLayer,
        
        MemoryStore: function (options) {
            return new MemoryStore(options);
        },
        FeatureModel: function (store, options) {
            return new FeatureModel(store, options);
        },
        FeatureLayer: function (model, options) {
            return new FeatureLayer(model, options);
        },
        createMapaLayer: createMapaLayer
    };
});