/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "./LayerFactory"
],
function (LayerFactory) {
   function openFiles(files, reference){
        var layer;
        for (var i = 0; i < files.length; i++) {
            layer = openFileOrUrl(files[i], reference);
        }
        return layer;
    }
    function openUrls(urls, reference){
        var layer;
        //for (var i = 0; i < urls.length; i++) {
            layer = openFileOrUrl("../../../../"+urls, reference);
        
        return layer;
    }
    function openFileOrUrl(urlOrFile, reference) {
        var fileName = (typeof urlOrFile === 'string') ? urlOrFile : urlOrFile.name;
        var extension = fileName.toLowerCase().replace(/.*\./, "");
        var layerName = fileName.replace(/.*\//, "").replace(/\.[^.]*$/, "");
        var options ={label: layerName, selectable: true};
        
        if (extension === "json" || extension === "geojson") {
            return LayerFactory.createFeatureLayer(reference, options, urlOrFile);
        } else if (extension.indexOf("gml") > -1 || extension === "xml") {
            return LayerFactory.createXMLLayer(reference, urlOrFile, options);
            //console.log("Cannot open xml or gml")
        } else if (extension === "kml") {
            console.log("kml");
            if (fileName !== urlOrFile) {
                console.log("Cannot open KML files directly, use URL instead.");
                return LayerFactory.createKmlLayer(urlOrFile, layerName, true);
            }
            else{
                return LayerFactory.createKmlLayer(urlOrFile, layerName, true);
            }
        } else if(extension === "xlsx") {
            console.log("archivo xlsx ");
            ExcelToJSON.parseExcel(urlOrFile);
            return null;
        }else {
            console.log( "Unknown file format for " + fileName);
            return null;
        }
    } 
    
    function getFiles(files){
        var data;
        for (var i = 0; i < files.length; i++) {
            data [i] = getUrl(files[i]);
        }
            return data;
    }
    function getUrl(urlOrFile) {
        var fileName = (typeof urlOrFile === 'string') ? urlOrFile : urlOrFile.name;
        var extension = fileName.toLowerCase().replace(/.*\./, "");
        var layerName = fileName.replace(/.*\//, "").replace(/\.[^.]*$/, "");
        if (extension === "json" || extension === "geojson") {
            return {type: "file", name: fileName, extension: extension, label: layerName};
        } else if (extension.indexOf("gml") > -1 || extension === "xml") {
            return {type: "file", name: fileName, extension: extension, label: layerName};
        }
    }
    
    return {
        openFiles: openFiles,
        openUrls: openUrls,
        openFileOrUrl: openFileOrUrl,
        getFiles: getFiles
    };
});


