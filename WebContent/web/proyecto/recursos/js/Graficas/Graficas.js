/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/model/feature/Feature",
    "./Tablas"
], function (ShapeFactory, ReferenceProvider, Feature, Tablas) {
    
    var graficas = new Array(), idGrafica=-1;
    var Filtros = [], Labels = [];
    /*
     * 
     * @param {type} features
     * @param {type} div
     * @param {type} labels
     * @param {type} type
     * @param {type} filtros
     * @param {type} divTable
     * @returns {GraficasL#12.crearGraficas.GraficasAnonym$2}
     */
    function crearGraficasFeatures(features, div, labels, type, filtros, divTable, titles, formatAnotattion) {
        var divs = [];
        document.getElementById(div).innerHTML = "";
        var ng = labels.length, etiqueta = "";
        Labels = labels;
        var baseGrafica = '<label><big>$label</big></label>'+
                    '<div id="$div"></div>';
        for(var i=0; i<ng; i++) {
            if(titles[i] && titles[i]!== "")
                divs[i] = titles[i];
            else {
                if( typeof labels[i] === "string") 
                    divs[i] = labels[i];
                else {
                    divs[i] = labels[i][0];
                }
            }
            etiqueta += baseGrafica.replace("$label", divs[i]).replace("$div", divs[i]+i);
        }
        document.getElementById(div).innerHTML = etiqueta;
        
        
        var tablaDatos = Tablas.crearArreglo(features, divTable);
        console.log(tablaDatos[0]);
        var Graficas = [], t;
        for(i=0; i<ng; i++) {
            if(type[i]) {
                t = type[i];
            } else {
                t = type[0];
            }
            if( typeof labels[i] === "string") {
                Graficas[i] = crearGraficaIdTexto(divs[i]+i, tablaDatos, t, labels[i]);
            } else {
                Graficas[i] = crearGraficaLabelValor(divs[i]+i, tablaDatos, t, labels[i][0], labels[i][1], formatAnotattion);
            }
           
            
            if(filtros[i]) {
                if(filtros[i] && filtros[i] !== "")
                    setOptions(filtros[i], Graficas[i].nombres);
            }
            Filtros = filtros;
            Graficas[i] = {
                id: Graficas[i].idGrafica,
                label: labels[i],
                tipo: t,
                div: divs[i]
            };
        }
        
        return  {
            graficas: Graficas,
            tablaDatos: tablaDatos
        };
    }
    
    /*
     * 
     * @param {type} div
     * @param {type} opciones
     * @returns {undefined}
     */
    function setOptions(div, opciones, firstOption) {
        if(!firstOption)
            firstOption = "Todos";
        document.getElementById(div).innerHTML = "";
        var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", firstOption).replace("$LABEL", firstOption);
        var  n = opciones.length;
        for(var i =0; i<n; i++) {
            etiqueta += base.replace("$VALUE", opciones[i]).replace("$LABEL", opciones[i]);
        }
        document.getElementById(div).innerHTML = etiqueta;
        
    }
    
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} tipo
     * @param {String} titulo
     * @returns {unresolved}
     */
    function dibujarGraficaArreglo(div, datos, tipo, titulo, id, chartOptions) {
        document.getElementById(div).innerHTML="";
        var c;
        
        switch(tipo.toLowerCase()) {
            case "piechart":
                chartOptions = chartOptions? chartOptions: getPieOptions();
                c = PieChart(div, datos, titulo, id, chartOptions);
                break;
            case "columnchart":
                c = ColumnChart(div, datos, titulo, id);
                break;
            case "areachart":
                c = AreaChart(div, datos, id);
                break;
            default: console.log("Tipo de Grafica desconocida "+ tipo);return null;
        }
        
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {undefined}
     */
    function ColumnChart(div, datos, titulo, id) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            chart = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getColumnOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            graficas[id] = chart;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {unresolved}
     */
    function PieChart(div, datos, titulo, id, chartOptions){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawPieChart() {
            chart = new google.visualization.PieChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= chartOptions? chartOptions : getPieOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[id] = chart;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @returns {unresolved}
     */
    function AreaChart(div, datos, id){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            chart = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getAreaOptions();
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[id] = chart;
        }
    }
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     *
    function crearArreglo(features, div) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        //encavezados[i] = "Longitud";
        //encavezados[i+1] = "Latitud";
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            for(key in features[i].properties) {
                try {
                    x = parseFloat(features[i].properties[key]);
                    if(x)
                        valores[k] = x;
                    else
                        valores[k] = features[i].properties[key];
                }catch (e) {
                    
                }
                k++;
            }
            coordenadas = features[i].geometry.coordinates;
            if(coordenadas.length === 3) {
                valores[k] = coordenadas[0];
                valores[k+1] = coordenadas[1];
            } else {
                valores[k] = coordenadas[0].x;
                valores[k+1] = coordenadas[0].y;
            }
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        if(div) {
            Tablas.crearTablaDIV(div, tabla);
        }
        return tabla;
    }
    
    /*
     * 
     * @param {type} div
     * @param {type} tablaDatos
     * @param {type} chartType
     * @param {type} label
     * @returns {GraficasL#11.crearGraficaTabla.datos}
     */
    function crearGraficaTabla(div, tablaDatos, chartType, label) {
        var numeroDatos =[], tipoDato = [];
        for(var i = 1; i<tablaDatos.length; i++) {
            tipoDato[i-1] = tablaDatos[i][0];
            numeroDatos[i-1] = tablaDatos[i][1];
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        tablaDatos = [];
        tablaDatos[0] = [label, "Cantidad"];
        if(z>10)
            z = 10;
        z=tipoDato.length;
        for(i=0; i<z; i++) {
            tablaDatos[i+1] = [""+tipoDato[i], numeroDatos[i]];
        }
        var otros =0;
        for(i=i; i<tipoDato.length; i++) {
            if((i-z) > 10)
                break;
            otros += numeroDatos[i];
        }
        //tablaDatos[z] = ["Otros", otros];
        idGrafica++;
        var datos = {
            tabla: tablaDatos,
            nombres: tipoDato,
            valores: numeroDatos,
            idGrafica: idGrafica
        };
        //crearGraficaIdTexto(div, datos.tabla, chartType, label);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica);
        
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} id
     * @returns {Number}
     */
    function crearGraficaIdTexto(div, tablaDatos, chartType, label, options) {
        idGrafica++;
        console.log("Creando Grafica "+label+" id Grafica"+idGrafica);
        var datos = datosChartTexto(tablaDatos, label);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica, options);
        datos.idGrafica = idGrafica;
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {Number}
     */
    function crearGraficaLabelValor(div, tablaDatos, chartType, label, valor, formatAnotation, options) {
        idGrafica++;
        console.log("Creando Grafica "+label+" id Grafica"+idGrafica);
        var datos = datosChartLabelValor(tablaDatos, label, valor, formatAnotation);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica, options);
        datos.idGrafica = idGrafica;
        return datos;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChartTexto(tablaDatos, label) {
        var tabla = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), valor, nuevoDato=false;
        var i=0, j=0, k;
        valor = tablaDatos[0];
        for(k=0;k<valor.length;k++) {
            if(valor[k] === label)
                break;
        }
        for(i=1; i<tablaDatos.length; i++) {
            
            valor = tablaDatos[i][k];
            if(tipoDato.length===0) {
                tipoDato[0] = valor;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(valor === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = valor;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>10)
            z = 10;
        for(i=0; i<z; i++) {
            tabla[i+1] = [""+tipoDato[i], numeroDatos[i]];
        }
        if(tipoDato.length>10) {
            var otros =0;
            for(i=i; i<tipoDato.length; i++) {
                if((i-z) > 10)
                    break;
                otros += numeroDatos[i];
            }
            tabla[z] = ["Otros", otros];
        }
        return {
            tabla: tabla,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} idLabel
     * @param {int} idValor
     * @param {String} label
     * @returns {Array}
     */
    function datosChartLabelValor(tablaDatos, label, lValor, formatAnotation) {
        var tabla = [[label, "number", {type: 'string', role: 'annotation'}]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        var idLabel, idValor;
        valor = tablaDatos[0];
        for(var k=0;k<valor.length;k++) {
            if(valor[k] === label)
                idLabel = k;
            if(valor[k] === lValor)
                idValor = k;
        }
        
        for(i=1; i<tablaDatos.length; i++) {
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        if(!formatAnotation || formatAnotation=== null) 
            formatAnotation = defaultFormat;
        var z = tipoDato.length;
        if(z>10)
            z = 10;
        for(i=0; i<z; i++) {
            tabla[i+1] = [""+tipoDato[i], numeroDatos[i], formatAnotation(numeroDatos[i])];
        }
        if(tipoDato.length>10) {
            var otros =0;
            for(i=i; i<tipoDato.length; i++) {
                if((i-z) > 10)
                    break;
                otros += numeroDatos[i];
            }
            tabla[z] = ["Otros", otros];
        }
        
        return {
            tabla: tabla,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    function defaultFormat(number) {
        var anotation, c, j=0, x=false;
        anotation = Math.round(number) +"";
        number = "";
        for(var i=anotation.length-1;i>=0;i--) {
            c = anotation.charAt(i);
            if(c !=='K') {
                j++;
            }
            if(j>3) {
                number+=",";
                j=1;
                x=true;
            }
            number+=c;
        }
        if(x) {
            anotation = "";
            for(i=number.length-1; i>=0;i--) {
                c = number.charAt(i);
                anotation += c;
            }
        }
        return anotation;
    }
    
    /*
     * Se encarga de preparar los datos y enviarlos a la funcion para crear la grafica.
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} id
     * @returns {undefined}
     */
    function actualizarGraficaIdTexto(idGrafica, tablaDatos, typeChart, label) {
        var datos = datosChartTexto(tablaDatos, label).tabla;
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos, typeChart);
        else 
            console.log("Grafica aun no cargada");
    }
    /*
     * 
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {undefined}
     */
    function actualizarGraficaLabelValor(idGrafica, tablaDatos, typeChart, labels, formatAnotation) {
        var datos = datosChartLabelValor(tablaDatos, labels[0], labels[1], formatAnotation);
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos.tabla, typeChart);
        else
            console.log("Grafica aun no cargada");
    }
    /*
     * 
     * @param {Object} chart
     * @param {Array[][]} newData
     * @param {String} chartType
     * @returns {undefined}
     */
    function updateChart(chart, newData, chartType) {
        var newOptions;
        switch(chartType) {
            case "piechart": 
                newOptions = getPieOptions();
                break;
            case "columnchart":
                newOptions = getColumnOptions();
                break;
            case "areachart":
                newOptions = getAreaOptions();
                break;
        }
        var table = new google.visualization.arrayToDataTable(newData);
        chart.draw(table, newOptions);
    }
    function actualizarGrafica(idGrafica, newData, chartType) {
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], newData, chartType);
    }
    /*
     * Se encarga de leer los datos de la columna dada por id de la tabla tablaDatos. 
     * Y regresar un Arreglo de datos listos para crear la grafica.
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChart(tablaDatos, id, label) {
        var datos = [[label, "number"]], dato;
        var tipoDato = new Array(), numeroDatos = new Array(), nuevoDato=false;
        var i, j;
        
        for(i=1; i<tablaDatos.length; i++) {
            dato = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = dato;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(dato === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = dato;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>50)
            z = 50;
        for(i=0; i<z; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
                      
    function getId(label, list) {
        for( var k=0;k<list.length;k++) {
            if(list[k] === label)
                return k;
        }
    }
    /*
     * 
     * @param {type} tablaDatos
     * @param {type} timeEventLayer
     * @param {type} mapBounds
     * @param {type} selectedFeature
     * @returns {Array}
     */
    
    function filtrarPorBounds(tablaDatos, timeEventLayer, mapBounds, selectedFeature, year) {
        var tabla = [tablaDatos[0]], i, j=1, lon, lat;
        var baseDate = "aaaa/mes/dia hora:00";
        var zonaFilterNode = document.getElementById( "zonaFiltro" );
        var crimenFilterNode = document.getElementById( "crimenFiltro" );
        var crimen = crimenFilterNode.selectedOptions[0].value;
        var zona = zonaFilterNode.selectedOptions[0].value;
        var n = tablaDatos.length;
        for(i=1;i<n;i++) {
            lat = tablaDatos[i][n-1];
            lon = tablaDatos[i][n-2];
            var eventTime = tablaDatos[i][n-3];
            
            var point = createPoint(lon, lat, 0, i, {EventTime: eventTime});
            var f = timeEventLayer.shapeProvider.provideShape(point);
            var z = tablaDatos[i][5], c = tablaDatos[i][0];
            if(selectedFeature){
                var cuadrante = obtenerCuadrante(selectedFeature.properties.name);
                if(cuadrante === tablaDatos[i][6]) {
                    tabla[j] = tablaDatos[i];
                    j++;
                }
                /*
                if(mapBounds.contains2D(f) && selectedFeature.geometry.bounds.contains2D(point.shape)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                    
                }*/
            } else {
                if(mapBounds.contains2D(f)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                }
            }
        }
        if(tabla.length > 1)
            return tabla;
        else {
            console.log("tabla no actualizada");
            return tablaDatos;
        }
            
    }
    function createPoint(x, y, z, id, properties) {
        var reference = ReferenceProvider.getReference("CRS:84");
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, id
        );
    }
    
    /*
     * 
     * @param {type} valores
     * @param {type} nombres
     * @returns {GraficasL#11.ordenarMayorMenor.GraficasAnonym$14}
     */
    function ordenarMayorMenor(valores, nombres) {
        var n = valores.length, x, y, k =0;
        for(var j=1; j<n; j++) {
            if(valores[j] > valores[j-1]) {
                x = valores[j-1];
                y = nombres[j-1];
                valores[j-1] = valores[j];
                nombres[j-1] = nombres[j];
                valores[j] = x;
                nombres[j] = y;
                j = 0;
                k++;
                if(k>(n*10)) {
                    break;
                }
            }
        }
        return {
            valores: valores, nombres: nombres
        };
    }
    
    
    /*
     * 
     * @param {type} tablaDatos
     * @param {type} timeEventLayer
     * @param {type} mapBounds
     * @param {type} selectedFeature
     * @returns {Array}
     */
    
    function actualizarTabla(tablaDatos, timeEventLayer, mapBounds) {
        var tabla = [tablaDatos[0]], i=0, j=1, lon, lat;
        var n = tablaDatos[0].length, filtros = [], values = [];
        for(j=0;j<Filtros.length;j++) {
            if(Filtros[j] && Filtros[j]!=="") {
                filtros[i] = document.getElementById( Filtros[j] );
                values[i] = filtros[i].selectedOptions[0].value;
                i++;
            }
        }
        if(timeEventLayer && mapBounds) {
            var k=1;
            for(i=1;i<tablaDatos.length;i++) {
                lat = tablaDatos[i][getId("LONGITUD", tablaDatos[0])];
                lon = tablaDatos[i][getId("LATITUD", tablaDatos[0])];
                var eventTime = tablaDatos[i][getId("EventTime", tablaDatos[0])];;
                var point = createPoint(lon, lat, 0, i, {EventTime: eventTime});
                var f = timeEventLayer.shapeProvider.provideShape(point);
                var vFeatures = [];
                for(j=0;j<filtros.length;j++) {
                    var idLabel;
                    if(typeof Labels[j] === "string")
                        idLabel = getId(Labels[j], tablaDatos[0]);
                    else
                        idLabel = getId(Labels[j][0], tablaDatos[0]);
                    vFeatures[j] = tablaDatos[i][idLabel];
                }
                var xFiltros = 0;
                if(mapBounds.contains2D(f)) {
                    for(j=0;j<filtros.length;j++) {
                        if(vFeatures[j] === values[j] || values[j] === "Todos") {
                            xFiltros++;
                        }
                    }
                    if(xFiltros === filtros.length){
                        tabla[k] = tablaDatos[i];
                        k++;
                    }
                }
            }
            if(tabla.length > 1)
                return tabla;
            else {
                console.log("tabla no actualizada");
                return tablaDatos;
            }
        } else {
            var k=1;
            for(i=1;i<tablaDatos.length;i++) {
                var x=0, l=0;
                for(j=0;j<filtros.length;j++) {
                    var label;
                    while(typeof Labels[l] !== "string") {
                        l++;
                    }
                    label = Labels[l];
                    l++;
                    var vTabla = tablaDatos[i][getId(label, tablaDatos[0])];
                    var value = filtros[j].selectedOptions[0].value;
                    if(vTabla === value || value === "Todos") {
                        x++;
                    }
                }
                if(x === filtros.length){
                    tabla[k] = tablaDatos[i];
                    k++;
                }
            }
            if(tabla.length > 1)
                return tabla;
            else {
                console.log("tabla no actualizada");
                return tablaDatos;
            }
        }    
            
    }
    function createPoint(x, y, z, id, properties) {
        var reference = ReferenceProvider.getReference("CRS:84");
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, id
        );
    }
    
    
    function getPieOptions() {
        return {
        width: 350,
        height: 220,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 50,
            width: 270,
            height: 180,
            backgroundColor: "#17202a"
        },
        pieSliceBorderColor: "#17202a"
      };
    }
    function getColumnOptions() {
        return {
        width: 300,
        height: 220,
        orientation: "vertical",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 100,
            width: 150,
            height: 230,
            backgroundColor: "#17202a"
        },
        annotations: {
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        },
        animation:{
            duration: 2000,
            easing: 'linear'
        }
      };
    }
    function getAreaOptions () {
        return {
        width: 370,
        height: 320,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            right: 10,
            width: 300,
            height: 280,
            backgroundColor: "#17202a"
        }
        };
    }
    
    
    return {
        crearGraficasFeatures: crearGraficasFeatures,
        dibujarGraficaArreglo: dibujarGraficaArreglo,
        updateChart: updateChart,
        //rearArreglo: crearArreglo,
        crearGraficaIdTexto: crearGraficaIdTexto,
        actualizarGraficaIdTexto: actualizarGraficaIdTexto,
        crearGraficaLabelValor: crearGraficaLabelValor,
        actualizarGraficaLabelValor: actualizarGraficaLabelValor,
        filtrarPorBounds: filtrarPorBounds,
        crearGraficaTabla: crearGraficaTabla,
        actualizarTabla: actualizarTabla,
        setOptions: setOptions,
        actualizarGrafica: actualizarGrafica
    };
    
});
