define([
  "luciad/reference/ReferenceProvider",
  "luciad/model/tileset/WMSTileSetModel",
  "luciad/view/tileset/WMSTileSetLayer",
  "luciad/model/image/WMSImageModel",
  "luciad/view/image/WMSImageLayer"
], function(ReferenceProvider, WMSTileSetModel, WMSTileSetLayer, WMSImageModel, WMSImageLayer) {

  return {

    /**
     * Creates a layer for WMS data.
     *
     * This class uses Web Mercator as reference by default.
     *
     * Summary:
     * - Create a {@link luciad/model/tileset/WMSTileSetModel}
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     *
     * Alternatively, to use single-image WMS:
     * - Create a {@link luciad/model/image/WMSImageModel}
     * - Create a {@link luciad/view/image/RasterImageLayer}
     */

    createLayer: function(url, layerName, layerId, queryable) {

      var modelOptions = {
        getMapRoot: url,
        layers: [layerId],
        reference: ReferenceProvider.getReference("EPSG:3857"),
        transparent: true
      };

      if (queryable) {
        modelOptions.queryLayers = [layerId];
      }

      var model, layer;

        model = new WMSTileSetModel(modelOptions);
        layer = new WMSTileSetLayer(model, {label: layerName + " (WMS)"});
      

      return layer;
    }
  };

});
