define([
  "luciad/view/feature/FeatureLayer",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "samples/dataformats/LocalFileStore",
  "luciad/model/codec/GeoJsonCodec"
], function(FeatureLayer, FeatureModel, UrlStore, LocalFileStore, GeoJsonCodec) {

  return {

    /**
     * Creates a layer for the given URL of File, assuming it is GeoJson content.
     *
     * You can style GeoJson data using a FeaturePainter.
     * See for example the <i>Vector Data<i> sample.
     *
     * Summary:
     * - Create a store to access the data, for example {@link luciad/model/store/UrlStore}
     * - Use {@link luciad/model/codec/GeoJsonCodec} in the store (this is the default)
     * - Create a {@link luciad/model/feature/FeatureModel} for the store
     * - Create a {@link luciad/view/feature/FeatureLayer}
     * - Create a {@link luciad/view/feature/FeaturePainter} to style the layer (optional)
     */

    createLayer: function(layerName, urlOrFile, painter) {

      //#snippet geojson
      var codec = new GeoJsonCodec({generateIDs: true});
      var store;

      if (typeof urlOrFile === 'string') {
        store = new UrlStore({target: urlOrFile, codec: codec});
      } else {
        store = new LocalFileStore(urlOrFile, codec);
      }

      var model = new FeatureModel(store);

      var layer = new FeatureLayer(model, {label: layerName + " (JSON)", selectable: true, painter: painter});
      //#endsnippet geojson

      return layer;
    }
  };

});