define([
  "luciad/view/LayerType",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/model/tileset/BingMapsTileSetModel",
  "dojo/request"
], function(LayerType, RasterTileSetLayer, BingMapsTileSetModel, request) {

  return {

    /**
     * Creates a layer for Bing Maps data.
     *
     * Summary:
     * - Contact Bing proxy to initialize session
     * - Create a {@link luciad/model/tileset/BingMapsTileSetModel}
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     *
     * @param type {String} One of "road", "aerial" or "AerialWithLabels"
     * @return {Promise} A promise for a layer.
     */

    createLayer: function(type) {

      //#snippet createModel
      var getBingMetadata = request("/bingproxy/" + type, {handleAs: "json"});

      return getBingMetadata.then(
          function(data) {
            //Inspect the response
            var resourceSet;
            var resource = data;

            if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' && data.resourceSets.length > 0) {
              resourceSet = data.resourceSets[0];
              if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' && resourceSet.resources.length > 0) {
                resource = resourceSet.resources[0];
              }
            }

            resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;

            var model = new BingMapsTileSetModel(resource);

            var layer = new RasterTileSetLayer(model, {label: type + " (Bing)", layerType: LayerType.BASE, id: "Bing"});

            return layer;
          }, function() {
            throw new Error("Something went wrong while contacting the Bing proxy.  Did you configure it with a Bing Maps key?");
          }
      );
      //#endsnippet createModel
    }
  };

});