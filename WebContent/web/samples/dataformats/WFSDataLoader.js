define([
  "luciad/reference/ReferenceProvider",
  "luciad/view/feature/FeatureLayer",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/WFSFeatureStore",
  "luciad/model/codec/GeoJsonCodec"
], function(ReferenceProvider, FeatureLayer, FeatureModel, WFSFeatureStore, GeoJsonCodec) {

  return {

    /**
     * Creates a layer for WFS data.
     *
     * You can style WFS data using a FeaturePainter.
     * See for example the <i>Vector Data<i> sample.
     *
     * Summary:
     * - Create a {@link luciad/model/store/WFSFeatureStore} to access the server
     * - Create a {@link luciad/model/feature/FeatureModel} for the store
     * - Create a {@link luciad/view/feature/FeatureLayer}
     * - Create a {@link luciad/view/feature/FeaturePainter} to style the layer (optional)
     */
    createLayer: function(url, typeName, options) {
      options = options || {};
      var reference = ReferenceProvider.getReference(options.reference || "urn:ogc:def:crs:OGC::CRS84");
      var versions = options.versions || ['1.1.0'];
      var painter = options.painter;

      //#snippet createWFSModel
      var store = new WFSFeatureStore({
        serviceURL: url,
        typeName: typeName,
        outputFormat: "application/json",
        codec: new GeoJsonCodec(),
        reference: reference,
        versions: versions
      });

      var model = new FeatureModel(store);

      var layer = new FeatureLayer(model, {
        label: typeName + " (WFS)",
        selectable: true,
        painter: painter
      });
      //#endsnippet createWFSModel
      return layer;
    }
  };

});