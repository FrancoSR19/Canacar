define([
  "luciad/shape/format/MGRSPointFormat",
  "samples/common/mouseLocation/MouseLocationComponent",
  "dojo/store/Memory",
  "samples/common/LayerConfigUtil"
], function(MGRSPointFormat, MouseLocationComponent, Memory, LayerConfigUtil) {

  /**
   * @fileOverview
   * The ThemeManager contains all available themes, and allows for one theme to be active at a time.
   * Each theme should register itself with the ThemeManager.
   */
  themestore = new Memory({
    data: []
  });

  var themeManager = {
    store: themestore,
    gridLayer: LayerConfigUtil.createLonLatGridLayer(),

    /**
     * Register a theme with the manager. Typically, this is done by the theme itself when it is
     * initially loaded.
     * @public
     * @param theme The theme to register
     */
    register: function(theme) {
      themestore.put({id: theme.name, theme: theme});
    },

    /**
     * Returns the store that contains all themes
     * @public
     *
     * @returns A dojo store containing the available themes.
     */
    getStore: function() {
      return themestore;

    },

    /**
     * Activates the theme with the given name. The currently active theme
     * will be deactivated.
     * @param themeName {String} The name of a theme
     */
    activateTheme: function(themeName) {
      document.activeElement.blur();
      if (this.context) {
        var newTheme = this.store.get(themeName);
        if (newTheme) {
          if (this.activeTheme) {
            this.context.map.layerTree.removeChild(this.gridLayer);
            this.activeTheme.deactivate(this.context);
            this.activeTheme = null;
          }
          newTheme = newTheme.theme;
          newTheme.activate(this.context);
          this.context.map.layerTree.addChild(this.gridLayer);
          this.activeTheme = newTheme;
          if (this.context.firstLoad) {
            this.context.firstLoad = false;
          }
        } else {
          console.log("Could not find theme: '" + themeName + "'!");
        }
        this._setMouseLocationComponent(this.context.map, themeName);
      }
    },

    _setMouseLocationComponent: function(map, themeName) {
      this.context.mouseLocationComponent.destroy();
      if (themeName === "Mission Control") {
        this.context.mouseLocationComponent = new MouseLocationComponent(map, {
          formatter: new MGRSPointFormat({
            coordinateSeparator: " ",
            zoneSeparator: " "
          })
        });
      } else {
        this.context.mouseLocationComponent = new MouseLocationComponent(map);
      }
    },

    activeTheme: null
  };

  /**
   * Global alternative to activate themes.
   * @param themeName
   */
  activateTheme = function(themeName) {
    themeManager.activateTheme(themeName);
  };

  return themeManager;
});