define([
  "dojo/dom-style",
  "dijit/registry",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeaturePainterUtil",
  "samples/common/LayerConfigUtil",
  "../ThemeManager",
  "./CityPainter"
], function(domStyle, registry, ShapeFactory, FeaturePainterUtil, LayerConfigUtil, ThemeManager, CityPainter) {

  var citiesLayer;

  var theTheme = {
    name: "DefaultTheme",
    activate: function(context) {

      var _gazetteertitlepane = registry.byId("gazetteerTitlePane");
      _gazetteertitlepane.set("open", false);
      domStyle.set(_gazetteertitlepane.domNode, "display", "none");
      var _annotationtitlepane = registry.byId("annotationTitlePane");
      _annotationtitlepane.set("open", false);
      domStyle.set(_annotationtitlepane.domNode, "display", "none");

      var citiesPainter;
      if (!citiesLayer) {
        citiesPainter = FeaturePainterUtil.addSelection(new CityPainter());
        citiesLayer = LayerConfigUtil.addCitiesLayer(context.map, citiesPainter);
      } else {
        context.map.layerTree.addChild(citiesLayer);
      }

      if (!context.firstLoad) {
        context.map.mapNavigator.fit(
            {
              bounds: ShapeFactory.createBounds(citiesLayer.model.reference, [-124, 60, 22, 30]),
              animate: true
            });
      }
    },
    deactivate: function(context) {
      if (citiesLayer) {
        context.map.layerTree.removeChild(citiesLayer);
      }
    },
    toString: function() {
      return this.name;
    }
  };
  ThemeManager.register(theTheme);
  return theTheme;
});
