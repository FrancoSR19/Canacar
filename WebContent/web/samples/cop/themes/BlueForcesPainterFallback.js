define([
  'samples/common/IconFactory',
  'luciad/view/feature/FeaturePainter'], function(IconFactory, FeaturePainter) {

  var smallRedCircle = IconFactory.circle({
    width: 18,
    height: 18,
    fill: "#80E0FF",
    stroke: "#FF0000"
  });
  var smallBlueCircle = IconFactory.circle({
    width: 18,
    height: 18,
    fill: "#80E0FF",
    stroke: "#000000"
  });
  var bigRedRectangle = IconFactory.rectangle({
    width: 32,
    height: 32,
    fill: "#80E0FF",
    stroke: "#FF0000"
  });
  var bigBlueRectangle = IconFactory.rectangle({
    width: 32,
    height: 32,
    fill: "#80E0FF",
    stroke: "#000000"
  });

  function BlueForcesPainterFallback() {
  }

  BlueForcesPainterFallback.prototype = Object.create(FeaturePainter.prototype);

  //#snippet getDetailLevelScales
  BlueForcesPainterFallback.prototype.getDetailLevelScales = function() {
    return [1 / 37800];
  };
  //#endsnippet getDetailLevelScales
  
  //#snippet paintBody
  BlueForcesPainterFallback.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    if (state.level === 0) {
      if (state.selected) {
        geoCanvas.drawIcon(shape, {image: smallRedCircle});
      } else {
        geoCanvas.drawIcon(shape, {image: smallBlueCircle});
      }
    } else {
      if (state.selected) {
        geoCanvas.drawIcon(shape, {image: bigRedRectangle});
      } else {
        geoCanvas.drawIcon(shape, {image: bigBlueRectangle});
      }
    }
  }
  //#endsnippet paintBody

  return BlueForcesPainterFallback;

});