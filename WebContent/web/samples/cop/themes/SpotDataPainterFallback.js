define([
  'luciad/view/feature/FeaturePainter',
  'samples/common/IconFactory'
], function(FeaturePainter, IconFactory) {

  function SpotDataPainterFallBack() {
  };

  SpotDataPainterFallBack.prototype = Object.create(FeaturePainter.prototype);

  SpotDataPainterFallBack.prototype.constructor = SpotDataPainterFallBack;

  var colorMap = {
    F: "rgb(128, 224, 225)",
    H: "rgb(255, 128, 128)",
    U: "rgb(255, 255, 128)",
    N: "rgb(171, 255, 171)"
  };

  function getCircleStyle(properties, size, state) {
    var affiliation = properties.code[1];
    var circleStyle = {width: size, height: size, stroke: "#000000"};
    var customFill = colorMap[affiliation];  //color circle according to affiliation
    circleStyle.fill = customFill ? customFill : "rgb(255, 255, 128)";
    if (state.selected) {
      circleStyle.stroke = "#FF0000";
    }
    return {image: IconFactory.circle(circleStyle)}
  }

  SpotDataPainterFallBack.prototype._paintSmallScale = function(geoCanvas, feature, shape, layer, map, state) {
    var circleStyle = getCircleStyle(feature.properties, 18, state);
    geoCanvas.drawIcon(shape, circleStyle);
  };

  SpotDataPainterFallBack.prototype.getDetailLevelScales = function() {
    return [1 / 189000];
  };

  SpotDataPainterFallBack.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {

    var circleStyle;
    if (state.level === 0) {  //zoomed out -> draw small circle
      this._paintSmallScale(geoCanvas, feature, shape, layer, map, state);
    } else { // zoomed in -> do the same, but larger
      circleStyle = getCircleStyle(feature.properties, 32, state);
      geoCanvas.drawIcon(shape, circleStyle);
    }
  };

  return SpotDataPainterFallBack;

});