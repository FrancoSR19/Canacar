define([
  "luciad/shape/ShapeType",
  "../AnnotationInfoPanel",
  "../BlueForcesInfoPanel",
  "../Gazetteer",
  "../ObjectWithUUIDCreateController",
  "../spotreports/SpotBalloonContentProvider",
  "../ThemeManager",
  "../ui/CreateControllerButton",
  "./BlueForcesPainter!",
  "./SpotDataPainter!",
  "../../common/store/RestStore",
  "dijit/registry",
  "dojo/dom-style",
  "dojo/query",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/LayerType",
  "luciad/view/style/LineMarkerType",
  "samples/common/store/WebSocketNotifier"
], function(ShapeType, AnnotationInfoPanel, BlueForcesInfoPanel, Gazetteer, ObjectWithUUIDCreateController,
            SpotBalloonContentProvider, ThemeManager, CreateControllerButton, BlueForcesPainter, SpotDataPainter,
            RestStore, registry, domStyle, dQuery, FeatureModel, MemoryStore, ShapeFactory, FeatureLayer,
            FeaturePainter, LayerType, LineMarkerType, WebSocketNotifier) {

  var SPOT_DATA_URL = "../../../../db/dataobject/report/";
  var SPOT_NOTIFICATIONS_WS = "ws://"+ window.location.host + "/db/notifications";

  function createSpotReportLayer(context) {

    var spotReportStore = new RestStore({target: SPOT_DATA_URL});
    // WebSocketNotifier wraps around the store to listen for updates and update the wrapped model
    var MonitorStore = new WebSocketNotifier({
      target: SPOT_NOTIFICATIONS_WS,
      delegateStore: spotReportStore,
      interval: 700
    });

    var gazetModel = new FeatureModel(spotReportStore);
    var reportPainter = new SpotDataPainter();

    var spotReportLayer = new FeatureLayer(gazetModel, {
      label: "Spot reports",
      id: "spotLayer",
      painter: reportPainter
    });
    spotReportLayer.selectable = true;

    spotReportLayer.balloonContentProvider = SpotBalloonContentProvider.makeProvider(context.map,
        spotReportLayer, SPOT_DATA_URL);

    return spotReportLayer;
  }

  function createAnnotationLayer(context) {

    var annotationURL = "../../../../annotation/";
    var annotationStore = new RestStore({target: annotationURL});
    // WebSocketNotifier wraps around the store to listen for updates and update the wrapped model
    var monitorStore = new WebSocketNotifier({
      target: "ws://"+ window.location.host + "/annotation/notifications",
      interval: 700,
      delegateStore: annotationStore
    });
    var annotationPainter = new FeaturePainter();
    annotationPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
      if (state.selected) {
        geoCanvas.drawShape(shape, {
          stroke: {
            color: "rgba(255,0,0,1)",
            width: 2,
            endMarker: {type: LineMarkerType.ARROW, size: 20}
          },
          fill: { color: "rgba(0,255,0,0.5)" }
        });
      } else {
        geoCanvas.drawShape(shape, {
          stroke: {
            color: "rgba(255,255,255,1)",
            width: 2,
            endMarker: {type: LineMarkerType.ARROW, size: 20}
          },
          fill: { color: "rgba(0,255,0,0.5)" }
        });

      }
    };

    var annotationModel = new FeatureModel(annotationStore, {});
    var annotationLayer = new FeatureLayer(annotationModel, {
      label: "Annotations",
      layerType: LayerType.DYNAMIC,
      painter: annotationPainter,
      selectable: true,
      editable: true
    });

    annotationLayer.balloonContentProvider = function(o) {
      return new AnnotationInfoPanel({annotation: o, layer: annotationLayer, map: context.map}).domNode;
    };

    return annotationLayer;
  }

  var theTheme = {
    name: "Mission Control",
    activate: function(context) {

      dQuery(".securityBanner")
          .removeClass("securityBannerClassified")
          .addClass("securityBannerClassified")
          .forEach(function(n) {
            n.innerHTML = "DEMO SECRET";
          });

      Gazetteer.enable(context.map);

      var _annotationtitlepane = registry.byId("annotationTitlePane");
      _annotationtitlepane.set("open", true);
      domStyle.set(_annotationtitlepane.domNode, "display", "block");

      if (!this._spotReportLayer) {
        this._spotReportLayer = createSpotReportLayer(context);
      }
      context.map.layerTree.addChild(this._spotReportLayer);

      if (!this._unitsLayer) {

        var blueforcesMemoryStore = new MemoryStore();

        this._unitsStore = new WebSocketNotifier({
          target: "ws://"+ window.location.host + "/blueforces/",
          delegateStore: blueforcesMemoryStore,
          interval: 500
        });
        var unitsModel = new FeatureModel(blueforcesMemoryStore, {
        });

        var unitsPainter = new BlueForcesPainter();

        this._unitsLayer = new FeatureLayer(
            unitsModel,
            {
              label: "Blue forces",
              layerType: LayerType.DYNAMIC,
              painter: unitsPainter
            }
        );

        //create a balloon provider that creates a new widget for each balloon
        var self = this;
        this._unitsLayer.balloonContentProvider = function(o) {
          if (self.blueForcesBalloonPanel) {
            self.blueForcesBalloonPanel.destroy();
          }
          self.blueForcesBalloonPanel = new BlueForcesInfoPanel({unit: o, layer: self._unitsLayer});
          return self.blueForcesBalloonPanel.domNode;
        };

        this._unitsLayer.workingSet.on("WorkingSetChanged", function(eventType, feature, id) {
          if (eventType === "update" && self.blueForcesBalloonPanel) {
            self.blueForcesBalloonPanel.refresh(feature, id);
          }
        });

        this._unitsLayer.selectable = true;
      }
      context.map.layerTree.addChild(this._unitsLayer);

      if (!this._createController) {
        var spotCreateController = new ObjectWithUUIDCreateController(ShapeType.POINT, {
          code: "SUZP--------***",
          date: null
        });
        this._createController = new CreateControllerButton(spotCreateController, "spotCreateContainer", context.map,
            this._spotReportLayer);
      }

      if (!this._annotationLayer) {
        this._annotationLayer = createAnnotationLayer(context);
      }
      context.map.layerTree.addChild(this._annotationLayer);
      if (!this._controllersAdded) {
        new CreateControllerButton(new ObjectWithUUIDCreateController(ShapeType.POLYGON, {text: null}),
            "polygonContainer",
            context.map, this._annotationLayer);
        new CreateControllerButton(new ObjectWithUUIDCreateController(ShapeType.POLYLINE,
            {text: null, lineCapDecoration: "arrow"}), "polylineContainer", context.map, this._annotationLayer);
        this._controllersAdded = true;
      }

      if (!context.firstLoad) {
        context.map.mapNavigator.fit(
            {
              bounds: ShapeFactory.createBounds(this._spotReportLayer.model.reference, [-71.10, .15, 42.3, .15]),
              animate: true
            });
      }

    },
    deactivate: function(context) {

      var _annotationtitlepane = registry.byId("annotationTitlePane");
      domStyle.set(_annotationtitlepane.domNode, "display", "none");
      context.map.layerTree.removeChild(this._unitsLayer);
      this._unitsLayer = null;
      if (this._unitsStore) {
        this._unitsStore.destroy();
        this._unitsStore = null;
      }

      dQuery(".securityBanner")
          .removeClass("securityBannerClassified")
          .forEach(function(n) {
            n.innerHTML = "UNCLASSIFIED";
          });

      if (this._spotReportLayer) {
        context.map.layerTree.removeChild(this._spotReportLayer);
      }
      if (this._unitsLayer) {
        context.map.layerTree.removeChild(this._unitsLayer);
      }
      if (this._annotationLayer) {
        context.map.layerTree.removeChild(this._annotationLayer);
      }
      context.map.controller = null;
      Gazetteer.disable();

    },
    toString: function() {
      return this.name;
    }
  };

  ThemeManager.register(theTheme);
  return theTheme;
});
