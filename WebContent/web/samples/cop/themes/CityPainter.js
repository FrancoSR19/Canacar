define([
  "luciad/view/feature/FeaturePainter",
  "luciad/view/style/PointLabelPosition",
  "samples/common/IconFactory"
], function(FeaturePainter, PointLabelPosition, IconFactory) {

  var HUGE_CITY_COLOR = "rgb( 255, 127, 57 )",
      HUGE_CITY_FONT = "18px Arial",
      LARGE_CITY_COLOR = "rgb( 246, 153, 26 )",
      LARGE_CITY_FONT = "16px Arial",
      MEDIUM_CITY_COLOR = "rgb( 255, 174, 75 )",
      MEDIUM_CITY_FONT = "14px Arial",
      SMALL_CITY_COLOR = "rgb(255, 194, 75)",
      SMALL_CITY_FONT = "12px Arial",
      HUGE_CITY_ICON_STYLE = {
        image: IconFactory.circle({
          fill: HUGE_CITY_COLOR,
          stroke: HUGE_CITY_COLOR,
          width: 10,
          height: 10
        }),
        width: "10px",
        height: "10px"
      },
      LARGE_CITY_ICON_STYLE = {
        image: IconFactory.circle({
          fill: LARGE_CITY_COLOR,
          stroke: LARGE_CITY_COLOR,
          width: 8,
          height: 8
        }),
        width: "8px",
        height: "8px"
      },
      MEDIUM_CITY_ICON_STYLE = {
        image: IconFactory.circle({
          fill: MEDIUM_CITY_COLOR,
          stroke: MEDIUM_CITY_COLOR,
          width: 6,
          height: 6
        }),
        width: "6px",
        height: "6px"
      },
      SMALL_CITY_ICON_STYLE = {
        image: IconFactory.circle({
          fill: SMALL_CITY_COLOR,
          stroke: SMALL_CITY_COLOR,
          width: 4,
          height: 4
        }),
        width: "4px",
        height: "4px"
      };

  var POINT_LABEL_POSITIONS = PointLabelPosition.NORTH_EAST | PointLabelPosition.NORTH_WEST |
                              PointLabelPosition.SOUTH_EAST | PointLabelPosition.SOUTH_WEST;

  function CitiesPainter() {
  }

  CitiesPainter.prototype = new FeaturePainter();
  CitiesPainter.prototype.constructor = CitiesPainter;

  CitiesPainter.prototype.getDetailLevelScales = function() {
    return [ 1.0 / 18000000 ];
  };

  CitiesPainter.prototype.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
    var paintLevel = paintState.level,
        population = feature.properties.TOT_POP;

    if (population > 1000000) {
      geocanvas.drawIcon(shape, HUGE_CITY_ICON_STYLE);
    }
    else if (population > 500000) {
      geocanvas.drawIcon(shape, LARGE_CITY_ICON_STYLE);
    }
    else if (population > 250000) {
      geocanvas.drawIcon(shape, MEDIUM_CITY_ICON_STYLE);
    }
    else if (paintLevel > 0) {
      geocanvas.drawIcon(shape, SMALL_CITY_ICON_STYLE);
    }
  };


  var cityLabelTemplate = '<div style="color:#FFFFFF; font: $font; text-shadow: 1px 1px 1px #000000, 1px -1px 1px #000000, -1px 1px 1px #000000, -1px -1px 1px #000000, 0px -1px 1px #000000, -1px 0px 1px #000000, 1px 0px 1px #000000, 0px 1px 1px #000000">$city</div>';

  CitiesPainter.prototype.paintLabel = function(labelcanvas, feature, shape, layer, map,
                                                paintState) {
    var paintLevel = paintState.level,
        name = feature.properties.CITY,
        population = feature.properties.TOT_POP,
        labelStyle = {
          positions: POINT_LABEL_POSITIONS,
          priority: -population
        };


    name = cityLabelTemplate.replace("$city",name);

    if (population > 1000000) {
      name = name.replace("$font",HUGE_CITY_FONT);
      labelcanvas.drawLabel(name, shape, labelStyle);
    } else if (population > 500000) {
      name = name.replace("$font",LARGE_CITY_FONT);
      labelcanvas.drawLabel(name, shape, labelStyle);
    } else if (population > 250000) {
      name = name.replace("$font",MEDIUM_CITY_FONT);
      labelcanvas.drawLabel(name, shape, labelStyle);
    }
    else if (paintLevel > 0) {
      name = name.replace("$font",SMALL_CITY_FONT);
      labelcanvas.drawLabel(name, shape, labelStyle);
    }

  };

  return CitiesPainter;
});