define([
      './SpotReportInfoPanel!'
    ], function(SpotReportInfoPanel) {
      /**
       * @fileOverview Content provider for spot report balloons
       */
      return {
        makeProvider: function(map, layer, spotdataurl) {
          var panel = null;
          return  function(o) {
            if (!!panel) {
              panel.destroy();
            }

            panel = new SpotReportInfoPanel({map: map, spotreport: o, spotdataurl: spotdataurl, layer: layer});
            return panel.domNode;
          }
        }
      };
    }
);




