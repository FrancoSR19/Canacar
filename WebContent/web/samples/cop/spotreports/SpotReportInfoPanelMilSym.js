define([
  "./SpotReportInfoPanelFallback",
  "dojo/_base/declare",
  "dojo/dom-construct",
  "dojo/parser",
  "luciad/symbology/SymbologyProvider!MIL_STD_2525c",
  "samples/symbology/SymbologyPropertiesPanel",
  "dojo/text!samples/cop/css/balloonMilSym.css"
], function(SpotReportInfoPanelFallback, declare, domConstruct, parser, MIL_STD_2525c,
            SymbologyPropertiesPanel, balloonMilSymCSS) {

  var style = document.createElement('style');
  style.type = 'text/css';
  style.innerHTML = balloonMilSymCSS;
  document.getElementsByTagName('head')[0].appendChild(style);

  var SidicSelectorWidget = declare(null, {

    constructor: function(containerDiv) {
      parser.parse();
      this._propertiesPanel = new SymbologyPropertiesPanel(containerDiv.id);

      var self = this;
      this._propertiesPanel.onSymbolChanged = function(event) {
        self.setSidic(event.symbol.code);
      };

      domConstruct.place(this._propertiesPanel.domNode, containerDiv);
      domConstruct.place("<div style=\"clear: both;\"></div>",containerDiv, "last");
    },

    getSidic: function() {
      return this._propertiesPanel.getCode();
    },

    setSidic: function(sidic) {
      this._propertiesPanel.setMilitarySymbol(MIL_STD_2525c, sidic);
    },

    destroy: function() {
      this._propertiesPanel.destroy();
    }

  });

  return declare([SpotReportInfoPanelFallback], {

    _initializeSidicSelector: function(sidic) {
      this._sidicSelectorWidget = new SidicSelectorWidget(this._sidicSelectorDiv);
      this._sidicSelectorWidget.setSidic(sidic);
    }

  });
});
