define([
  'luciad/shape/format/LonLatPointFormat',
  'dojo/_base/lang',
  'dojo/_base/declare',
  "dojo/_base/Deferred",
  'dojo/query',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  "dijit/_WidgetsInTemplateMixin",
  "dijit/registry",
  "./PictureBox",
  "./SimpleSidicSelector",
  "dojo/text!../templates/SpotReportInfoPanel.html",
  //widgets in template
  "dijit/form/Form",
  "dijit/form/Button",
  "dijit/form/TextBox",
  "dojo/data/ItemFileReadStore",
  "dijit/form/ComboBox",
  "samples/cop/ui/LongitudeTextBox",
  "samples/cop/ui/LatitudeTextBox",
  "samples/cop/ui/StatusBox"
], function(LonLatPointFormat, lang, declare, Deferred, query, domConstruct, _WidgetBase,
            _TemplatedMixin,
            _WidgetsInTemplateMixin, registry, PictureBox, SimpleSidicSelector,
            SpotReportInfoPanelTemplateHTML) {

  function destroy(node) {
    var node = registry.byNode(node);
    if (node) {
      node.destroyRecursive();
    }
  }

  function getDisplayValue(form, name) {
    var input = form.getChildren().filter(function(e) {
      return e.name === name;
    });
    return input[0].getDisplayedValue();
  }

  function parseDMStoDD(value) {

    var deg = 0, min = 0, sec = 0;
    var isNegative = false;
    var start = 0;
    var index = value.indexOf("\u00b0", start);
    if (index < 0) {
      return undefined;
    }

    if (index === 0) {
      deg = 0;
    } else {

      deg = parseInt(value.substring(0, index), 10);
      if (isNaN(deg) || deg < this._degreesMin || deg > this._degreesMax) {
        return undefined;
      } else if (deg < 0) {
        deg = Math.abs(deg);
        isNegative = true;
      }
    }

    start = index + 1;
    index = value.indexOf("\'", start);
    if (index < 0) {
      min = 0;
    } else if (index === 0) {
      min = 0;
      start = index + 1;
    } else {
      min = parseInt(value.substr(start, index - start), 10);
      if (isNaN(min) || min < 0.0 || min > 60.0) {
        return undefined;
      }
      start = index + 1;
    }

    index = value.indexOf("\"", start);
    if (index <= 0) {
      sec = 0;
    } else {
      sec = parseFloat(value.substr(start, index - start));
      if (isNaN(sec) || sec < 0.0 || sec > 60.0) {
        return undefined;
      }
    }
    var result = deg + (min / 60.0) + sec / 3600.0;
    if (isNegative) {
      result = -result;
    }

    return result;

  }


  /**
   * @fileOverview
   * The widget used in the spot report balloon.
   */
  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: SpotReportInfoPanelTemplateHTML,
    _spotdatamodel: null,
    layer: null,
    map: null,
    spotreport: null,
    spotdataurl: null,

    _spotpane: null,

    _refreshstatus: null,
    _crudstatustimer: null,
    _refreshstatustimer: null,
    //the fields below will be bound to by the dijit templating mechanism
    _crudstatus: null,
    _btndelete: null,
    _btnupdate: null,

    constructor: function() {
      this._lonlatformatter = new LonLatPointFormat({pattern: 'lat(+DMS2),lon(+DMS2)'});
    },

    /**
     * _WidgetBase life-cycle method
     */
    postMixInProperties: function() {
      this._spotdatamodel = this.layer.model;
    },

    _initializeSidicSelector: function(sidic) {
      this._sidicSelectorWidget = new SimpleSidicSelector();
      domConstruct.place(this._sidicSelectorWidget.domNode, this._sidicSelectorDiv);
      this._sidicSelectorWidget.setSidic(sidic);
    },

    /**
     * _WidgetBase life-cycle method
     */
    postCreate: function() {
      this.inherited(arguments);

      this.form.connectChildren();
      //subscribe to formevents
      this.form.watch("state", lang.hitch(this, this._formValidStateChange));

      //fill in values in the form
      var props = lang.clone(this.spotreport.properties);

      props.lon = this._lonlatformatter.formatLon(this.spotreport.shape.x);
      props.lat = this._lonlatformatter.formatLat(this.spotreport.shape.y);

      this.form.setValues(props);


      this._displayPictures(props.attachments);

      this._initializeSidicSelector(props.code);
    },

    refreshSpotData: function() {
      this._btndelete.attr("disabled", true);
      this._btnupdate.attr("disabled", true);
      this._refreshstatus.setBusy();
      this._spotdatamodel.refresh();
    },

    /**
     * Called when the 'Delete' button is clicked.
     */
    deleteSpotData: function() {
      Deferred.when(
          this._spotdatamodel.remove(this.spotreport.id),
          lang.hitch(this, this._onDeleteFeature),
          lang.hitch(this, this._onDeleteFeatureError)
      );

    },

    /**
     * Called when the 'state' attribute of our form changes. So
     * we can prevent invalid data from being submitted.
     */
    _formValidStateChange: function(attr, oldVal, newVal) {
      this._btnupdate.attr("disabled", newVal !== "");
    },

    /**
     * Called when the 'Update' button is clicked.
     */
    updateSpotData: function() {

      var featCopy = this.spotreport.copy();

      var props = this.form.getValues();

      //dojo text box not applying these values. work around.
      var lon = parseDMStoDD(getDisplayValue(this.form, "lon"));
      var lat = parseDMStoDD(getDisplayValue(this.form, "lat"));
      lon = parseFloat(lon);
      lat = parseFloat(lat);
      featCopy.shape.move2D(lon, lat);

      delete props.lon;
      delete props.lat;
      lang.mixin(featCopy.properties, props);
      featCopy.properties.code = this._sidicSelectorWidget.getSidic();

      this._btndelete.attr("disabled", true);
      this._btnupdate.attr("disabled", true);
      this._crudstatus.setBusy();
      Deferred.when(this._spotdatamodel.put(featCopy),
          lang.hitch(this, this._onUpdateFeature),
          lang.hitch(this, this._onUpdateFeatureError));
    },

    _onDeleteFeature: function() {
      this._setCrudSuccess();
    },

    _onDeleteFeatureError: function() {
      this._setCrudFail();
    },

    _onUpdateFeature: function() {
    },
    _onUpdateFeatureError: function() {
    },

    /***********************************************************************/
    /*                            Visual goodies                           */
    /***********************************************************************/

    _setRefreshSuccess: function() {
      var rs = this._refreshstatus.setSuccess();

      if (this._refreshstatustimer) {
        clearTimeout(this._refreshstatustimer);
        this._refreshstatustimer = null;
      }
      this._refreshstatustimer = setTimeout(lang.hitch(this, this._clearRefreshStatus), 3000);
    },

    _clearRefreshStatus: function() {
      this._refreshstatus.clear();
      this._refreshstatustimer = null;
    },

    _setCrudSuccess: function() {
      if (this._crudstatus) {
        this._crudstatus.setSuccess();
      }

      if (this._crudstatustimer) {
        clearTimeout(this._crudstatustimer);
        this._crudstatustimer = null;
      }
      this._crudstatustimer = setTimeout(lang.hitch(this, this._clearCrudStatus), 3000);
    },

    _clearCrudStatus: function() {
      if (this._crudstatus) {
        this._crudstatus.clear();
      }
      this._crudstatustimer = null;
    },

    _setCrudFail: function() {
      if (this._crudstatus) {
        this._crudstatus.setFail();
      }

      if (this._crudstatustimer) {
        clearTimeout(this._crudstatustimer);
        this._crudstatustimer = null;
      }
      this._crudstatustimer = setTimeout(lang.hitch(this, this._clearCrudStatus), 3000);
    },

    _clearPictures: function() {
      query("[widgetId]", this.picturebox).forEach(destroy);
      this.picturebox.innerHTML = "";
    },

    /**
     * Displays a set of attachments of a spot report.
     * @private
     */
    _displayPictures: function(att) {

      this._clearPictures();

      if (!att) {
        return;
      }
      var self = this;
      att.forEach(function(p) {
        domConstruct.place(new PictureBox({imgURL: self.spotdataurl + p}).domNode, self.picturebox, "last");
      });
    },

    destroy: function() {
      this._btndelete.attr("disabled", false);
      this._btnupdate.attr("disabled", false);
      this._sidicSelectorWidget.destroy();
      this.inherited(arguments);
    }
  })
});
