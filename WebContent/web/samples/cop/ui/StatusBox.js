define([
  'dojo/_base/declare',
  'dojo/dom-class',
  "dijit/_WidgetBase",
  "dijit/_TemplatedMixin"
], function(declare, domClass, _WidgetBase, _TemplatedMixin) {
  return declare("cop.ui.StatusBox", [_WidgetBase, _TemplatedMixin], {

    templateString: "<div class='StatusBox'></div>",

    busyicon: "./resources/icons/busy.gif",
    successicon: "./resources/icons/success.png",
    failicon: "./resources/icons/fail.png",

    _clearContent: function() {
      this.domNode.innerHTML = "&nbsp;";
    },

    _removeClasses: function() {
      domClass.remove(this.domNode, "StatusBoxFail");
      domClass.remove(this.domNode, "StatusBoxSuccess");
      domClass.remove(this.domNode, "StatusBoxBusy");
    },

    clear: function() {
      this._clearContent();
      this._removeClasses();
    },

    setSuccess: function() {
      this.clear();
      domClass.add(this.domNode, "StatusBoxSuccess");
      this.domNode.innerHTML = "<img src='" + this.successicon + "'></img>";
    },

    setFail: function() {
      this.clear();
      domClass.add(this.domNode, "StatusBoxFail");
      this.domNode.innerHTML = "<img src='" + this.failicon + "'></img>";
    },

    setBusy: function() {
      this.clear();
      domClass.add(this.domNode, "StatusBoxBusy");
      this.domNode.innerHTML = "<img src='" + this.busyicon + "'></img>";
    }

  })
});
  