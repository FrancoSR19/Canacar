define([
  'dojo/_base/declare',
  'dijit/form/NumberTextBox',
  './_LonLatMixin'
], function(declare, NumberTextBox, _LonLatMixin) {
  return declare("cop.ui.LongitudeTextBox", [NumberTextBox, _LonLatMixin], {
    _degreesMin: -180.0,
    _degreesMax: 180.0

  });
});
