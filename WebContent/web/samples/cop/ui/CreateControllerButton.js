define([
  'dojo/query',
  'dojo/aspect'
], function(query, aspect) {

  return function(controller, aButtonNodeId, aMap, aLayer) {

    var buttonNode = query("#" + aButtonNodeId);

    function _toggleState() {
      if (aMap.controller != controller) {
        //ensure that the layer for the annotation is added to the map
        if (!aMap.layerTree.findLayerById(aLayer.id)) {
          aMap.layerTree.addChild(aLayer);
        }
        aMap.controller = controller;
        buttonNode.addClass("LuciadControllerActive");
      }
      else {
        buttonNode.removeClass("LuciadControllerActive");
        aMap.controller = null;
      }
    }

    aspect.after(controller, "onDeactivate", function() {
      buttonNode.removeClass("LuciadControllerActive");
    });

    buttonNode.on("click", _toggleState);

    controller.onChooseLayer = function() {
      return aLayer;
    };
  };
});


