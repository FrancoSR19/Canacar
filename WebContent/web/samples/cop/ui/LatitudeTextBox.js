define([
  'dojo/_base/declare',
  'dijit/form/NumberTextBox',
  './_LonLatMixin'
], function(declare, NumberTextBox, _LonLatMixin) {
  return declare("cop.ui.LatitudeTextBox", [NumberTextBox, _LonLatMixin], {
    _degreesMin: -90.0,
    _degreesMax: 90.0


  });
});
