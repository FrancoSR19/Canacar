define([
  "samples/common/store/RestStore",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetBase",
  "dijit/form/ComboBox",
  "dojo/_base/declare",
  "dojo/dom-class",
  "dojo/text!./templates/GazetteerControl.html",
  "luciad/model/feature/FeatureModel",
  "luciad/reference/ReferenceProvider",
  "luciad/transformation/TransformationFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/loadingstrategy/LoadEverything",
  "luciad/view/feature/QueryProvider"
], function(RestStore, _TemplatedMixin, _WidgetBase, ComboBox, declare, domClass, Template, FeatureModel,
            ReferenceProvider,
            TransformationFactory, FeatureLayer, LoadEverything, QueryProvider) {
  var WGS84 = ReferenceProvider.getReference("CRS:84");
  /**
   * @fileOverview
   * The GazetteerControl contains a combobox and some buttons to query the gazetteer service
   * for all features of a given class in the bounding box of the map.
   * @extends  dijit._WidgetBase
   */
  return declare([_WidgetBase, _TemplatedMixin], {

    _map: null,
    _url: null,
    _layer: null,
    _comboBox: null,

    _gazetteerTypesStore: null,

    map: {},
    url: "",
    searchAttr: "",
    store: {},
    value: "",
    painter: null,
    transformer: null,

    templateString: Template,

    postMixInProperties: function() {
      this._url = this.url;
      this._map = this.map;
    },

    buildRendering: function() {
      this.inherited(arguments);
      this._comboBox = new ComboBox({
        store: this.store,
        value: this.value,
        searchAttr: this.searchAttr
      }, this._comboBoxNode);
      domClass.add(this._comboBox.domNode, "GazetteerSearchBox");
    },

    postCreate: function() {
      this.connect(this._btnSearch, "click", this._doSearch);
      this.connect(this._btnClear, "click", this._doClear);
    },

    _doClear: function() {
      if (this._layer) {
        this._map.layerTree.removeChild(this._layer);
        this._layer = null;
      }
    },

    _doSearch: function() {
      var mapBounds = this._map.mapBounds;
      var toWgs84 = TransformationFactory.createTransformation(mapBounds.reference, WGS84);
      var modelBounds = toWgs84.transformBounds(mapBounds);
      var clazz = this._comboBox.get("value");

      this._doClear();

      if (!this._layer) {
        var url = this._url + "/search";
        var restStore = new RestStore({target: url});
        delete restStore.add;
        delete restStore.put;
        delete restStore.remove;
        delete restStore.get;
        var gazetModel = new FeatureModel(restStore, {});

        //#snippet filterprops
        var gazetteerQueryProvider = new QueryProvider();
        gazetteerQueryProvider.getQueryLevelScales = function(layer, map) {
          return [];
        };
        gazetteerQueryProvider.getQueryForLevel = function(level) {
          return {
            "class": clazz,
            minX: modelBounds.x,
            minY: modelBounds.y,
            maxX: (modelBounds.x + modelBounds.width ),
            maxY: (modelBounds.y + modelBounds.height),
            fields: "featureName,featureClass,countyName",
            limit: 500
          };
        };
        //#endsnippet filterprops

        this._layer = new FeatureLayer(gazetModel, {
          label: "Places",
          id: "gazetlayer",
          loadingStrategy: new LoadEverything({
            queryProvider: gazetteerQueryProvider
          }),
          painter: this.painter,
          transformer: this.transformer,
          selectable: true
        });
      }

      if (!this._map.layerTree.findLayerById(this._layer.id)) {
        this._map.layerTree.addChild(this._layer);
      }
    },

    disable: function() {
      if (this._layer) {
        this._map.layerTree.removeChild(this._layer);
        this._layer = null;
      }
    }
  });
});