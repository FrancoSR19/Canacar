define([
      "dojo/dom-style",
      "dojo/store/Memory",
      "dijit/registry",
      "luciad/view/feature/FeaturePainter",
      "luciad/view/style/PointLabelPosition",
      "luciad/view/feature/transformation/ClusteringTransformer",
      "samples/cop/ui/GazetteerControl",
      "samples/common/IconFactory",
      "dojo/text!samples/cop/gazetteer.json"
    ], function(domStyle, MemoryStore, registry, FeaturePainter, PointLabelPosition,
                ClusteringTransformer,
                GazetteerControl, IconFactory, gazeteerTypes) {

      var selectionColor = "rgb(192,217,42)";

      function retrieveIconStyle(feature) {
        if (feature.properties.featureClass === "School") {
          return {
            width: "28px",
            height: "28px",
            url: "./resources/icons/school_framed_36.png"
          };
        } else if (feature.properties.featureClass === "Airport") {
          if (feature.properties.featureName.match("Heliport") || feature.properties.featureName.match("heliport")) {
            return {
              width: "22px",
              height: "22px",
              url: "./resources/icons/heliport.png"
            };
          }
          else {
            return {
              width: "22px",
              height: "22px",
              url: "./resources/icons/airport.png"
            };
          }
        } else if (feature.properties.featureClass === "Hospital") {
          return {
            width: "28px",
            height: "28px",
            url: "./resources/icons/hospital.png"
          };
        }
        return null;
      }

      function createGazetteerPainter() {
        var painter = new FeaturePainter();
        var gazetteerLabelTemplate = '<div style="color:#FFFFFF; font: 14pt Calibri; text-shadow: 1px 1px 1px #000000, 1px -1px 1px #000000, -1px 1px 1px #000000, -1px -1px 1px #000000, 0px -1px 1px #000000, -1px 0px 1px #000000, 1px 0px 1px #000000, 0px 1px 1px #000000">$label</div>';
        painter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
          var lbl = gazetteerLabelTemplate.replace("$label", feature.properties.featureName);
          if (state.selected) {
            labelCanvas.drawLabel(lbl, shape, {});
          }
        };
        painter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
          if (state.selected) {
            geoCanvas.drawIcon(
                shape,
                {
                  width: "30px",
                  height: "30px",
                  image: IconFactory.rectangle({width: 30, height: 30, stroke: selectionColor})
                }
            );
          }
          var iconStyle = retrieveIconStyle(feature);
          if (iconStyle) {
            geoCanvas.drawIcon(shape, iconStyle);
          }
        };
        return painter;
      }

      function createClusteringTransformer() {
        return ClusteringTransformer.create();
      }

      function calculateClusterSize(cluster) {
        var elements = ClusteringTransformer.clusteredFeatures(cluster);
        var scaleFactor = Math.round(clamp(Math.log(elements.length) / Math.log(10), 1.33, 3));
        if (scaleFactor < 2) {
          return 26;
        } else if (scaleFactor == 2) {
          return 52;
        } else {
          return 78;
        }
      }

      function createClusterIconStyle(cluster) {
        var elements = ClusteringTransformer.clusteredFeatures(cluster);
        var feature = elements[0];
        var fill;
        if (feature.properties.featureClass === "School" ||
            feature.properties.featureClass === "Hospital") {
          fill = "rgb(128,224,255)";
        } else {
          fill = "rgb(255,228,181)";
        }
        var size = calculateClusterSize(cluster);
        return {
          width: "100%",
          height: "100%",
          image: IconFactory.circle({
            width: size,
            height: size,
            stroke: "#000000",
            strokeWidth: 3,
            fill: fill
          })
        };
      }

      function decorateWithClusteringPainter(singleElementPainter) {
        var painter = new FeaturePainter();

        var clusterLabelTemplate =
            '<div style="color:#000000; font: 11pt bold SansSerif;">$label</div>';

        painter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
          if (ClusteringTransformer.isCluster(feature)) {
            geoCanvas.drawIcon(shape, createClusterIconStyle(feature));

            if (state.selected) {
              var size = calculateClusterSize(feature) + 4;
              geoCanvas.drawIcon(shape, {
                width: size + "px",
                height: size + "px",
                image: IconFactory.rectangle({width: size, height: size, stroke: selectionColor})
              }, feature);
            }
          } else {
            singleElementPainter.paintBody(geoCanvas, feature, shape, layer, map, state);
          }
        };

        painter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
          if (ClusteringTransformer.isCluster(feature)) {
            var elements = ClusteringTransformer.clusteredFeatures(feature);
            labelCanvas.drawLabel(clusterLabelTemplate.replace("$label", elements.length), shape, {
              positions: PointLabelPosition.CENTER
            })
          } else {
            singleElementPainter.paintLabel(labelCanvas, feature, shape, layer, map, state);
          }
        };
        return painter;
      }

      function clamp(value, min, max) {
        if (value > max) {
          value = max;
        }
        if (value < min) {
          value = min;
        }
        return value;
      }

      var gazetStore = new MemoryStore({
        data: JSON.parse(gazeteerTypes)
      });
      var gazet;

      var painter = decorateWithClusteringPainter(createGazetteerPainter());

      /**
       * @fileOverview
       * The Gazetteer is a component that queries a web service for point features in the current
       * map bounding box. It will automatically add a layer to the map that displays the query results.
       */
      return {
        enable: function(map) {
          gazet = new GazetteerControl({
            map: map,
            url: gazetUrl,
            store: gazetStore,
            searchAttr: "id",
            value: "School",
            painter: painter,
            transformer: createClusteringTransformer()
          }, "gazetteerContainer");
          gazet.startup();
          //run setup only on first init
          this.enable = function(map) {
            var _gazetteertitlepane = registry.byId("gazetteerTitlePane");
            _gazetteertitlepane.set("open", true);
            domStyle.set(_gazetteertitlepane.domNode, "display", "block");
          };
          this.enable(map);
        },
        disable: function() {
          gazet.disable();
          var _gazetteertitlepane = registry.byId("gazetteerTitlePane");
          _gazetteertitlepane.set("open", false);
          domStyle.set(_gazetteertitlepane.domNode, "display", "none");
        }
      };
    }
);
