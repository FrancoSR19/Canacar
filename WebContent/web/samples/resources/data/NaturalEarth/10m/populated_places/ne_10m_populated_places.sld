<?xml version='1.0' encoding='UTF-8'?>
<FeatureTypeStyle xmlns="http://www.opengis.net/se" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:gml="http://www.opengis.net/gml" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/ogc http://schemas.opengis.net/filter/1.1.0/filter.xsd http://www.w3.org/2001/XMLSchema http://www.w3.org/2001/XMLSchema.xsd http://www.opengis.net/se http://schemas.opengis.net/se/1.1.0/FeatureStyle.xsd http://www.opengis.net/gml http://schemas.opengis.net/gml/3.1.1/base/gml.xsd http://www.w3.org/1999/xlink http://www.w3.org/1999/xlink.xsd " version="1.1.0">
  <Description>
    <Title>Populated places</Title>
  </Description>
  <Rule>
    <Description>
      <Title>Large cities</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsBetween>
      <ogc:PropertyName>SCALERANK</ogc:PropertyName>
        <LowerBoundary><ogc:Literal>1</ogc:Literal></LowerBoundary>
        <UpperBoundary><ogc:Literal>2</ogc:Literal></UpperBoundary>
      </ogc:PropertyIsBetween>
      </ogc:Filter>
    <PointSymbolizer>
      <Graphic>
        <ExternalGraphic>
          <InlineContent encoding="base64">iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAH0lEQVQYV2P8DwQMeAAjSEEdIyNWJU1AvUNHAT5vAgDR8jHptuzwOwAAAABJRU5ErkJggg==</InlineContent>
          <Format>image/png</Format>
        </ExternalGraphic>
        <Size>8</Size>
      </Graphic>
    </PointSymbolizer>
    <TextSymbolizer>
      <Label>
        <ogc:PropertyName>NAME</ogc:PropertyName>
      </Label>
      <Font>
        <SvgParameter name="font-family">Tahoma</SvgParameter>
        <SvgParameter name="font-weight">bold</SvgParameter>
        <SvgParameter name="font-size">12</SvgParameter>
      </Font>
      <Halo>
        <Radius>1</Radius>
        <Fill>
          <SvgParameter name="fill">#dddddd</SvgParameter>
        </Fill>
      </Halo>
      <Fill>
        <SvgParameter name="fill">#222222</SvgParameter>
      </Fill>
    </TextSymbolizer>
  </Rule>

  <Rule>
    <Description>
      <Title>Medium cities</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsBetween>
      <ogc:PropertyName>SCALERANK</ogc:PropertyName>
        <LowerBoundary><ogc:Literal>3</ogc:Literal></LowerBoundary>
        <UpperBoundary><ogc:Literal>7</ogc:Literal></UpperBoundary>
      </ogc:PropertyIsBetween>
      </ogc:Filter>
    <MaxScaleDenominator>5.0E6</MaxScaleDenominator>
    <PointSymbolizer>
      <Graphic>
        <ExternalGraphic>
          <InlineContent encoding="base64">iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAHklEQVQIW2P8DwQMWAAjSKKOkRFFqgmoltoS2CwHACVQI+/9x0Y+AAAAAElFTkSuQmCC</InlineContent>
          <Format>image/png</Format>
        </ExternalGraphic>
        <Size>6</Size>
      </Graphic>
    </PointSymbolizer>
    <TextSymbolizer>
      <Label>
        <ogc:PropertyName>NAME</ogc:PropertyName>
      </Label>
      <Font>
        <SvgParameter name="font-family">Tahoma</SvgParameter>
        <SvgParameter name="font-weight">bold</SvgParameter>
        <SvgParameter name="font-size">12</SvgParameter>
      </Font>
      <Halo>
        <Radius>1</Radius>
        <Fill>
          <SvgParameter name="fill">#dddddd</SvgParameter>
        </Fill>
      </Halo>
      <Fill>
        <SvgParameter name="fill">#222222</SvgParameter>
      </Fill>
    </TextSymbolizer>
  </Rule>

  <Rule>
    <Description>
      <Title>Other cities</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsBetween>
      <ogc:PropertyName>SCALERANK</ogc:PropertyName>
        <LowerBoundary><ogc:Literal>8</ogc:Literal></LowerBoundary>
        <UpperBoundary><ogc:Literal>11</ogc:Literal></UpperBoundary>
      </ogc:PropertyIsBetween>
      </ogc:Filter>
    <MaxScaleDenominator>2.0E6</MaxScaleDenominator>
    <PointSymbolizer>
      <Graphic>
      <ExternalGraphic>
          <InlineContent encoding="base64">iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAHklEQVQIW2P8DwQMWAAjSKKOkRFFqgmoltoS2CwHACVQI+/9x0Y+AAAAAElFTkSuQmCC</InlineContent>
          <Format>image/png</Format>
        </ExternalGraphic>
        <Size>6</Size>
      </Graphic>
    </PointSymbolizer>
    <TextSymbolizer>
      <Label>
        <ogc:PropertyName>NAME</ogc:PropertyName>
      </Label>
      <Font>
        <SvgParameter name="font-family">Tahoma</SvgParameter>
        <SvgParameter name="font-weight">bold</SvgParameter>
        <SvgParameter name="font-size">9</SvgParameter>
      </Font>
      <Halo>
        <Radius>1</Radius>
        <Fill>
          <SvgParameter name="fill">#dddddd</SvgParameter>
        </Fill>
      </Halo>
      <Fill>
        <SvgParameter name="fill">#222222</SvgParameter>
      </Fill>
    </TextSymbolizer>
  </Rule>
</FeatureTypeStyle>