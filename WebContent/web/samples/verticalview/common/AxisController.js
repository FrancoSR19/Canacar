define([
  "luciad/shape/ShapeFactory",
  "luciad/view/Map",
  "luciad/view/controller/Controller",
  "luciad/view/controller/HandleEventResult",
  "luciad/view/input/GestureEventType"
], function(ShapeFactory, Map, Controller, HandleEventResult, GestureEventType) {

  function AxisController() {
  };

  AxisController.prototype = Object.create(Controller.prototype);

  AxisController.prototype.onDraw = function(geoCanvas) {
    //no need for drawing anything.
  };

  AxisController.prototype.onActivate = function(map) {
    if (this._map && this._map !== map) {
      throw new Error("A single instance of a AxisController may not be active on multiple maps at the same time.");
    }
    this._map = map;
    Controller.prototype.onActivate.call(this, map);
  };

  AxisController.prototype.onDeactivate = function(map) {
    this._map = null;
    Controller.prototype.onDeactivate.call(this, map);
  };

  AxisController.prototype._mouseMove = function(viewPoint) {
    if (this._map.isInBorder(Map.Border.LEFT, viewPoint.x, viewPoint.y)) {
      this._map._domNode.style.cursor = "n-resize";
    } else if (this._map.isInBorder(Map.Border.BOTTOM, viewPoint.x, viewPoint.y)) {
      this._map._domNode.style.cursor = "w-resize";
    } else {
      this._map._domNode.style.cursor = "default";
    }
    return HandleEventResult.EVENT_HANDLED;
  };

  AxisController.prototype._scaleByFixedAmount = function(viewPoint, isZoomIn) {

    var factor = isZoomIn ? {x: 2, y: 2} : {x: 0.5, y: 0.5};

    if (this._map.isInBorder(Map.Border.LEFT, viewPoint.x, viewPoint.y)) {
      factor.x = 1;
    } else if (this._map.isInBorder(Map.Border.BOTTOM, viewPoint.x, viewPoint.y)) {
      factor.y = 1;
    }

    this._map.mapNavigator.zoom({factor: factor, location: viewPoint, animate: {duration: 250}});

    return HandleEventResult.EVENT_HANDLED;
  };

  AxisController.prototype._scaleByScaleFactor = function(viewPoint, scaleFactor) {

    var targetScale = {x: this._map.mapScale[0], y: this._map.mapScale[1]};

    if (scaleFactor > 0 && !isNaN(scaleFactor) &&
        !(scaleFactor === Number.POSITIVE_INFINITY || scaleFactor === Number.NEGATIVE_INFINITY)) {
      if (this._map.isInBorder(Map.Border.LEFT, viewPoint.x, viewPoint.y)) {
        targetScale.y *= scaleFactor;
      } else if (this._map.isInBorder(Map.Border.BOTTOM, viewPoint.x, viewPoint.y)) {
        targetScale.x *= scaleFactor;
      } else {
        targetScale.x *= scaleFactor;
        targetScale.y *= scaleFactor;
      }
    }

    this._map.mapNavigator.zoom({targetScale: targetScale, location: viewPoint});

    return HandleEventResult.EVENT_HANDLED;
  };

  //-------------------------------------------------------------------
  //controller API (cf. luciad/view/controller/Controller contract)
  //-------------------------------------------------------------------
  /**
   * handle the user input. The event-object contains information about the type of user-interaction
   */
  AxisController.prototype.onGestureEvent = function(event) {
    var type = event.type;
    var viewPoint = ShapeFactory.createPoint(null, event.viewPosition);

    if (type === GestureEventType.SCROLL) {
      //we only care about scrolls.
      // scrolling upwards => amount > 0 => zoom in
      return this._scaleByFixedAmount(viewPoint, (event.amount > 0));
    } else if (type === GestureEventType.PINCH) {
      //pinch events come with a scaleFactor
      return this._scaleByScaleFactor(viewPoint, event.scaleFactor);
    } else if (type === GestureEventType.MOVE) {
      return this._mouseMove(viewPoint);
    } else {
      return HandleEventResult.EVENT_IGNORED;
    }
  };

  return AxisController;
});

