define([
  "dojo/_base/lang",
  "luciad/reference/ReferenceProvider",
  "luciad/view/Map",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/BasicFeaturePainter",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/view/PaintRepresentation",
  "./TrajectoryShapeProvider",
  "./VerticalViewOptions"
], function(dLang, ReferenceProvider, Map, FeatureLayer, BasicFeaturePainter, FeatureModel, MemoryStore,
            PaintRepresentation, TrajectoryShapeProvider, VerticalViewOptions) {


  /*
   The vertical view is created for a specific air track. A model and layer will be created for the specified
   air track, and this layer will be added to the map. When the air track is updated later on, the previous
   air track is removed from the model and the new one is added.

   The vertical view will also update the shape providers of all available layers if the shape provider is
   set and has a "updateForTrajectory" function. For example the AirspaceShapeProvider has such a function,
   and needs the air track to calculate its results.
   */

  function VerticalView(node, options) {
    options = dLang.mixin({}, VerticalViewOptions, options || {});
    Map.call(this, node, options);
    this._trajectory = null;
    this._trajectoryPainter = options.trajectoryPainter || new BasicFeaturePainter();

    this._trajectoryStore = new MemoryStore();
    this._trajectoryModel = new FeatureModel(this._trajectoryStore, {
      reference: ReferenceProvider.getReference("EPSG:4326")
    });
    this.trajectoryLayer = new FeatureLayer(this._trajectoryModel, {
      painter: this._trajectoryPainter
    });
    this.trajectoryLayer.shapeProvider = new TrajectoryShapeProvider();
    this.trajectoryLayer.setPaintRepresentationVisible(PaintRepresentation.LEFT_BORDER_BODY, true);
    this.trajectoryLayer.setPaintRepresentationVisible(PaintRepresentation.LEFT_BORDER_LABEL, true);

    var self = this;
    this.layerTree.addChild(this.trajectoryLayer, "top");
    this.layerTree.on("NodeAdded", function(/*event*/) {
      self._updateTrajectoryOnAllShapeProviders(self._trajectory);
    });
  }

  VerticalView.prototype = Object.create(Map.prototype);

  VerticalView.prototype.constructor = VerticalView;

  VerticalView.prototype._setNavigationRestriction = function() {

    if (!this._trajectory) {
      return;
    }

    var shape = this.trajectoryLayer.shapeProvider.provideShape(this._trajectory);
    this.restrictNavigationToBounds(shape.bounds, {
      padding: {
        left: 4,
        right: 10,
        top: 10,
        bottom: 0
      }
    });
    this.mapNavigator.fit({bounds: shape.bounds, animate: true}, true);
  };

  VerticalView.prototype._reconfigureTrajectory = function(trajectoryFeature) {
    if (this._trajectory) {
      this._trajectoryModel.remove(this._trajectory.id);
      this._trajectory = null;
    }
    this._trajectory = trajectoryFeature;
    if (trajectoryFeature) {
      this._trajectoryModel.add(trajectoryFeature);
    }
    this._updateTrajectoryOnAllShapeProviders(trajectoryFeature);
    this._setNavigationRestriction();
  };

  VerticalView.prototype._updateTrajectoryOnAllShapeProviders = function(trajectoryFeature) {
    var allLayersArray = this.layerTree.children,
        i, layer, shapeProvider;
    for (i = 0; i < allLayersArray.length; i++) {
      layer = allLayersArray[i];
      shapeProvider = layer.shapeProvider;
      if (shapeProvider && typeof shapeProvider.updateForTrajectory === "function") {
        shapeProvider.updateForTrajectory(trajectoryFeature);
      }
    }
  };

  VerticalView.prototype.fitOnTrajectoryLayer = function() {
    this._fitOnTrajectoryLayer();
  };

  VerticalView.prototype._fitOnTrajectoryLayer = function() {
    var self, handle;

    //if the bounds are defined, we can fit immediately
    if (this.trajectoryLayer.bounds) {
      this.mapNavigator.fit({bounds: this.trajectoryLayer.bounds, animate: true}, true);
    } else {
      //if the bounds are not defined, we can assume that a query is in progress
      self = this;
      handle = this.trajectoryLayer.workingSet.on("QueryFinished", function() {
        self.mapNavigator.fit({bounds: self.trajectoryLayer.bounds, animate: true}, true);
        handle.remove();
      });
    }
  };

  VerticalView.prototype.invalidateTrajectory = function() {
    this._trajectoryModel.put(this._trajectory);
    this._updateTrajectoryOnAllShapeProviders(this._trajectory);
    this._setNavigationRestriction();
    this._fitOnTrajectoryLayer();
  }

  Object.defineProperty(VerticalView.prototype, "trajectory", {
    get: function() {
      return this._trajectory;
    },
    set: function(trajectory) {
      this._reconfigureTrajectory(trajectory);
    }
  });

  return VerticalView;
})
;
