define([
  "luciad/view/feature/ShapeProvider",
  "luciad/geodesy/GeodesyFactory",
  "luciad/geodesy/LineType",
  "luciad/geometry/TopologyFactory",
  "luciad/shape/ShapeFactory",
  "luciad/shape/ShapeType",
  "luciad/reference/ReferenceProvider",
  "luciad/uom/UnitOfMeasureRegistry"
], function(ShapeProvider, GeodesyFactory, LineType, TopologyFactory, ShapeFactory, ShapeType,
            ReferenceProvider, UnitOfMeasureRegistry) {

  function compareDistance(segment1, segment2) {
    return segment1.distance - segment2.distance;
  }

  function AirspaceShapeProvider(lowProp, highProp) {
    this.lowProp = lowProp;
    this.highProp = highProp;
    this.topology = null;
    this._trajectory = null;
    // this provider expects all values to be in meters, and
    // returns cartesian coordinates which are expressed in meters as well
    var meterUnit = UnitOfMeasureRegistry.getUnitOfMeasure("Meter");
    this.reference = ReferenceProvider.createCartesianReference({
      xUnitOfMeasure: meterUnit,
      yUnitOfMeasure: meterUnit
    });
  }

  function contains2D ( intersectionPoints, point ){
    for (var i = 0; i < intersectionPoints.length; i++) {
      if ( intersectionPoints[i].intersectionPoint.equals( point ) ){
        return true;
      }
    }
    return false;
  }

  function isIntersectionEdgeInsideAirspace(intersectionPoint1, intersectionPoint2, airspace) {
    var intermediatePointX = (intersectionPoint1.x + intersectionPoint2.x) / 2;
    var intermediatePointY = (intersectionPoint1.y + intersectionPoint2.y) / 2;

    return airspace.shape.contains2DPoint(ShapeFactory.createPoint(intersectionPoint1.reference,
        [intermediatePointX, intermediatePointY]));
  }

  AirspaceShapeProvider.prototype = new ShapeProvider();
  AirspaceShapeProvider.prototype.constructor = AirspaceShapeProvider;

  AirspaceShapeProvider.prototype.findIntersectionPointsForSegment = function(intersectionPoints, shape, point0,
                                                                              point1) {
    var i, intersectionSegmentData, segments;
    var result = [];

    function segmentFilter(segmentData) {
      return segmentData.shape === shape &&
             ( (segmentData.points[0].equals(point0) && segmentData.points[1].equals(point1)) ||
               (segmentData.points[0].equals(point1) && segmentData.points[1].equals(point0)) );
    }

    for (i = 0; i < intersectionPoints.length; i++) {
      intersectionSegmentData = intersectionPoints[i].intersectionSegments;
      segments = intersectionSegmentData.filter(segmentFilter);
      if (segments.length > 0) {
        result.push({
          point: intersectionPoints[i].intersectionPoint,
          distance: this.geodesy.distance(point0, intersectionPoints[i].intersectionPoint)
        });
      }
    }

    result.sort(compareDistance);
    return result;
  };
  /*
   This shape provider will calculate the intersection between the specified trajectory and the airspace. For
   each airspace passed to this function, it will check whether it intersects with the specified trajectory.
   If it does, a polygon with a cartesian reference is returned.

   The x-coordinates represent the distance between the starting point of the trajectory and the point where
   the trajectory intersects with the air space. A geometry instance is used to determine the intersection
   points.

   The y-coordinates are determined by the minimum and maximum height of the air space, which are available
   as properties on the feature.
   */
  AirspaceShapeProvider.prototype.provideShape = function(airspaceFeature) {
    var trajectoryShape, trajectoryShapePointCount,
        intersectionResult, intersectionPointMetaData,
        trajectoryDistance,
        airspaceSegments,
        point0, point1, i, j,
        low, high;

    if (!this._trajectory) {
      return null;
    }

    if (!this.topology) {
      this.topology = TopologyFactory.createEllipsoidalTopology(airspaceFeature.shape.reference);
    }

    if (!this.geodesy) {
      //#snippet distances
      this.geodesy = GeodesyFactory.createEllipsoidalGeodesy(airspaceFeature.shape.reference);
      //#endsnippet distances
    }

    //only compute intersections for polylines and polygons
    trajectoryShape = this._trajectory.shape;
    if ((trajectoryShape.type !== ShapeType.POLYLINE) && (trajectoryShape.type !== ShapeType.POLYGON)) {
      return null;
    }

    trajectoryShapePointCount = trajectoryShape.pointCount;

    //check if bounds intersect with trajectory
    if (!trajectoryShape.bounds.interacts2D(airspaceFeature.shape.bounds)) {
      return null;
    }

    intersectionResult = this.topology.calculateIntersections(trajectoryShape, airspaceFeature.shape.baseShape);

    airspaceSegments = [];
    if (trajectoryShapePointCount === 1) {
      // 1 point, which is intersection point
      airspaceSegments.push({point: trajectoryShape.getPoint( 0 ), distance: 0});
    } else {
      trajectoryDistance = 0;
      var firstPoint = trajectoryShape.getPoint(0);
      if (airspaceFeature.shape.contains2DPoint(firstPoint) && !contains2D(intersectionResult, firstPoint)) {
        // if the first point of the trajectory is inside the airspace shape, start a polygon at distance 0;
        airspaceSegments.push({point: firstPoint, distance: 0});
      }
      for (i = 1; i < trajectoryShapePointCount; i++) {
        point0 = trajectoryShape.getPoint(i - 1);
        point1 = trajectoryShape.getPoint(i);
        intersectionPointMetaData = this.findIntersectionPointsForSegment(
            intersectionResult,
            trajectoryShape,
            point0,
            point1);

        if (intersectionPointMetaData.length > 0) {
          for (j = 0; j < intersectionPointMetaData.length; j++) {
            var distance = trajectoryDistance + intersectionPointMetaData[j].distance;
            airspaceSegments.push( {point: intersectionPointMetaData[j].point, distance: distance} );
          }
        }
        //#snippet distances
        trajectoryDistance += this.geodesy.distance(point0, point1, LineType.SHORTEST_DISTANCE);
        //#endsnippet distances
      }
    }

    // if last point is inside the polygon, it is added to the segments array
    var lastPoint = trajectoryShape.getPoint(trajectoryShapePointCount - 1);
    if (airspaceFeature.shape.contains2DPoint(lastPoint) && !contains2D(intersectionResult, lastPoint)) {
      airspaceSegments.push( {point: lastPoint, distance: trajectoryDistance});
    }

    low = airspaceFeature.shape.minimumHeight;
    high = airspaceFeature.shape.maximumHeight;

    if(airspaceSegments.length < 2) {
      return null;
    }
    var resultShape = ShapeFactory.createShapeList(this.reference);
    for ( i = 0; i < airspaceSegments.length - 1; i++ ) { // skip last point because we are only checking pairs
      var segment = airspaceSegments[i];
      var next = airspaceSegments[i + 1];
      if ( !isIntersectionEdgeInsideAirspace( segment.point, next.point, airspaceFeature ) ) {
        // console.debug( "Intersection segment does not lie inside airspace " + airspaceId + ". Skipped!" );
        continue;
      }
      resultShape.addShape(
          ShapeFactory.createPolygon(this.reference, [
            ShapeFactory.createPoint(this.reference, [segment.distance, low]),
            ShapeFactory.createPoint(this.reference, [segment.distance, high]),
            ShapeFactory.createPoint(this.reference, [next.distance, high]),
            ShapeFactory.createPoint(this.reference, [next.distance, low])
          ])
      );
    }
    return resultShape;
  };
  /**
   * Specify the trajectory which is currently visible in the vertical view.
   * @param trajectoryFeature The air track
   */
  AirspaceShapeProvider.prototype.updateForTrajectory = function(trajectoryFeature) {
    if (this._trajectory && this._trajectory.id === trajectoryFeature.id) {
      return;
    }
    this._trajectory = trajectoryFeature;
    // changing the trajectory changes the results of this ShapeProvider
    // use the invalidateAll call to inform the layer about the changed state
    this.invalidateAll();
  };

  return AirspaceShapeProvider;
});
