define([
  "dojo/parser",
  "dojo/domReady!"
], function(parser) {

  var executed = false;

  //initialization function for the mobile template.
  return function() {

    if (executed) {
      return;
    }

    executed = true;

    parser.parse();

    var dc = document.getElementById("help");
    var description = document.getElementById("description");
    var closebutton = document.getElementById("close");

    if (!dc || !description || !closebutton) {
      console.log('Cannot initialize mobile template. Dom does not contain correct elements.');
      return;
    }

    closebutton.onclick = function() {
      description.setAttribute("class", "header_collapse");
    };

    dc.onclick = function() {
      description.setAttribute("class", "header_expanded");
    };
  };

});