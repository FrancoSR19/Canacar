define([
  "samples/common/html5layertreecontrol/HTML5LayerTreeControl",
  "./MapLoader!",
  "dojo/request",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "samples/common/LayerConfigUtil",
  "samples/common/mouseLocation/MouseLocationComponent",
  "samples/common/URLUtil",
  "samples/common/navigation/ZoomControl"
], function(HTML5LayerTreeControl, Map, request, ReferenceProvider, ShapeFactory,
                                         LayerConfigUtil, MouseLocationComponent, URLUtil, ZoomControl) {

  function fitOnWorldOnLoad(map) {
    var viewSize = map.viewSize;
    var b;
    if (viewSize[0] === 0 || viewSize[1] === 0) {
      b = ShapeFactory.createBounds(ReferenceProvider.getReference("CRS:84"), [-180, 360, -90, 180]);
      map.resize();
      map.mapNavigator.fit({bounds: b, animate: true});
    }
  }

  var errorTags = {};
  var warningTags = {};

  return {

    fitOnWorldOnLoad: fitOnWorldOnLoad,

    makeMapAndComponents: function(mapOptions, options) {

      options = options || {};
      mapOptions = mapOptions || {};

      if (options.useUrlQueryParameters !== false) {
        options = URLUtil.getUrlQueryParameters(options);
        mapOptions = URLUtil.getUrlQueryParameters(mapOptions);
        delete options.webgl;
        delete mapOptions.webgl;
      }

      var warn = this.warn.bind(this);

      //map
      var map = new Map("map", mapOptions);

      fitOnWorldOnLoad(map);

      //zoom control
      if (options.includeZoomControl !== false) {
        var zoomControl = new ZoomControl(map, {touch : false});
      }

      //background layer
      if (options.includeBackground !== false) {
        LayerConfigUtil.createFusionBackgroundLayer()
            .then(function(layer) {
              return layer;
            }, function(error) {
              warn("LFImagery",
                  "LuciadFusion server at " + LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + " is not reachable.<br>A local image tileset layer is added to this sample instead.<br>In some cases, clearing your browser cache can help.");
              return LayerConfigUtil.createFallbackBackgroundLayer();
            }).then(function(layer) {
          map.layerTree.addChild(layer, "bottom");
        });
      }

      //elevation layer in 3D
      if (options.includeElevation !== false && map.reference.identifier === "EPSG:4978") {
        LayerConfigUtil.createFusionElevationLayer()
            .then(function(layer) {
              map.layerTree.addChild(layer);
            }, function(error) {
              warn("LFElevation",
                  "LuciadFusion server at " + LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "is not reachable.<br>No elevation layer is added to this sample.");
              return true;
            });
      }

      //mouse location control
      if (options.includeMouseLocation !== false) {
        var mouseLocationComponent = new MouseLocationComponent(map);
      }

      //layer control
      var control;
      if (options.includeLayerControl !== false) {
        control = new HTML5LayerTreeControl(map, {noLayerDelete: options.noLayerDelete});
        control.close();

        //on small screens, close the layer control by default
        setTimeout(function() {
          var screenwidth = document.body.offsetWidth;
          var screenheight = document.body.offsetHeight;
          if (screenheight < 300 || screenwidth < 600) {
            control.close();
          } else {
            control.open();
          }
        }, 10);
      }

      return {
        map: map,
        mouseLocationComponent: mouseLocationComponent,
        zoomControl: zoomControl,
        layerControl: control
      };
    },

    makeMap: function(mapOptions, options) {
      return this.makeMapAndComponents(mapOptions, options).map;
    },

    destroyMapAndComponents: function(map) {
      map.map.destroy();//destroy existing map.
      if (map.zoomControl) {
        map.zoomControl.destroy();
      }
      if (map.mouseLocationComponent) {
        map.mouseLocationComponent.destroy();
      }
      if (map.layerControl) {
        map.layerControl.destroyController();
      }
    },

    toast: function(className, message) {
      var toasts = document.getElementById("toasts");
      var toast = document.createElement("div");
      toast.className = "toast " + className;
      toast.innerHTML = message;
      toasts.appendChild(toast);

      setTimeout(function() {
        toast.style.opacity = 0;
        var hideToast = function() {
          toast.style.display = "none";
        };
        toast.addEventListener("transitionend", hideToast, false);
        toast.addEventListener("webkitTransitionEnd", hideToast, false);
      }, 12000);
    },

    info: function(message) {
      this.toast("", message);
      console.log(message);
    },

    error: function(tag, message, error) {
      if (tag && tag in errorTags) {
        //already logged errors for tag
        return;
      }
      errorTags[tag] = true;
      console.log(error || message);
      this.toast("error", message);
    },

    warn: function(tag, message, error) {
      if (tag && tag in warningTags) {
        //already logged warnings for tag
        return;
      }
      warningTags[tag] = true;
      console.log(error || message);
      this.toast("warning", message);
    },

    /**
     * Verifies if the URL is reachable, issuing a warning if not
     */
    probeURL: function(url) {
      var self = this;
      request(url).then(function(v) {
        // OK
      }, function(e) {
        self.warn(url, "Server at " + url + " not available.");
        console.log("Server at " + url + " not available.");
        console.log(e);
      });
    }

  };

});


