var profile = {
  basePath: "..",
  releaseDir: "release",
  action: "release",
  stripConsole: "all",
  optimize: "closure",
  layerOptimize: "closure",
  selectorEngine: "acme",
  async: 1,
  packages: [
    {
      name: "dojo",
      location: "./samples/lib/dojo",
      destLocation: "./samples/lib/dojo"
    },
    {
      name: "dijit",
      location: "./samples/lib/dijit",
      destLocation: "./samples/lib/dijit"
    },
    {
      name: "dojox",
      location: "./samples/lib/dojox",
      destLocation: "./samples/lib/dojox"
    },
    {
      name: "d3",
      location: "./samples/lib/d3",
      destLocation: "./samples/lib/d3"
    },
    {
      name: "luciad",
      location: "./luciad",
      destLocation: "./luciad"
    },
    {
      name: "samples",
      location: "./samples",
      destLocation: "./samples"
    }
  ],
  layers: {
    "dojo/dojo": {
      include: ["dojo/dojo"],
      boot: true
    },
    "samples/firstsample/main": {
      include: ["dijit/_base", "samples/firstsample/main"]
    },
    "samples/trajectories/main": {
      include: ["dijit/_base", "samples/trajectories/main"]
    },
    "samples/cop/main": {
      include: ["dijit/_base", "samples/cop/main", "luciad/view/Map", "luciad/view/WebGLMap",
                "samples/cop/spotreports/SpotReportInfoPanelFallback", "samples/cop/themes/BlueForcesPainterFallback",
                "samples/cop/themes/SpotDataPainterFallback"]
    }
  }
};