define([
  "luciad/reference/ReferenceProvider",
  "luciad/view/PaintRepresentation",
  "samples/common/LayerConfigUtil",
  "samples/template/sample",
  "samples/template/sampleReady!"
], function(ReferenceProvider, PaintRepresentation, LayerConfigUtil, sampleTemplate) {

  var map = sampleTemplate.makeMap();

  //#snippet references
  var reference = ReferenceProvider.getReference("EPSG:4326");
  //#endsnippet references


  var smallWorldLayer = LayerConfigUtil.addWorldLayer(map);

  // CITIES
  var citiesLayer = LayerConfigUtil.addCitiesLayer(map);
  citiesLayer.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);


  LayerConfigUtil.addLonLatGridLayer(map);


  //#snippet ballooncontentprovider
  function contentProvider(prefix, property) {
    return function(o) {
      return prefix + (o.properties[property] || "no property");
    };
  }
  //#endsnippet ballooncontentprovider

  // Create a balloon Content provider for the world layer
  smallWorldLayer.balloonContentProvider = contentProvider("COUNTRY: ", "name");

  //#snippet ballooncontentprovider
  // Create a balloon content provider for the cities layer, which creates HTML
  // for cities with more than 1.000.000 inhabitants and uses the "default"
  // content provider if not.
  var citiesDefaultProvider = contentProvider("CITY: ", "CITY");
  citiesLayer.balloonContentProvider = function(o) {
    var cityProps = o.properties, node;
    if (cityProps.TOT_POP > 1000000) {
      node = document.createElement("ul", {});
      for (var key in cityProps) {
        if (cityProps.hasOwnProperty(key) && key !== "uid") {
          var item = document.createElement("li");
          item.innerHTML = key + ": " + cityProps[key];
          node.appendChild(item);
        }
      }
      return node;
    } else {
      return citiesDefaultProvider(o);
    }
  };
  //#endsnippet ballooncontentprovider

  //fit on the cities layer
  var queryFinishedHandle = citiesLayer.workingSet.on("QueryFinished", function() {
    map.mapNavigator.fit({bounds: citiesLayer.bounds, animate: true});
    queryFinishedHandle.remove();
  });

});
