Running the sample build script build.[bat|sh] creates a minified build of the following samples:
- FirstSample
- Trajectories
- Cop

It uses the dojo build tools to package all Javascript dependencies into a single file.

You can optionally specify the build profile as the first argument.

> build.bat samples

If you also have the optional Symbology component, you need to build the cop sample with the cop-milsym profile.
This is because the cop sample relies on modules from the optional component that are not accessible with the core LuciadRIA release.

> build.bat cop-milsym



