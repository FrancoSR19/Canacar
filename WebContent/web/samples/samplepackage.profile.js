var profile = (function() {
  function filesToCopy(filename, mid) {
    return filename.indexOf("amdconfig") >= 0 ||
           filename.indexOf("samples/process") >= 0
  }

  function isDojo(filename) {
    return filename.indexOf("samples/lib/util") >= 0 ||
           filename.indexOf("samples/lib/dojo") >= 0 ||
           filename.indexOf("samples/lib/dijit") >= 0 ||
           filename.indexOf("samples/lib/dojox") >= 0;
  }

  function isLib(filename) {
    return filename.indexOf("samples/lib") >= 0;
  }

  function isAMDLib(filename) {
    return filename.indexOf("samples/lib/d3") >= 0;
  }

  return {
    resourceTags: {
      amd: function(filename, mid) {
        return (/\.js$/).test(filename) &&
               !filesToCopy(filename, mid) &&
               (!isLib(filename));
      },
      ignore: function(filename, mid) {
        return isDojo(filename) || isAMDLib(filename);
      },
      copyOnly: function(filename, mid) {
        return !(/\.js$/.test(filename)) ||
               filesToCopy(filename, mid) ||
               (isLib(filename) && !isAMDLib(filename) && !isDojo(filename));
      }
    }
  };
})();
