var amdconfig = {
  baseUrl: __AMD_CONFIG_BASE_URL__,
  packages: [
    {name: "dojo", location: "./samples/lib/dojo"},
    {name: "dojox", location: "./samples/lib/dojox"},
    {name: "dijit", location: "./samples/lib/dijit"},
    {name: "d3", location: "./samples/lib/d3"},
    {name: "luciad", location: "./luciad"},
    {name: "samples", location: "./samples"}
  ],
  cache: {}
};
if (require.config) {
  // RequireJS
  require.config(amdconfig);
} else {
  // Dojo
  require(amdconfig);
}
