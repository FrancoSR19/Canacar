define([
  "./LayerFactory",
  "./TimeChart",
  "./TypeaheadFilter",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/LightEffect",
  "luciad/view/WebGLMap",
  "samples/common/LayerConfigUtil",
  "samples/common/URLUtil",
  "dojo/domReady!"
], function(LayerFactory, TimeChart, TypeaheadFilter, ReferenceProvider, ShapeFactory, LightEffect, WebGLMap, LayerConfigUtil, URLUtil) {

  /**
   * This sample uses a few third-party libraries:
   * - D3: To draw the interactive time chart (see TimeChart.js)
   * - Typeahead + Bloodhound: for the interactive combobox filter
   * - jQuery: to wire the user interface
   */

  var map, timeChart, typeaheadFilter;

  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent);

  // start and end date of the data set in seconds since epoch
  var dataSetStartTime = 1421143477; // Tue Jan 13 11:04:37 CET 2015
  var dataSetEndTime = 1421230337; // Wed Jan 14 11:12:17 CET 2015
  /**
   * Note that the time values inside the data set are relative to the 'dataSetStartTime'. In other words the .json
   * files contains time values in [0, dataSetEndTime - dataSetStartTime] where a value of 0 means the point is at
   * time 'dataSetStartTime'.
   */

  var playing = false;
  var speedup = 1440;

  initMap();
  initTimeChart();
  initTypeaheadFilter();
  wireListeners();

  setTime(12 * 60);

  var trajectoriesLayer, airspacesLayer;

  function initMap() {
    map = new WebGLMap("map", URLUtil.getUrlQueryParameters());
    map.mapNavigator.fit(
        {
          bounds: ShapeFactory.createBounds(ReferenceProvider.getReference("EPSG:4326"), [-10, 30, 30, 30]),
          animate: true
        });
    trajectoriesLayer = LayerFactory.createTrajectoriesLayer("data/flightradar24_data" + (isMobile ? "_reduced" : "") + ".json", dataSetStartTime, dataSetEndTime);

    airspacesLayer = LayerFactory.createAirspacesLayer();
    airspacesLayer.visible = false;

    LayerConfigUtil.createFusionElevationLayer().then(function(layer) {
      map.layerTree.addChild(layer, "bottom");
    });
    LayerConfigUtil.createFusionBackgroundLayer().then(function(layer) {
      map.layerTree.addChild(layer, "bottom");
    });
    map.layerTree.addChild(airspacesLayer);
    map.layerTree.addChild(trajectoriesLayer);
  }

  function initTimeChart() {
    timeChart = new TimeChart(document.getElementById("timeChart"), new Date(dataSetStartTime * 1000), new Date(dataSetEndTime * 1000));
    var gmtPlus = (new Date().getTimezoneOffset() / -60);
    $('#timezone').text("GMT" + (gmtPlus > 0 ? "+" : "") + gmtPlus);
  }

  function initTypeaheadFilter() {
    typeaheadFilter = new TypeaheadFilter({
      filterNode: $('#filter'),
      textInputNode: $(".hasclear"),
      clearNode: $('.clearicon'),
      dataSets: [
        {
          source: "data/airports.json",
          name: 'airports',
          displayKey: 'name',
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'iata', 'icao'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          templates: {
            header: '<h4 class="typeahead-dropdown-header">Airports</h3>'
          }
        },
        {
          source: "data/airlines.json",
          name: 'airlines',
          displayKey: 'name',
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'icao', 'iata', 'callsign'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          templates: {
            header: '<h4 class="typeahead-dropdown-header">Airlines</h3>'
          }
        }
      ]
    });
  }

  function wireListeners() {
    //timechart
    timeChart.addInteractionListener(timeUpdated);
    timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
      speedup *= prevZoomLevel / currZoomLevel;
    });

    //typeahead filter listeners
    typeaheadFilter.on("SelectionChanged", function(obj, datum, dataSetName) {
      var trajectoriesPainter = trajectoriesLayer.painter;
      if (dataSetName === 'airports') {
        navigateTo({lon: datum.lon, lat: datum.lat});
        trajectoriesPainter.defaultColor = null;
        trajectoriesPainter.propertyColorExpressions = [
          {property: "origin", value: datum.icao, color: LayerFactory.DEFAULT_COLOR},
          {property: "destination", value: datum.icao, color: LayerFactory.OTHER_COLOR}
        ];
      } else if (dataSetName === 'airlines') {
        trajectoriesPainter.defaultColor = null;
        trajectoriesPainter.propertyColorExpressions = [
          {property: "airline", value: datum.icao, color: LayerFactory.DEFAULT_COLOR}
        ];
      }
    });

    typeaheadFilter.on("Clear", function() {
      var trajectoriesPainter = trajectoriesLayer.painter;
      trajectoriesPainter.propertyColorExpressions = [];
      trajectoriesPainter.defaultColor = LayerFactory.DEFAULT_COLOR;
    });

    //expose filterOn on window so it can be called from index.html
    window.filterOn = TypeaheadFilter.prototype.setFilter.bind(typeaheadFilter);

    //UI buttons
    $('#airspaces').click(function() {
      airspacesLayer.visible = !airspacesLayer.visible;
      $('#airspaces').toggleClass('active', airspacesLayer.visible);
    });

    $("#play").click(function() {
      lasttime = Date.now();
      play(!playing);
    });

    //expose navigateTo on window so it can be called from index.html
    window.navigateTo = function(moveTo) {
      var wgs84 = ReferenceProvider.getReference("EPSG:4326");
      var center = ShapeFactory.createPoint(wgs84, [moveTo.lon, moveTo.lat]);
      var radius = moveTo.radius || 5.0;
      var bounds = ShapeFactory.createBounds(wgs84, [center.x - radius, 2 * radius, center.y - radius, 2 * radius]);
      return map.mapNavigator.fit(bounds);
    };
  }

  function timeUpdated() {
    var currentTime = timeChart.getCurrentTime();
    var dataSetTimeValue = timeChart.getCurrentTime().getTime() / 1000 - dataSetStartTime; // as noted earlier the time values inside the file are relative to 'dataSetStartTime'
    var trajectoriesPainter = trajectoriesLayer.painter;
    map.effects.light = LightEffect.createSunLight({time : currentTime});
    trajectoriesPainter.timeWindow = [dataSetTimeValue - 1200, dataSetTimeValue];
    $('#timelabel').text(timeChart.getFormattedCurrentTime());
  }

  var lasttime = Date.now();
  function playStep() {
    if (playing) {
      var currTime = Date.now();
      var deltaTime = (currTime - lasttime);
      lasttime = currTime;
      var timeChartTime = timeChart.getCurrentTime().getTime();
      var newTime = timeChartTime + (deltaTime) * speedup;
      if (newTime >= timeChart.endDate.getTime()) {
        newTime = timeChart.startDate.getTime();
      }
      timeChart.setCurrentTime(new Date(newTime));
      window.requestAnimationFrame(playStep);
    }
  }

  function play(shouldPlay) {
    playing = shouldPlay;
    if (playing) {
      playStep();
    }
    var playBtn = $('#play');
    playBtn.toggleClass("active", playing);
    playBtn.find("> span").toggleClass("glyphicon-pause", playing);
    playBtn.find("> span").toggleClass("glyphicon-play", !playing);
  }

  function setTime(minutes) {
    timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + (1000 * 60 * minutes)));
    timeUpdated();
  }
});
