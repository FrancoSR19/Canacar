var profile = {
  action: 'release',
  optimize: false,
  layerOptimize: false,
  selectorEngine: 'acme',
  resourceTags: {
    amd: function(filename, mid) {
      return (/\.js$/).test(filename);
    }
  }
};
