define([
    "luciad/util/Evented"
], function(Evented) {

  function TypeaheadFilter(options) {
    Evented.apply(this);
    this._filterNode = options.filterNode;
    this._inputNode = options.textInputNode;
    this._clearNode = options.clearNode;
    this._selectionListeners = [];
    this._clearListeners = [];
    this._dataSets = options.dataSets;

    var self = this;

    //download and initialize dataSets
    var downloads = this._dataSets.map(function(dataSet) {
      return $.getJSON(dataSet.source);
    });

    $.when.apply($, downloads).then(function() {
      for (var i = 0; i < arguments.length; i++) {
        var downloadedData = arguments[i];
        var dataSet = self._dataSets[i];
        dataSet._bloodHound = new Bloodhound({
          datumTokenizer: dataSet.datumTokenizer,
          queryTokenizer: dataSet.queryTokenizer,
          local: downloadedData[0]
        });
        dataSet._bloodHound.initialize();
      }

      var typeaheadArgs = [
        {
          hint: true,
          highlight: true,
          minLength: 1,
          autoselect: true
        }
      ];

      self._dataSets.forEach(function(dataSet) {
        typeaheadArgs.push({
          name: dataSet.name,
          displayKey: dataSet.displayKey,
          source: dataSet._bloodHound.ttAdapter(),
          templates: dataSet.templates
        });
      });

      self._typeahead = self._filterNode.typeahead.apply(self._filterNode, typeaheadArgs);
    });

    //wire DOM listeners

    this._filterNode.on('typeahead:opened', function() {
      //move dropdown up the DOM so it doesn'get cut off by parent div(s)
      var dropdown = $('.twitter-typeahead > .tt-dropdown-menu');
      if (dropdown) {
        var body = $('#body');
        dropdown.appendTo(body);

        function calcDropdownPosAndSize() {
          var docPos = self._filterNode.offset();
          var newBottom = body.height() - docPos.top;
          dropdown.css('bottom', newBottom);
          dropdown.css('left', docPos.left);
          dropdown.css('top', 'auto');
          dropdown.css('width', self._filterNode.outerWidth());
        }

        $(window).on('resize', function() {
          calcDropdownPosAndSize();
        });

        calcDropdownPosAndSize();
      }
    });

    this._filterNode.bind('typeahead:selected', function(obj, datum, dataSetName) {
      self._notifySelectionListeners(obj, datum, dataSetName);
    });

    this._filterNode.bind('input', function() {
      var val = $('#filter').val();
      if (val === "") {
        self.clearFilter();
      }
    });

    this._inputNode.keyup(function() {
      self._clearNode.toggle(Boolean(self._inputNode.val()));
    });

    this._inputNode.click(function(e) {
      var offset = self._inputNode.offset();
      var relX = e.pageX - offset.left;
      if ($(this).val() && relX >= ($(this).width() - self._clearNode.width())) {
        self._clearNode.click();
      }
    });

    this._clearNode.hide(Boolean(this._inputNode.val()));

    this._clearNode.click(function() {
      self.clearFilter();
    });
  }

  TypeaheadFilter.prototype = Object.create(Evented.prototype);
  TypeaheadFilter.prototype.constructor = TypeaheadFilter;

  TypeaheadFilter.prototype.setFilter = function(term) {
    if (!term) {
      this.clearFilter();
    } else {
      this._filterNode
          .focus()
          .typeahead('val', '')
          .typeahead('val', term)
          .trigger(jQuery.Event("keydown", {which: 13}))
          .trigger(jQuery.Event("keyup", {which: 13}))
          .blur();
    }
  };

  TypeaheadFilter.prototype.clearFilter = function() {
    this._filterNode.typeahead('val', '');
    this._clearNode.hide();
    this._notifyClearListeners();
  };

  TypeaheadFilter.prototype._notifySelectionListeners = function(obj, datum, dataSetName) {
    this.emit("SelectionChanged", obj, datum, dataSetName);
  };

  TypeaheadFilter.prototype._notifyClearListeners = function() {
    this.emit("Clear");
  };

  return TypeaheadFilter;

})
;