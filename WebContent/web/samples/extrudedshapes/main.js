define([
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/store/UrlStore",
  "luciad/model/feature/Feature",
  "luciad/model/feature/FeatureModel",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "samples/template/sample",
  "samples/template/sampleReady!"
], function(GeoJsonCodec, UrlStore, Feature, FeatureModel, ReferenceProvider, ShapeFactory, FeatureLayer,
            FeaturePainter, templateSample) {
  var map = templateSample.makeMap();

  var fillColors = ["#ffffcc", "#c7e9b4", "#7fcdbb", "#41b6c4", "#2c7fb8", "#253494"];
  var lineColors = ["#ddddaa", "#a7c994", "#5fad9b", "#2196a4", "#0c5f98", "#051474"];
  var thematicHeights = [220, 180, 150, 100, 50, 0];

  function getColor(height, heightThresholds, colors) {
    var clampedValue = Math.min(Math.max(heightThresholds[heightThresholds.length - 1], height), heightThresholds[0]);
    for (var i = 0; i < heightThresholds.length; i++) {
      if (clampedValue > heightThresholds[i]) {
        return colors[i];
      }
    }
    return colors[heightThresholds.length - 1];
  }

  function createBuildingsModel() {
    var modelRef = ReferenceProvider.getHeightAboveTerrainReference("EPSG:4326");

    var delegateCodec = new GeoJsonCodec();
    // overwrite the reference with an above ground reference
    var decodeGeometryOriginal = delegateCodec.decodeGeometryObject;
    delegateCodec.decodeGeometryObject = function(geometry, reference) {
      return decodeGeometryOriginal.call(this, geometry, modelRef);
    };
    // create a codec that converts the shapes to extruded shapes
    var extrudedCodec = {
      _delegate: delegateCodec,
      decode: function(object) {
        var cursor = this._delegate.decode(object);
        return {
          hasNext: function() {
            return cursor.hasNext();
          },
          next: function() {
            var baseFeature = cursor.next();
            var height = baseFeature.properties.MAXHEIGHT - baseFeature.properties.MINHEIGHT;
            return new Feature(
                ShapeFactory.createExtrudedShape(baseFeature.shape.reference,
                    baseFeature.shape,
                    0.0,
                    height
                ),
                {
                  HEIGHT: height
                },
                baseFeature.id
            );
          }
        };
      }
    };

    var store = new UrlStore({target: "data/buildings.json", codec: extrudedCodec});
    return new FeatureModel(store, {
      reference: modelRef
    });
  }

  function createBuildingsLayer(buildingsModel) {
    var featurePainter = new FeaturePainter();
    featurePainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
      var fillColor = getColor(feature.properties.HEIGHT, thematicHeights, fillColors);
      var lineColor = getColor(feature.properties.HEIGHT, thematicHeights, lineColors);
      geocanvas.drawShape(shape, {
        fill: {
          color: (paintState.selected ? "rgba(43, 150, 33, 0.6)" : fillColor)
        },
        stroke: {
          color: (paintState.selected ? "rgba(140, 160, 10, 0.9)" : lineColor),
          width: 1.5
        }
      });
    };

    return new FeatureLayer(buildingsModel, {
      id: "Buildings",
      selectable: true,
      editable: false,
      painter: featurePainter,
      label: "San Francisco Buildings"
    });
  }

  //Create model, layer, and add the result to the layer tree
  var buildingsModel = createBuildingsModel();
  var buildingsLayer = createBuildingsLayer(buildingsModel);
  map.layerTree.addChild(buildingsLayer, "top");

  //Fit on exact camera angle
  if (map.reference.identifier === 'EPSG:4978') {
    map.mapNavigator.lookFrom(ShapeFactory.createPoint(ReferenceProvider.getReference("EPSG:4978"), [-2704965.418306253, -4261299.032250457, 3886789.332955691]), 55.78790525145676, -23.351176380543002, 0.0);
  } else {
    map.mapNavigator.fit({bounds: ShapeFactory.createBounds(buildingsModel.reference, [-122.40846495844885, 0.023754347205283466, 37.776591025710495, 0.0236079898130583]), animate: true});
  }
});