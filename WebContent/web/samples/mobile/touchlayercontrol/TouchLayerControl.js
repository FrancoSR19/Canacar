define([
  'dojo/aspect',
  'luciad/view/LayerTreeNodeType',
  "dojo/_base/declare",
  "dojo/_base/lang",
  "dojo/_base/window",
  "dojo/dom-construct",
  "dijit/registry",
  "dojox/mobile/TreeView",
  "dojox/mobile/Heading",
  "dojox/mobile/ListItem",
  "dojox/mobile/Switch",
  "dojox/mobile/RoundRectList",
  "dojox/mobile/ScrollableView",
  "dojox/mobile/viewRegistry"
], function(aspect, LayerTreeNodeType, declare, lang, dWindow, domConstruct, registry,
            TreeView, Heading, ListItem, Switch, RoundRectList, ScrollableView,
            viewRegistry) {

  function toDijitId(treenode) {
    var newViewId = "layer_";
    newViewId += treenode.id;
    return newViewId.replace('/', '_');
  }

  function addLayerNodeChildItem(treeControlInstance, list, treeNode) {

    var listItemArgs = {
      item: treeNode,
      label: treeNode.label,
      transition: "slide"
    };

    treeControlInstance._customizeListItem(listItemArgs);
    listItemArgs.moveTo = '#';
    listItemArgs.onClick = function() {
      treeControlInstance.handleClick(this);
    };

    var listitem = new ListItem(listItemArgs);
    list.addChild(listitem);
  }

  return declare("touchlayercontrol.TouchLayerControl", TreeView, {

    constructor: function() {

      var self = this;
      this.handleClick = function(item) {
        if (item.item.treeNodeType === LayerTreeNodeType.LAYER_GROUP) {
          self._goToLayerTreeNodeView(item, '_createScrollableViewForChildren');
        } else if (item.item.treeNodeType === LayerTreeNodeType.LAYER) {
          self._goToLayerTreeNodeView(item, '_createLayerSettingsView');
        }
      };

      var destroyAndRecreate = function(dijit, parentTreeNode, contentFactory) {
        var old = dijit.getChildren()[1];
        dijit.removeChild(1);
        registry.remove(old.id);
        old.destroy();
        var list = self[contentFactory](parentTreeNode);
        dijit.addChild(list);
      };

      var recreateListInView = function(parentNode) {
        var dijit, id;
        if (parentNode.id === self._rootId) {
          destroyAndRecreate(self, parentNode, "_createLayerListContents");
        } else {
          id = toDijitId(parentNode);
          dijit = registry.byId(id);
          if (dijit) {
            destroyAndRecreate(dijit, parentNode, "_createLayerGroupListContents");
          }
        }
      };

      this._handleNodeAdded = function(event) {
        var parentTreeNode = event.node.parent;
        recreateListInView(parentTreeNode);
      };

      this._handleNodeRemoved = function(event) {
        //always show current (by removing node, we might end up in invalid state).
        self.show();
        if (event.path[0]) {
          recreateListInView(event.path[0]);
        }
      };

      this._handleNodeMoved = function(event) {
        var parentTreeNode = event.node.parent;
        recreateListInView(parentTreeNode);
      };

    },

    postCreate: function() {

      this.inherited(arguments);

      var h1 = this.model.map.layerTree.on('NodeAdded', this._handleNodeAdded);
      var h2 = this.model.map.layerTree.on('NodeMoved', this._handleNodeMoved);
      var h3 = this.model.map.layerTree.on('NodeRemoved', this._handleNodeRemoved);

      aspect.after(this, 'destroy', function() {
        h1.remove();
        h2.remove();
        h3.remove();
      });

    },

    _createScrollableView: function(viewId, list, treeItem) {

      var heading = new Heading({
        label: treeItem.item.label,
        back: "Back",
        moveTo: viewRegistry.getEnclosingView(treeItem.domNode).id
      });

      var scrView = new ScrollableView({
        id: viewId
      }, domConstruct.create("div", null, dWindow.body()));
      scrView.addChild(heading);
      scrView.addChild(list);
      scrView.startup();

      return scrView;

    },

    _goToLayerTreeNodeView: function(treeItem, factoryMethodName) {

      var newViewId = toDijitId(treeItem.item);

      if (registry.byId(newViewId)) {  // view already exists, just transition to it
        registry.byNode(treeItem.domNode).transitionTo(newViewId);
        return;
      }

      this[factoryMethodName](treeItem, newViewId);

    },

    _addBoolPropertyToggle: function(list, layer, propertyName, labelName) {

      var toggleItem = new ListItem();
      toggleItem.placeAt(list.containerNode, "first");
      toggleItem.containerNode.innerHTML = labelName;
      toggleItem.startup();

      var visSwitch = new Switch({
        value: "on"
        //,
//        "class": "LayerControlSwitch"
      });
      visSwitch.placeAt(toggleItem.containerNode, "first");

      //work around for https://github.com/SitePen/dgrid/issues/958
      //this was a regression in dojo 1.10
      setTimeout(function() {
        visSwitch.startup();
      }, 0);

      visSwitch.set("value", layer[propertyName] ? "on" : "off");

      var handle = visSwitch.on('StateChanged', function(newState) {
        layer[propertyName] = (newState === "on");
      });

      aspect.after(visSwitch, 'destroy', function() {
        handle.remove();
      });

    },

    _addVisiblityToggle: function(list, layer) {
      this._addBoolPropertyToggle(list, layer, 'visible', 'Visible');
    },

    _createLayerSettingsView: function(treeItem, viewId) {

      var list = new RoundRectList();
      if (typeof treeItem.item.selectable === 'boolean') {
        this._addBoolPropertyToggle(list, treeItem.item, 'selectable', 'Selectable');
      }
      this._addVisiblityToggle(list, treeItem.item);

      var newView = this._createScrollableView(viewId, list, treeItem);
      registry.byNode(treeItem.domNode).transitionTo(newView.id);

    },

    _createListForLayerGroup: function(treeItem) {
      return this._createLayerGroupListContents(treeItem.item);
    },

    _createScrollableViewForChildren: function(treeItem, viewId) {
      var list = this._createListForLayerGroup(treeItem);
      var newView = this._createScrollableView(viewId, list, treeItem);
      registry.byNode(treeItem.domNode).transitionTo(newView.id);

    },

    _createLayerGroupListContents: function(treeNode) {
      var list = this._createLayerListContents(treeNode);
      this._addVisiblityToggle(list, treeNode);
      return list;
    },

    _createLayerListContents: function(treeNode) {
      var list = new RoundRectList();
      var children = treeNode.children;
      children.reverse();
      children.forEach(lang.partial(addLayerNodeChildItem, this, list));
      return list;
    },

    _handleRoot: function(root) {

      this._rootId = root.id;

      var list = this._createLayerListContents(root);
      var heading = new Heading({
        label: "Layers",
        back: "Back",
        moveTo: this.backRef
      });

      this.addChild(heading);
      this.addChild(list);

    },

    _customizeListItem: function(lia) {
      lia.label = lia.item.label;
    },

    _load: function() {
      this._handleRoot(this.model.map.layerTree);
    }


  });

});
