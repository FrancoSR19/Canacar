require([
  "dijit/registry",
  "dojo/_base/connect",
  "dojo/_base/kernel",
  "dojo/_base/window",
  "dojo/ready",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "samples/common/navigation/ZoomControl",
  "samples/common/LayerConfigUtil",
  "samples/common/layertreecontrol/LayerTreeModel",
  "samples/template/sample",
  "samples/mobile/touchlayercontrol/TouchLayerControl",
  "dojox/mobile",
  "dojox/mobile/deviceTheme",
  "dojox/mobile/ScrollableView",
  "dojox/mobile/Heading",
  "dojox/mobile/RoundRectList",
  "dojox/mobile/compat" ,
  "samples/template/sampleReady!"
], function(dRegistry, dConnect, dKernel, dWindow, dReady, ReferenceProvider, ShapeFactory,
            ZoomControl, LayerConfigUtil, LayerTreeModel, sampleTemplate,
            TouchLayerControl) {

  dReady(function() {
    mapNode = document.getElementById("map");
    var mapNode, map;

    function setupMap() {

      function createMap() {
        map = sampleTemplate.makeMap({}, { includeBackground: false, includeZoomControl: false });
        new ZoomControl(map, {touch : true});


        LayerConfigUtil.createDefaultLayerConfig(map, "../../resources");

        LayerConfigUtil.addLonLatGridLayer(map);

        //fit on the world
        var reference = ReferenceProvider.getReference("CRS:84");
        var worldBounds = ShapeFactory.createBounds(reference, [-180, 360, -90, 180]);
        map.mapNavigator.fit({bounds: worldBounds, animate: true});

        // apparently our map is blank after it is transitioned in by dojox.mobile,
        // so we refresh it again if the view in made active (again)
        dConnect.connect(dRegistry.byId("mapview"), "onBeforeTransitionIn", function() {
          map.resize();
        });
      }

      /**
       * fitToScreen adapts the map nodes height when the device's screen orientation changes
       */
      function fitToScreen() {
        dWindow.body().style.minHeight = "100%";
        mapNode.style.height = "100%";
        setTimeout(function() {
          if (map) {
            map.resize();
          }
          dKernel.global.scrollTo(0, 1);
        }, 0);
      }

      function setupLayerControl() {

        var mapView = dRegistry.byId("mapview");
        var layerButton = document.getElementById("layerbutton");

        var ltmodel = new LayerTreeModel(map);
        var layercontroller = new TouchLayerControl({
          model: ltmodel,
          backRef: "mapview"
        }, document.getElementById('layercontroller'));
        layercontroller.startup();

        layerButton.addEventListener("click", function() {
          mapView.performTransition("#layercontroller", 1, "slide");
        });

      }

      /**
       * Determine screen size and adapt the mapNode so it covers the entire screen
       */
      setTimeout(function() {
        fitToScreen();
        createMap();
        setupLayerControl();
      }, 200);

      /**
       * modify screensize if user tilts phone
       */
      dConnect.connect(dKernel.global, "onorientationchange", function() {
        setTimeout(fitToScreen, 50);
      });
    }

    setupMap();
  });
});
