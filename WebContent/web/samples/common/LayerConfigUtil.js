define([
  "dojo/_base/lang",
  "dojo/request",
  "dojo/request",
  "luciad/model/feature/Feature",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/model/store/UrlStore",
  "luciad/model/tileset/RasterDataType",
  "luciad/model/tileset/RasterSamplingMode",
  "luciad/model/tileset/BingMapsTileSetModel",
  "luciad/model/tileset/FusionTileSetModel",
  "luciad/model/tileset/UrlTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/format/LonLatPointFormat",
  "luciad/shape/ShapeFactory",
  "luciad/util/Promise",
  "luciad/view/controller/EditController",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/grid/GridLayer",
  "luciad/view/grid/LonLatGrid",
  "luciad/view/LayerType",
  "luciad/view/style/PinEndPosition",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/feature/transformation/ClusteringTransformer",
  "samples/common/IconFactory",
  "samples/common/ShapePainter"
], function(dLang, request, dRequest, Feature, FeatureModel, MemoryStore, UrlStore, RasterDataType, RasterSamplingMode,
            BingMapsTileSetModel, FusionTileSetModel, UrlTileSetModel, ReferenceProvider, LonLatPointFormat,
            ShapeFactory, Promise, EditController, FeatureLayer, FeaturePainter, GridLayer, LonLatGrid, LayerType,
            PinEndPosition, RasterTileSetLayer, ClusteringTransformer, IconFactory, ShapePainter) {

  // This LuciadFusion server is demonstration purposes only
  var PUBLIC_LUCIADFUSION_URL = window.location.protocol + "//sampleservices.luciad.com/";

  // Tests whether the public demo LuciadFusion server is reachable
  var publicLuciadFusionServerPromise = null;

  function isPublicLuciadFusionServerReachable() {
    return publicLuciadFusionServerPromise || (publicLuciadFusionServerPromise = dRequest(PUBLIC_LUCIADFUSION_URL +
                                                                                          "/lts?SERVICE=LTS&VERSION=1.0.0&REQUEST=getTile&COVERAGE_ID=4ceea49c-3e7c-4e2d-973d-c608fb2fb07e&LEVEL=0&ROW=0&COLUMN=0"));
  }

  //computes the resources directory
  var RESOURCE_DIRECTORY = (function() {
    var rp, url, matches, relsmpl, relpath;
    rp = new RegExp(/^.*\/samples\//);
    url = window.location.href;
    matches = rp.exec(url);
    if (matches) {
      relsmpl = url.replace(matches[0], "");
      matches = relsmpl.match(/\//g);
      relpath = matches.map(function(ignore) {
        return "..";
      }).join("/");
      return relpath + "/resources";
    } else {
      return '/samples/resources';
    }
  }());

  var CONTEXT_MENU_IDS = {
    EDIT_ID: "EDIT_ID",
    DELETE_ID: "DELETE_ID"
  };

  function formatResourceData(data) {

    //Inspect the response
    var resourceSet;
    var resource = data;
    if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' && data.resourceSets.length > 0) {
      resourceSet = data.resourceSets[0];
      if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' &&
          resourceSet.resources.length > 0) {
        resource = resourceSet.resources[0];
      }
    }

    resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;
    return resource;
  }

  function throwBingProxyError() {
    throw new Error("Something went wrong while contacting the Bing Proxy.  Did you configure it with a Bing Maps key?");
  }

  function createBingLayer(model) {
    return new RasterTileSetLayer(model, {layerType: LayerType.BASE});
  }

  function createFusionBackgroundLayer() {
    var tileSetReference = ReferenceProvider.getReference("EPSG:4326");

    var imageryParameters = {
      reference: tileSetReference,
      level0Columns: 4,
      level0Rows: 2,
      levelCount: 24,
      bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
      url: PUBLIC_LUCIADFUSION_URL + "/lts",
      coverageId: "4ceea49c-3e7c-4e2d-973d-c608fb2fb07e",
      tileWidth: 256,
      tileHeight: 256
    };

    return isPublicLuciadFusionServerReachable().then(function() {
      return new RasterTileSetLayer(new FusionTileSetModel(imageryParameters),
          {label: "Satellite imagery", layerType: LayerType.BASE});
    });
  }

  function createFusionElevationLayer() {
    var tileSetReference = ReferenceProvider.getReference("EPSG:4326");

    var elevationParameters = {
      reference: tileSetReference,
      level0Columns: 4,
      level0Rows: 2,
      levelCount: 24,
      bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
      url: PUBLIC_LUCIADFUSION_URL + "/lts",
      coverageId: "e8f28a35-0e8c-4210-b2e8-e5d4333824ec",
      tileWidth: 32,
      tileHeight: 32,
      dataType: RasterDataType.ELEVATION,
      samplingMode: RasterSamplingMode.POINT
    };

    return isPublicLuciadFusionServerReachable().then(function() {
      return new RasterTileSetLayer(new FusionTileSetModel(elevationParameters), {label: "Elevation"});
    });
  }

  function createFallbackBackgroundLayer() {
    var modelPromise = dRequest(RESOURCE_DIRECTORY + "/data/boston/meta.json", {
      handleAs: "json",
      contentType: "application/json"
    }).then(function(data) {
      data.reference = ReferenceProvider.getReference(data.reference);
      data.bounds = ShapeFactory.createBounds(data.reference, data.bounds);
      dLang.mixin(data, {
        baseURL: RESOURCE_DIRECTORY + "/data/boston/{z}_{x}_{y}.png"
      });
      return new UrlTileSetModel(data);
    });

    return modelPromise.then(function(model) {
      return new RasterTileSetLayer(model,
          {label: "Earth Image", layerType: LayerType.BASE}
      );
    });
  }

  function createBingModel(options) {
    options = options || {};
    var proxyUri = options.proxyUri || "/bingproxy/";
    var type = options.type || "Aerial";

    //return a promise for the bing layer.
    return request(proxyUri + type, {handleAs: "json"})
        .then(formatResourceData, throwBingProxyError)
        .then(function(resource) {
          return new BingMapsTileSetModel(resource);
        });
  }

  var util = {

    PUBLIC_LUCIADFUSION_URL: PUBLIC_LUCIADFUSION_URL,

    createBingLayer: function(options) {
      return createBingModel(options).then(createBingLayer);
    },

    createBackgroundLayer: createFallbackBackgroundLayer,
    createFusionBackgroundLayer: createFusionBackgroundLayer,
    createFallbackBackgroundLayer: createFallbackBackgroundLayer,
    createFusionElevationLayer: createFusionElevationLayer,

    addBackgroundRaster: function(map) {
      Promise
          .when(createFallbackBackgroundLayer())
          .then(function(tileSetLayer) {
            map.layerTree.addChild(
                tileSetLayer,
                "bottom"
            );
          });
    },

    createCitiesPainter: function() {
      //#snippet memoization
      var citiesPainter = new FeaturePainter();
      citiesPainter.cache = {};
      citiesPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
        var dimension = this.getIconSize(feature);
        var icon = this.cache[dimension];
        if (!icon || state.selected) {
          icon = IconFactory.circle({
            stroke: state.selected ? "rgba(0,0,255,1.0)" : "rgba(255,255,255,1.0)",
            fill: state.selected ? "rgba(255,0,255,0.7)" : "rgba(255,0,0,0.5)",
            strokeWidth: state.selected ? 3 : 2,
            width: dimension,
            height: dimension
          });
          if (!state.selected) {
            this.cache[dimension] = icon;
          }
        }
        //#endsnippet memoization
        geoCanvas.drawIcon(
            shape.focusPoint,
            {
              width: dimension + "px",
              height: dimension + "px",
              image: icon
            }
        );
      };

      var emptyStyle = {
        offset: 20,
        pin: {
          endPosition: PinEndPosition.MIDDLE,
          width: 1
        }
      };
      var htmlTemplate = '<div style="background-color: rgb(40,40,40); padding: 5px; border-radius: 10px; color: rgb(238,233,233)">$city, $state</div>';
      citiesPainter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
        var html;
        if (state.selected) {
          html = htmlTemplate.replace('$city', feature.properties.CITY);
          html = html.replace('$state', feature.properties.STATE);
          labelCanvas.drawLabel(html, shape, emptyStyle);
        }
      };

      citiesPainter.getIconSize = function(feature) {
        return Math.round(Math.sqrt(Math.round(feature.properties.TOT_POP / 10000) * 10000 / Math.PI) / 28);
      };
      return citiesPainter;
    },

    createWorldLayer: function() {
      // world layer
      var reference = ReferenceProvider.getReference("EPSG:4326");
      var smallWorldStore = new UrlStore({
        target: RESOURCE_DIRECTORY + "/data/world.json"
      });
      var smallWorldModel = new FeatureModel(smallWorldStore, {
        reference: reference
      });

      return new FeatureLayer(smallWorldModel, {
        label: "World Countries",
        layerType: LayerType.STATIC,
        selectable: true
      });
    },

    addWorldLayer: function(map) {
      var layer = util.createWorldLayer();
      map.layerTree.addChild(layer);
      return layer;
    },
    addCitiesLayer: function(map, painter) {

      /************************** CITIES Layer *****************************/
      var reference = ReferenceProvider.getReference("CRS:84");
      var citiesStore = new UrlStore({
        target: RESOURCE_DIRECTORY + "/data/cities.json"
      });

      var citiesModel = new FeatureModel(citiesStore, {
        reference: reference
      });

      var citiesPainter;
      if (painter) {
        citiesPainter = painter;
      } else {
        citiesPainter = util.createCitiesPainter();
      }

      var citiesLayer = new FeatureLayer(citiesModel, {
        label: "USA Cities",
        layerType: LayerType.STATIC,
        painter: citiesPainter,
        selectable: true
      });
      map.layerTree.addChild(citiesLayer);
      return citiesLayer;
    },
    //#snippet createLonLatGridLayer
    createLonLatGridLayer: function() {
      //Define scale ranges and create a grid
      var settings = [
        {scale: 40000.0E-9, deltaLon: 1 / 60, deltaLat: 1 / 60},
        {scale: 20000.0E-9, deltaLon: 1 / 30, deltaLat: 1 / 30},
        {scale: 10000.0E-9, deltaLon: 1 / 10, deltaLat: 1 / 10},
        {scale: 5000.0E-9, deltaLon: 1 / 2, deltaLat: 1 / 2},
        {scale: 1000.0E-9, deltaLon: 1, deltaLat: 1},
        {scale: 200.0E-9, deltaLon: 5, deltaLat: 5},
        {scale: 20.0E-9, deltaLon: 10, deltaLat: 10},
        {scale: 9.0E-9, deltaLon: 20, deltaLat: 20},
        {scale: 5.0E-9, deltaLon: 30, deltaLat: 30},
        {scale: 0, deltaLon: 45, deltaLat: 45}

      ];
      var grid = new LonLatGrid(settings);

      //Set the default styling for grid lines and labels
      grid.fallbackStyle = {
        labelFormat: new LonLatPointFormat({pattern: "lat(+DM),lon(+DM)"}),
        originLabelFormat: new LonLatPointFormat({pattern: "lat(+D),lon(+D)"}),
        originLineStyle: {
          color: "rgba(230, 20, 20, 0.6)",
          width: 2
        },
        lineStyle: {
          color: "rgba(210,210,210,0.6)",
          width: 1
        },
        originLabelStyle: {
          fill: "rgba(210,210,210,0.8)",
          halo: "rgba(230, 20, 20, 0.8)",
          haloWidth: 3,
          font: "12px sans-serif"
        },
        labelStyle: {
          fill: "rgb(220,220,220)",
          halo: "rgb(102,102,102)",
          haloWidth: 3,
          font: "12px sans-serif"
        }
      };

      //Use simplified labels when zoomed out a lot.
      var degreesOnlyFormat = new LonLatPointFormat({pattern: "lat(+D),lon(+D)"});
      grid.setStyle(grid.scales.indexOf(0), {
        labelFormat: degreesOnlyFormat
      });
      grid.setStyle(grid.scales.indexOf(5.0E-9), {
        labelFormat: degreesOnlyFormat
      });
      grid.setStyle(grid.scales.indexOf(9.0E-9), {
        labelFormat: degreesOnlyFormat
      });
      grid.setStyle(grid.scales.indexOf(20.0E-9), {
        labelFormat: degreesOnlyFormat
      });
      grid.setStyle(grid.scales.indexOf(200.0E-9), {
        labelFormat: degreesOnlyFormat
      });
      return new GridLayer(grid, {label: "Grid"});
    },
    //#endsnippet createLonLatGridLayer
    addLonLatGridLayer: function(map) {
      var grid = util.createLonLatGridLayer();
      map.layerTree.addChild(grid, "top");
      return grid;
    },

    CONTEXT_MENU_IDS: CONTEXT_MENU_IDS,

    createContextMenu: function(contextMenu, map, contextMenuInfo) {
      if (contextMenuInfo.objects.length > 0) {
        //we do not want to allow users to edit or delete clusters
        for (var i = 0; i < contextMenuInfo.objects.length; i++) {
          var feature = contextMenuInfo.objects[i];
          if (ClusteringTransformer.isCluster(feature)) {
            return;
          }
        }
        contextMenu.addItem({
          id: CONTEXT_MENU_IDS.EDIT_ID,
          label: "Edit",
          action: function() {
            map.controller = new EditController(contextMenuInfo.layer, contextMenuInfo.objects[0],
                {finishOnSingleClick: true});
          }
        });
        contextMenu.addItem({
          id: CONTEXT_MENU_IDS.DELETE_ID,
          label: "Delete",
          action: function() {
            map.selectObjects([]);
            contextMenuInfo.layer.workingSet.remove(contextMenuInfo.objects[0].id);
          }
        });
      }
    },

    createDefaultLayerConfig: function(map, resourcelocation) {

      util.addBackgroundRaster(map);
      util.addCitiesLayer(map);

      /********************** RANDOM OBJECTS Layer *************************/
          //#snippet createModel
      var pointGeoReference = ReferenceProvider.getReference("CRS:84");
      //#endsnippet createModel
      // create 100 random points in a memory store.
      var dataPoints = [];
      for (var i = 0; i < 100; i++) {
        dataPoints.push(new Feature(
            ShapeFactory.createPoint(pointGeoReference, [(Math.random() * 90) - 150, (Math.random() * 40) + 20]),
            {},
            i)
        );
      }
      var mem = new MemoryStore({data: dataPoints});

      //#snippet createModel
      var randomPointModel = new FeatureModel(mem, {
        reference: pointGeoReference
      });
      //#endsnippet createModel

      var memPainter = new ShapePainter({
            width: "8px",
            height: "8px"
          },

          {
            width: "16px",
            height: "16px"
          });

      map.layerTree.addChild(new FeatureLayer(
          randomPointModel,
          {
            label: "Random objects",
            layerType: LayerType.STATIC,
            painter: memPainter
          }
      ));
    }
  };

  return util;
});
