define([
  "luciad/model/codec/GeoJsonCodec",
  "luciad/error/ProgrammingError"
], function(GeoJsonCodec, ProgrammingError) {

  /**
   * A store that connects to a websocket, or long-polling enabled server. The connection is initiated at construction time, and terminated
   * when the 'destroy()' method is called.
   * @param options an object hash that has to contain a 'target' property which is the absolute websocket location,
   * and a 'delegateStore' property which will be updated when changes from the server are received.
   * An optional 'getId(feature)' function has to be provided if the features do not have an 'id' property.
   * @constructor
   */
  function WebSocketNotifier(options) {

    options = options || {};

    //break early
    if (!(typeof options.target === 'string')) {
      throw new Error("WebSocketNotifier::cannot create model when no target url is given.");
    }
    var target = options.target;
    var geoJsonCodec = new GeoJsonCodec();

    var self = this;

    this._delegateStore = options.delegateStore;

    function onMessage(event) {

      var feature;
      if ("NoData" === event.data) {
        return;
      }

      try {

        var featureCursor = geoJsonCodec.decode({content: event.data});
        while (featureCursor.hasNext()) {
          handleFeature(featureCursor.next());
        }
      } catch (error) {
        //when an element is deleted, the server does not send json which can be converted to a feature, but only an id
        //this cannot be decoded by the codec, so we handle this scenario separately
        try {
          var json = JSON.parse(event.data);
          if (json.id) {
            self._delegateStore.emit("StoreChanged", "remove", undefined, json.id);
          } else {
            console.log('WARN: unexpectedly could not parse the web _socket message. expecting id.', event.data);
          }
        } catch (e) {
          console.log('WARN: unexpectedly could not parse the web _socket message', event.data);
        }

      }

      function handleFeature(aFeature) {
        if (options.getId) {
          aFeature.id = options.getId(aFeature);
        }
        else if (!aFeature.id) {
          throw new ProgrammingError("WebSocketNotifier:: feature: " + aFeature +
                                     " does not have an 'id' property. Please provide a getId function " +
                                     "that returns a unique id for the features.");
        }
        self._delegateStore.emit("StoreChanged", "update", aFeature, aFeature.id);
      }
    }

    this._socket = new WebSocket(target);
    this._socket.onmessage = onMessage;

  }

  WebSocketNotifier.prototype.constructor = WebSocketNotifier;

  /**
   * @name destroy
   * @description Destroys the socket and ends the connection to the server.
   */
  WebSocketNotifier.prototype.destroy = function() {
    this._socket.onmessage = null;
    this._socket.close();
    this._delegateStore = null;
  }

  return WebSocketNotifier;
});
