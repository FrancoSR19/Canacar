define([], function(){

  /**
   * Throttle the given function to be called only once every 'wait' ms.
   *
   * This function ensure that the last call is always executed, but possibly with a delay. This is different from
   * most other throttle functions, that simply drop calls if they are too soon after the previous call.
   *
   * @param {Function} callback The function to throttle
   * @param {Number} wait The minimum amount of milliseconds between two successive calls to the throttled function.
   */
  return function(callback, wait){

    if(wait <= 0) {
      return callback;
    }

    var throttled = false;
    var scheduled = false;
    var latestThis;
    var latestArguments;
    return function(){
      latestThis = this;
      latestArguments = arguments;

      if (!throttled) {
        callback.apply(this, arguments);
        throttled = true;
      } else if(!scheduled){
        setTimeout(function() {
          callback.apply(latestThis, latestArguments);
          throttled = false;
          scheduled = false;
        }, wait);
        scheduled = true;
      }
    };
  };
});