define([
  "dojo/Evented",
  'luciad/view/LayerType',
  'luciad/view/LayerTreeNodeType',
  'luciad/view/LayerGroup',
  'luciad/view/feature/QueryStatus',
  'dojo/dom-class',
  'dojo/_base/lang',
  'luciad/view/PaintRepresentation',
  'dojo/dom-construct',
  "dojo/_base/declare",
  "dijit/form/CheckBox",
  "dijit/Tree",
  "dijit/Menu",
  "dijit/MenuItem",
  "dijit/CheckedMenuItem"
], function(Evented, LayerType, LayerTreeNodeType, LayerGroup, QueryStatus, domClass, lang, PaintRepresentation,
            domConstruct, dDeclare, Checkbox, Tree, Menu, MenuItem, CheckedMenuItem) {

  function fitOnBounds(map, layer) {
    if (layer.bounds) {
      //#snippet layerFit
      map.mapNavigator.fit({bounds: layer.bounds, animate: true});
      //#endsnippet layerFit
    }
  }

  function getClassNameForQueryStatus(queryStatus) {

    switch (queryStatus) {
    case QueryStatus.PENDING:
    case QueryStatus.QUERY_FINISHED:
      return "LayerNotBusy";
    case QueryStatus.QUERY_STARTED:
    case QueryStatus.QUERY_SUCCESS:
      return "LayerBusy";
    case QueryStatus.QUERY_ERROR:
      return "LayerError";
    }
  }

  var LayerContextMenu = dDeclare(Menu, {

    destroy: function() {
      this.lcdLayer = null;
      this.lcdMap = null;
      this.inherited(arguments);
    },

    _addVisibilityCheckbox: function() {

      var layerTreeNode = this.lcdLayer;
      if (this.lcdMap.layerTree === layerTreeNode) {
        return;
      }

      var visItem = new CheckedMenuItem({
        label: "Visible",
        checked: layerTreeNode.visible,
        onChange: function(value) {
          layerTreeNode.visible = value;
        }
      });

      layerTreeNode.on("VisibilityChanged", function(vis) {
        visItem.set("checked", vis, false);
      });

      this.addChild(visItem);
    },

    _addFitButton: function() {
      var layerTreeNode = this.lcdLayer;
      if (layerTreeNode.treeNodeType === LayerTreeNodeType.LAYER_GROUP) {
        return;
      }

      var map = this.lcdMap;
      var fitItem = new MenuItem({
        label: "Fit",
        iconClass: "FitAction",
        onClick: function() {
          fitOnBounds(map, layerTreeNode);
        }
      });
      this.addChild(fitItem);
    },

    _addLabeledCheckbox: function() {

      var layerTreeNode = this.lcdLayer;
      if (typeof layerTreeNode.isPaintRepresentationSupported !== 'function') {
        return;
      }

      if (!layerTreeNode.isPaintRepresentationSupported(PaintRepresentation.LABEL)) {
        return;
      }

      var labeledItem = new CheckedMenuItem({
        label: "Labeled",
        onChange: function(value) {
          layerTreeNode.setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
        },
        checked: layerTreeNode.isPaintRepresentationVisible(PaintRepresentation.LABEL)
      });

      layerTreeNode.on("PaintRepresentationVisibilityChanged", function(value, paintRepresentation) {
        if (paintRepresentation === PaintRepresentation.LABEL) {
          labeledItem.set("checked", value, false);
        }
      });

      this.addChild(labeledItem);
    },

    _addSelectableCheckbox: function() {

      var layerTreeNode = this.lcdLayer;
      if (typeof layerTreeNode.selectable !== 'boolean') {
        return;
      }

      var selItem = new CheckedMenuItem({
        label: "Selectable",
        onChange: function(value) {
          layerTreeNode.selectable = value;
        },
        checked: layerTreeNode.selectable
      });

      layerTreeNode.on("SelectableChanged", function(selectable) {
        selItem.set("checked", selectable, false);
      });

      this.addChild(selItem);

    },

    _addRemoveButton: function() {

      var layerTreeNode = this.lcdLayer;
      var map = this.lcdMap;

      if (map.layerTree === layerTreeNode) {
        return;
      }

      var removeButton = new MenuItem({
        label: "Remove",
        iconClass: "dijitEditorIcon dijitEditorIconDelete",
        onClick: function() {
          layerTreeNode.parent.removeChild(layerTreeNode);
        }
      });
      this.addChild(removeButton);
    },

    _addNewLayerGroupButton: function() {

      var layerTreeNode = this.lcdLayer;
      if (layerTreeNode.treeNodeType !== LayerTreeNodeType.LAYER_GROUP) {
        return;
      }

      var createLayerGroup = new MenuItem({
        label: "Add new group",
        onClick: function() {
          //#snippet newLayerGroup
          //#description  create a new layer group and add it to the parent.
          var lg = new LayerGroup({
            label: 'Group'
          });
          layerTreeNode.addChild(lg);
          //#endsnippet newLayerGroup
        }
      });
      this.addChild(createLayerGroup);

    },

    postCreate: function() {

      this.inherited(arguments);

      this._addFitButton();
      this._addRemoveButton();
      this._addVisibilityCheckbox();
      this._addSelectableCheckbox();
      this._addLabeledCheckbox();
      this._addNewLayerGroupButton();

    }
  });

  var LayerTreeNode = dDeclare(Tree._TreeNode, {

    _wireBusyEvents: function() {

      var self = this;

      function setBusyStatus() {
        domClass.remove(self._statusIcon, "LayerNotBusy LayerBusy LayerError");
        domClass.add(self._statusIcon, getClassNameForQueryStatus(self.item.workingSet.queryStatus));
        if (self.item.workingSet.queryStatus === QueryStatus.QUERY_ERROR) {
          self._statusIcon.title = "Error when retrieving data.";
        } else {
          self._statusIcon.title = "";
        }
      }

      this.item.workingSet.on('QueryStarted', setBusyStatus);
      this.item.workingSet.on('QuerySuccess', setBusyStatus);
      this.item.workingSet.on('QueryError', setBusyStatus);
      this.item.workingSet.on('QueryFinished', setBusyStatus);
      this.item.workingSet.on('QueryInterrupted', setBusyStatus);

      setBusyStatus();

    },

    _createLayerStatus: function() {
      if (!this.item.workingSet) {
        return;
      }

      this._statusIcon = document.createElement("div");
      domClass.add(this._statusIcon, "LayerStatus");
      domConstruct.place(this._statusIcon, this._cont, "first");
      this._wireBusyEvents();
    },

    _createCheckbox: function() {

      this._checkboxCont = document.createElement('div');
      domClass.add(this._checkboxCont, "VisibilityCheckbox");
      domConstruct.place(this._checkboxCont, this._cont, 'last');

      var self = this;
      this._checkbox = new Checkbox({
        checked: this.item.visible,
        onChange: function() {
          self.item.visible = self._checkbox.checked;
          self._checkbox.set("checked", self.item.visible, false);
          var tree = self.getParent().getParent();
          tree.emit("LayerSelected", self.item);
        }
      }, this._checkboxCont);

      //#snippet visibilitychanged
      this.item.on("VisibilityChanged", function(val) {
        self._checkbox.set("checked", val, false);
      });
      //#endsnippet visibilitychanged

    },

    _createMetaInfo: function() {

      if (this.item === this.lcdMap.layerTree) {
        return;
      }

      domClass.add(this.domNode, "LayerTreeNode");
      this._cont = document.createElement('span');
      domClass.add(this._cont, "RowContainer");
      domConstruct.place(this._cont, this.domNode, 'last');

      if (this.item.treeNodeType === LayerTreeNodeType.LAYER && this.item.type === LayerType.BASE) {
        domClass.add(this.domNode, "BaseLayer");
      }

      this._createLayerStatus();
      this._createCheckbox();
    },

    destroy: function() {
      domConstruct.destroy(this._checkbox);
      domConstruct.destroy(this._checkboxCont);
      domConstruct.destroy(this._statusIcon);
      this.contentNode.parentNode.removeEventListener("dblclick", this._doubleClickHandle);
      this._doubleClickHandle = null;
      this.inherited(arguments);
    },
    postCreate: function() {

      this._createMetaInfo();

      var contextMenu = new LayerContextMenu({
        targetNodeIds: [this.contentNode.parentNode],
        lcdLayer: this.item,
        lcdMap: this.lcdMap
      });

      contextMenu.startup();

      var self = this;
      self._doubleClickHandle = function(ev) {
        fitOnBounds(self.lcdMap, self.item);
      };
      this.contentNode.parentNode.addEventListener("dblclick", self._doubleClickHandle);

      this.inherited(arguments);
    }
  });

  return dDeclare([Tree, Evented], {

    onClick: function(item) {
      this.emit("LayerSelected", item);
    },

    _createTreeNode: function(args) {
      return new LayerTreeNode(lang.mixin({lcdMap: this.model.map}, args));
    }

  });

});