define([
  "dojo/_base/declare",
  "dojo/dom-construct",
  "./_FoldableLayerControlDisplay"
], function(dDeclare, dDomConstruct, _FoldableLayerControlDisplay) {

  return dDeclare(null, {

    _display: null,
    _map: null,
    _containerNode: null,

    constructor: function(map, containerNode) {
      this._map = map;
      this._containerNode = containerNode;
      this._initialize();
    },

    /**
     * must call when removing the layer controller from the page
     */
    destroyController: function() {
      this._display.destroyRecursive();
    },

    _initialize: function() {
      var domNode = dDomConstruct.create("div", {}, this._containerNode, "last");
      this._display = new _FoldableLayerControlDisplay({map: this._map}, domNode);

      this._display.startup();
    },

    toggle: function() {
      this._display.toggleControl();
    },

    open: function() {
      this._display.open();
    },

    close: function() {
      this._display.close();
    },

    on: function() {
      this._display.layerControl.on.apply(this._display.layerControl, arguments);
    }

  });

});
