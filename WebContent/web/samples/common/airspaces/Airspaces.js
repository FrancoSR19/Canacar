define([
  "./AirspaceGeoJsonCodec",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/Map",
  "luciad/view/style/PointLabelPosition",
  "samples/common/IconFactory"
], function(AirspaceGeoJsonCodec, FeatureModel, UrlStore, ShapeFactory, FeatureLayer,
            FeaturePainter, Map, PointLabelPosition, IconFactory) {

  var SELECTED_DEFAULT_STYLE = {
    fill: {
      color: "rgba(0,96,154,0.8)"
    },
    stroke: {
      color: "rgb(255,255,255)",
      width: 3
    }
  };

  var SELECTED_RESTRICTED_STYLE = {
    fill: {
      color: "rgba(109, 46, 34,0.3)"
    },
    stroke: {
      color: "rgb(255,255,255)",
      width: 3
    }
  };

  var DEFAULT_STYLE = {
    fill: {
      color: "rgba(0, 96, 154,0.3)"
    },
    stroke: {
      color: "rgb(10,71,147)"
    }
  };

  var RESTRICTED_STYLE = {
    fill: {
      color: "rgba(109, 46, 34, 0.3)"
    },
    stroke: {
      color: "rgb(109, 46, 34)",
      width: 2
    }
  };

  var LABEL_STYLE = {
    positions: PointLabelPosition.CENTER
  };

  function isRestricted(feature) {
    return (feature.properties.Type === "Military Training/Exercise Area" ||
            feature.properties.Type === "Restricted Area");
  }

  function minPoint(shape) {
    var min = shape.getShape(0).getPoint(0).y;
    for (var i = 0; i < shape.shapeCount; i += 1) {
      for (var j = 0; j < shape.getShape(i).pointCount; j += 1) {
        min = Math.min(min, shape.getShape(i).getPoint(j).y);
      }
    }
    return ShapeFactory.createPoint(shape.reference, [shape.focusPoint.x, min]);
  }

  return {

    createModel: function createModel(reference, file, fileHasIds) {
      var airspaceStore = new UrlStore({
        target: file,
        codec: new AirspaceGeoJsonCodec({
          generateIds: !fileHasIds,
          lowerLimitProperty: "Lower_Limit",
          upperLimitProperty: "Upper_Limit"
        })
      });
      return new FeatureModel(airspaceStore, {
        reference: reference
      });
    },
    createLayerForMap: function createLayerForMap(airspaceModel, airspacePainter, selectable) {
      return new FeatureLayer(airspaceModel, {
        painter: airspacePainter,
        selectable: selectable,
        label: 'Airspaces'
      });
    },
    createPainter: function createPainter() {

      var airspacePainter = new FeaturePainter();

      var restrictedIcon = IconFactory.circle({
        width: 20,
        height: 20,
        fill: "rgba(224,32,32,0.2)",
        stroke: "rgb(0,255,0)"
      });
      var unrestrictedIcon = IconFactory.circle({
        width: 16,
        height: 16,
        fill: "rgba(60,131,220,0.2)",
        stroke: "rgba(60,131,220,0.8)"
      });

      airspacePainter.paintBorderBody = function(geocanvas, feature, shape, layer, map, state) {

        if (state.border !== Map.Border.BOTTOM) {
          return;
        }

        var minp = minPoint(shape);
        var image = isRestricted(feature) ? restrictedIcon : unrestrictedIcon;
        geocanvas.drawIcon(minp, {
          image: image
        });
      };

      var smallLabelTemplate = '<div class="smallAirspaceLabel %colorclass%">' + '%Name%' + '</div>';
      airspacePainter.paintBorderLabel = function(labelCanvas, feature, shape, layer, map, state) {

        if (state.border !== Map.Border.BOTTOM) {
          return;
        }

        var label = smallLabelTemplate.replace("%Name%", feature.properties.Name);
        label = label.replace("%colorclass%", isRestricted(feature) ? "smallLabelRedColor" : "smallLabelBlueColor");

        var minp = minPoint(shape);
        labelCanvas.drawLabel(label, minp, {
          positions: PointLabelPosition.CENTER,
          offset: [0, 24]
        });
      };

      airspacePainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
        var style;
        if (isRestricted(feature)) {
          style = paintState.selected ? SELECTED_RESTRICTED_STYLE : RESTRICTED_STYLE;
        } else {
          style = paintState.selected ? SELECTED_DEFAULT_STYLE : DEFAULT_STYLE;
        }
        geocanvas.drawShape(shape, style);
      };

      //#snippet htmllabel
      var labelTemplate = '<div class="labelwrapper">' +
                          '<div class="airspaceLabel %colorclass%">' +
                          '<div class="theader">' +
                          '<div class="leftTick %colortickclass%"></div>' +
                          '<div class="rightTick %colortickclass%"></div>' +
                          '<div class="name">%Name%</div>' +
                          '</div>' +
                          '<div class="type">%Type%</div>' +
                          '<div class="Icao"><div style="display: inline-block">%Icao%&nbsp;&nbsp;</div>' +
                          '</div>' +
                          '</div>';
      //#endsnippet htmllabel

      airspacePainter.paintLabel = function(labelCanvas, feature, shape) {

        var i, shapeCount;

        var text = labelTemplate.replace('%Name%', feature.properties.Name);
        text = text.replace('%Type%', feature.properties.Type);
        text = text.replace('%Icao%', feature.properties.ICAO_Region);

        if (isRestricted(feature)) {
          text = text.replace('%colorclass%', 'redColorLabel');
          text = text.replace(/%colortickclass%/g, 'redColorTick');
          text = text.replace(/%backcol%/g, 'red');
        } else {
          text = text.replace('%colorclass%', 'blueColorLabel');
          text = text.replace(/%colortickclass%/g, 'blueColorTick');
          text = text.replace(/%backcol%/g, 'blue');
        }

        if (shape.shapeCount) {
          for (i = 0, shapeCount = shape.shapeCount; i < shapeCount; i++) {
            labelCanvas.drawLabel(text, shape, LABEL_STYLE);
          }
        } else {
          labelCanvas.drawLabel(text, shape, LABEL_STYLE);
        }
      };
      return airspacePainter;
    }

  };

});