define([
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/LayerType",
  "luciad/view/PaintRepresentation",
  "luciad/view/style/PathLabelPosition",
  "luciad/view/style/PathLabelRotation",
  "samples/common/LayerConfigUtil",
  "samples/common/ShapePainter",
  "samples/template/sample",
  "samples/template/sampleReady!"
], function(FeatureModel, UrlStore, ReferenceProvider, ShapeFactory, FeatureLayer, FeaturePainter, LayerType,
            PaintRepresentation, PathLabelPosition, PathLabelRotation, LayerConfigUtil, ShapePainter, sampleTemplate) {

  var RESOURCE_LOCATION = './';
  var modelReference = ReferenceProvider.getReference("EPSG:4326");

  var map = sampleTemplate.makeMap();

  function addStatesLayer(label, target) {
    var store = new UrlStore({
      target: target
    });

    var statesModel = new FeatureModel(store, {
      reference: modelReference
    });

    var statesLayer = new FeatureLayer(statesModel, {
      label: label,
      layerType: LayerType.STATIC,
      selectable: false,
      painter: new ShapePainter({
        fill: null,
        stroke: {
          width: 1,
          color: 'rgb(180,240,220)'
        }
      })
    });
    map.layerTree.addChild(statesLayer);
  }

  function addHighwaysLayer(label, target) {
    var modelReference = ReferenceProvider.getReference("EPSG:4269");
    var roadsStore = new UrlStore({
      target: target
    });
    var roadsModel = new FeatureModel(roadsStore, {
      reference: modelReference
    });

    var painter = new FeaturePainter();
    var selectedColor = 'rgb(255, 0, 0)';
    var haloWidthScale = 3;
    var labelStyle = {
      position: PathLabelPosition.CENTER,
      rotation: PathLabelRotation.FIXED_LINE_ANGLE
    };
    var startScale = 1 / 25000000; // show interstate routes
    var detailLevels = [1 / 3000000, // show US routes
                        1 / 1500000, // show state routes
                        1 / 750000,
                        1 / 350000,
                        1 / 30000];
    painter.getDetailLevelScales = function() {
      return detailLevels;
    };

    //#snippet omitfeatures
    painter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
      var name = feature.properties.NAME;

      if (state.level >= 0 && (name.indexOf('Interstate') >= 0)) {
        var interstateLineWidths = [1, 1.3, 1.9, 1.9, 3.2, 5.0];
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: interstateLineWidths[state.level] * haloWidthScale,
                color: state.selected ? selectedColor : 'rgb(177, 122, 70)'
              },
              zOrder: 4
            }
        );
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: interstateLineWidths[state.level],
                color: state.selected ? selectedColor : 'rgb(255, 175, 100)'
              },
              zOrder: 5
            }
        );
      }

      if (state.level >= 1 && (name.indexOf('US Route') >= 0)) {
        var usRouteLineWidths = [0, 1, 1.3, 1.9, 3.2, 5.0];
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: usRouteLineWidths[state.level] * haloWidthScale,
                color: state.selected ? selectedColor : 'rgb(177, 161, 84)'
              },
              zOrder: 2
            }
        );
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: usRouteLineWidths[state.level],
                color: state.selected ? selectedColor : 'rgb(255, 230, 120)'
              },
              zOrder: 3
            }
        );
      }

      if (state.level >= 2 && (name.indexOf('State Route') >= 0)) {
        var stateRouteLineWidths = [0, 0, 1, 1.3, 2.5, 5.0];
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: stateRouteLineWidths[state.level] * haloWidthScale,
                color: state.selected ? selectedColor : 'rgb(177, 177, 140)'
              },
              zOrder: 0
            }
        );
        geoCanvas.drawShape(shape,
            {
              stroke: {
                width: stateRouteLineWidths[state.level],
                color: state.selected ? selectedColor : 'rgb(255, 255, 200)'
              },
              zOrder: 1
            }
        );
      }
    };
    //#endsnippet omitfeatures

    var template = "<div class='$style Road'>$label</div>";
    painter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
      var name = feature.properties.NAME;

      if (state.level >= 0 && (name.indexOf('Interstate') >= 0)) {
        labelCanvas.drawLabelOnPath(
            template.replace('$label', name.replace('Interstate Route', 'I')).replace('$style', 'Interstate'),
            shape,
            labelStyle);
      }

      if (state.level >= 1 && (name.indexOf('US Route') >= 0)) {
        labelCanvas.drawLabelOnPath(
            template.replace('$label', name.replace('US Route', 'US')).replace('$style', 'USRoute'),
            shape,
            labelStyle);
      }

      if (state.level >= 2 && (name.indexOf('State Route') >= 0)) {
        labelCanvas.drawLabelOnPath(
            template.replace('$label', name.replace('State Route', 'SR')).replace('$style', 'StateRoute'),
            shape,
            labelStyle);
      }
    };

    //#snippet incrementalrend
    var roadsLayer = new FeatureLayer(roadsModel, {
      label: label,
      layerType: LayerType.STATIC,
      selectable: true,
      painter: painter,
      minScale: startScale
    });
    //#endsnippet incrementalrend

    roadsLayer.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
    map.layerTree.addChild(roadsLayer);
  }

  function addRoadsLayer(label, target, isMajorRoads) {
    var roadsStore = new UrlStore({
      target: target
    });
    var roadsModel = new FeatureModel(roadsStore, {
      reference: modelReference
    });

    var painter = new FeaturePainter();
    var regularColor = isMajorRoads ? 'rgb(240, 200, 80)' : 'rgb(214, 219, 231)';
    var regularHaloColor = isMajorRoads ? 'rgb(168, 140, 56)' : 'rgb(149, 153, 161)';
    var selectedColor = 'rgb(255, 0, 0)';
    var labelStyle = {
      position: PathLabelPosition.CENTER,
      rotation: PathLabelRotation.FIXED_LINE_ANGLE
    };
    painter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
      geoCanvas.drawShape(shape,
          {
            stroke: {
              width: 5,
              color: state.selected ? selectedColor : regularHaloColor
            },
            zOrder: 0
          }
      );
      geoCanvas.drawShape(shape,
          {
            stroke: {
              width: 3,
              color: state.selected ? selectedColor : regularColor
            },
            zOrder: 1
          }
      );
    };

    var template = "<div class='$style Road'>$label</div>".replace('$style', isMajorRoads ? 'Road' : 'Street');
    painter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
      labelCanvas.drawLabelOnPath(
          template.replace('$label', feature.properties.NAME),
          shape,
          labelStyle);
    };

    var layer = new FeatureLayer(roadsModel, {
      label: label,
      layerType: LayerType.STATIC,
      selectable: true,
      painter: painter,
      minScale: 1 / 30000
    });

    layer.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
    map.layerTree.addChild(layer);
  }

  addStatesLayer("States", RESOURCE_LOCATION + "../resources/data/states.json", false);
  addRoadsLayer("SF streets", RESOURCE_LOCATION + "./SanFrancisco_streets.json", false);
  addRoadsLayer("SF roads", RESOURCE_LOCATION + "./SanFrancisco_roads.json", true);
  addHighwaysLayer("USA highways", RESOURCE_LOCATION + "./Usa_highways.json");

  LayerConfigUtil.addLonLatGridLayer(map);

  // Fit on San Francisco
  if (map.reference.identifier === 'EPSG:4978') {
    map.camera.lookAt(ShapeFactory.createPoint(modelReference, [-122.4435, 37.7037]), 1165, 261.63, -37.25, 0.0);
  } else {
    map.mapNavigator.fit({bounds: ShapeFactory.createBounds(modelReference, [-122.49, 0.07, 37.68, 0.05]), animate: true});
  }
});
