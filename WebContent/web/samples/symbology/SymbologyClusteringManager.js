define([
  "luciad/symbology/military/MilitarySymbol",
  "luciad/view/feature/transformation/Classifier",
  "samples/common/IconFactory",
  "luciad/view/feature/transformation/ClusteringTransformer",
  "luciad/view/feature/BasicFeaturePainter",
  "luciad/model/feature/Feature",
  "samples/symbology/SymbologyUtil",
  "luciad/view/style/PointLabelPosition"
], function(MilitarySymbol, Classifier, IconFactory, ClusteringTransformer, BasicFeaturePainter, Feature,
            SymbologyUtil, PointLabelPosition) {

  function getClusterStyle(fillColor) {
    return {
      width: "60px",
      height: "60px",
      draped: false,
      image: IconFactory.circle({
        width: 60,
        height: 60,
        fill: fillColor,
        stroke: "#000000"
      })
    };
  }

  function getClusterLabel(feature) {
    return "<span style='color: black; background: rgba(255, 255, 255, 0.8); padding: 2px; font-size: medium; font-weight: bold; border-radius: 70px;'>" +
           ClusteringTransformer.clusteredFeatures(feature).length + "</span>";
  }

  /**
   * Provides clustering support for a given symbology.
   * @param symbology the symbology for which clustering support is provided
   **/

  function SymbologyClusteringManager(symbology) {
    this._symbology = symbology;
    this._classifier = this._createMilSymClassifier(this._symbology);
  }

  /**
   * Creates a clustering transformer that ensures the following rules for clustering:
   * - A cluster must contain at least 2 military symbols
   * - Units of different affiliation are not clustered together (As they have a different class, see _createMilSymClassifier).
   * - Units of different category are not clustered together (As they have a different class, see _createMilSymClassifier).
   * - Sea Surface, Sea Subsurface and MineWarfare units will not be clustered (As they all are classified as "SeaUnit",
   * see _createMilSymClassifier).
   */
  SymbologyClusteringManager.prototype.createMilSymClusteringTransformer = function() {
    return ClusteringTransformer.create({
      defaultParameters: {minimumPoints: 2},
      classifier: this._classifier,
      classParameters: [
        {
          classification: "SeaUnit",
          parameters: {noClustering: true}
        }
      ]
    });
  };

  /**
   * Creates a classifier that ensures the following classification rules:
   * - Units of different affiliation have a different classification.
   * - Units of different category have a different classification.
   * - Sea Surface, Sea Subsurface and MineWarfare units are classified as "SeaUnit".
   */
  SymbologyClusteringManager.prototype._createMilSymClassifier = function(symbology) {
    var classifier = new Classifier();
    classifier.getClassification = function(aObject) {
      var symbol = new MilitarySymbol(symbology, aObject.properties.code);
      var category = symbology.name === "APP_6C" ? symbol.code.substring(4, 6) : symbol.code.charAt(2);
      var affiliation = symbology.name === "APP_6C" ? symbol.standardIdentity2 : symbol.affiliation;

      // Sea Surface, Sea Subsurface and MineWarfare units are not clustered
      if (symbology.name === "APP_6C") {
        if (category === "30" || category === "35" || category === "36") {
          return "SeaUnit";
        }
      } else {
        if (category === "U" || category === "S") {
          return "SeaUnit";
        }
      }

      return category + "_" + affiliation;
    };
    return classifier;
  };

  /**
   * Creates a feature painter that applies the following rules:
   * 1. If the given feature is a Military Symbol: Apply default behavior by using the MilSymbPainter passed as parameter
   * 2. If the given feature is a cluster:
   *   2a. If it contains several instances of the same military symbol, the symbol's icon will be used to represent the cluster,
   *       only bigger and with a label indicating the number of elements in the cluster.
   *   2b. If it contains different symbols, a simple round icon with a label indicating the number of elements will be used.
   */
  SymbologyClusteringManager.prototype.createMilSymClusteringPainter = function(symbologyPainter) {
    var milSymClusteringPainter = new BasicFeaturePainter();
    var self = this;

    milSymClusteringPainter.paintLabel = function(labelcanvas, feature, shape, layer, map, paintState) {
      if (feature.properties.code) {
        symbologyPainter.paintLabel(labelcanvas, feature, shape, layer, map, paintState);
      } else {
        if (ClusteringTransformer.isCluster(feature)) {
          // Cluster painting
          labelcanvas.drawLabel(getClusterLabel(feature), shape.focusPoint, {
            group: "NON_DECLUTTERED",
            positions: PointLabelPosition.SOUTH,
            offset: 40
          });
          if (paintState.selected) {
            var elements = ClusteringTransformer.clusteredFeatures(feature);
            for (var i = 0; i < elements.length; i++) {
              var symbol = elements[i];
              symbologyPainter.paintLabel(labelcanvas, symbol, symbol.geometry, layer, map, paintState);
            }
          }
        }
      }
    };

    milSymClusteringPainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
      if (feature.properties.code) {
        symbologyPainter.paintBody(geocanvas, feature, shape, layer, map, paintState);
      } else {
        if (ClusteringTransformer.isCluster(feature) && ClusteringTransformer.clusteredFeatures(feature).length > 0) {
          //Cluster painting
          if (paintState.selected) {
            var elements = ClusteringTransformer.clusteredFeatures(feature);
            for (var i = 0; i < elements.length; i++) {
              var symbol = elements[i];
              symbologyPainter.paintBody(geocanvas, symbol, symbol.geometry, layer, map, paintState);
            }
          }

          var firstElementInCluster = ClusteringTransformer.clusteredFeatures(feature)[0];
          if (self._allIconsInClusterAreTheSame(feature)) {
            // Use same icon
            var clusterVisualization = new Feature(feature.geometry, {
              code: firstElementInCluster.properties.code,
              iconSize: 96
            }, feature.id);

            symbologyPainter.paintBody(geocanvas, clusterVisualization, clusterVisualization.geometry, layer, map,
                paintState);
          } else {
            if (self._classifier.getClassification(firstElementInCluster).indexOf("Hostile") !== -1) {
              geocanvas.drawIcon(shape.focusPoint, getClusterStyle(SymbologyUtil.HOSTILE_COLOR));
            } else if (self._classifier.getClassification(firstElementInCluster).indexOf("Friend") !==
                       1) {
              geocanvas.drawIcon(shape.focusPoint, getClusterStyle(SymbologyUtil.FRIEND_COLOR));
            } else {
              geocanvas.drawIcon(shape.focusPoint, getClusterStyle("rgba(165, 189, 41, 0.9)"));
            }
          }
        }
      }
    };

    return milSymClusteringPainter;
  };

  SymbologyClusteringManager.prototype._allIconsInClusterAreTheSame = function(cluster) {
    var clusteredElements = ClusteringTransformer.clusteredFeatures(cluster);
    var firstElementCode = clusteredElements[0].properties.code;
    for (var i = 1; i < clusteredElements.length; i++) {
      var feature = clusteredElements[i];
      if (feature.properties.code !== firstElementCode) {
        return false;
      }
    }
    return true;
  };

  return SymbologyClusteringManager;
});