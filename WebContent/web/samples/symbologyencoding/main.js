define([
  "samples/template/sample",
  "dojo/dom-construct",
  "dojo/request",
  "dijit/ProgressBar",
  "dijit/Dialog",
  "luciad/shape/ShapeFactory",
  "luciad/model/feature/FeatureModel",
  "luciad/reference/ReferenceProvider",
  "luciad/model/store/UrlStore",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainterUtil",
  "luciad/ogc/se/SEPainterFactory",
  "samples/template/sampleReady!"
], function(sampleTemplate, dConstruct, request,
            ProgressBar, Dialog, ShapeFactory, FeatureModel, ReferenceProvider, UrlStore,
            FeatureLayer, FeaturePainterUtil, SEPainterFactory) {


  /*
   * The sample loads a number of vector data geojson files, each associated with a SLD
   * Symbology encoding stylesheet. From each pair, a new FeatureLayer is created and added to
   * the map.
   *
   * The style sheets are also loaded into a live-editor. Each time a layer is selected in the
   * layer control, the corresponding style sheet is displayed in the editor.
   *
   * Editing the file only changes the file on the client. The changes are not saved back to
   * the server.
   *
   * The application is loaded with the bootstrap function.
   */

  var resourceLocation = "../resources/data/NaturalEarth/",
      layerMetaData,
      map,
      currentLayerMetaData,
      editorPane = document.getElementById("editorPane"),
      editorContainer = document.getElementById("editorContainer"),
      layerSelector = document.getElementById("selectedLayer");

  // A list of layer configurations. dataUrl points to the geojson file with the vector data.
  // seFile points to the style sheet.
  layerMetaData = [
    {
      name: "Countries",
      dataUrl: resourceLocation + "10m50m_combined/countries/countries_50m_with_usa_10m.json",
      seFile: resourceLocation + "10m50m_combined/countries/countries_50m_with_usa_10m.sld",
      selectable: false
    },
    {
      name: "Urban areas",
      dataUrl: resourceLocation + "10m/urban_areas/10m_urban_areas.json",
      seFile: resourceLocation + "10m/urban_areas/10m_urban_areas.sld",
      selectable: true
    },
    {
      name: "Parks",
      dataUrl: resourceLocation + "10m/parks_and_protected_areas/10m_us_parks_area.json",
      seFile: resourceLocation + "10m/parks_and_protected_areas/10m_us_parks_area.sld",
      selectable: true
    },
    {
      name: "Lakes",
      dataUrl: resourceLocation + "10m/lakes/ne_10m_lakes.json",
      seFile: resourceLocation + "10m/lakes/ne_10m_lakes.sld",
      selectable: true
    },
    {
      name: "Admin boundaries",
      dataUrl: resourceLocation + "10m/land_boundaries/ne_10m_admin_0_boundary_lines_land.json",
      seFile: resourceLocation + "10m/land_boundaries/ne_10m_admin_0_boundary_lines_land.sld",
      selectable: true
    },
    {
      name: "Railroads",
      dataUrl: resourceLocation + "10m/railroads/10m_railroads.json",
      seFile: resourceLocation + "10m/railroads/10m_railroads.sld",
      selectable: true
    },
    {
      name: "Roads",
      dataUrl: resourceLocation + "10m/roads/10m_roads.json",
      seFile: resourceLocation + "10m/roads/10m_roads.sld",
      selectable: true
    },
    {
      name: "Places",
      dataUrl: resourceLocation + "10m/populated_places/ne_10m_populated_places.json",
      seFile: resourceLocation + "10m/populated_places/ne_10m_populated_places.sld",
      selectable: true
    }
  ];

  function bootstrap() {
    setTimeout(function() {
      var editor = configureEditorPane();
      configureMap(editor);
      loadData();
    }, 50);
  }

  function configureMap(editor) {
    var ref = ReferenceProvider.getReference("EPSG:4326");
    map = sampleTemplate.makeMap({
      reference: ref
    }, {
      includeBackground: false,
      includeElevation: false,
      includeLayerControl: true,
      noLayerDelete: true
    });
    map.minMapScale = 2e-8;
    map.mapNavigator.fit({bounds: ShapeFactory.createBounds(ref, [-120, 5, 33, 3]), animate: true});

    // When the layer is selected, display the style sheet in the editor.
    layerSelector.addEventListener("change", function() {
      var layerName = layerSelector.value;
      var metaData = layerMetaData.filter(function(dataitem) {
        return dataitem.name === layerName;
      });
      currentLayerMetaData = metaData[0];
      editor.getSession().setValue(currentLayerMetaData.seSource);
      $("#btnApplyStyle").prop('disabled', !currentLayerMetaData);
    });

    var selectionItem = document.createElement("option");
    selectionItem.text = "Please select a layer";
    selectionItem.selected = true;
    selectionItem.disabled = true;
    layerSelector.appendChild(selectionItem);
    $("#btnApplyStyle").prop('disabled', true);
  }

  function configureEditorPane() {
    var editor, errorDialog;

    editor = ace.edit("editor");
    editor.setTheme("ace/theme/clouds");
    editor.getSession().setMode("ace/mode/xml");

    //wire up the custom ctrl-enter keyboard command
    editor.commands.addCommand({
      name: "update style",
      bindKey: {win: "Ctrl-Enter", mac: "Command-Enter"},
      exec: applyStyle
    });

    $("#btnApplyStyle").click(function() {
      applyStyle(editor);
    });

    errorDialog = new Dialog({
      title: "Symbology Encoding Error",
      style: "width: 350px;"
    });

    function applyStyle(aEditor) {

      if (!currentLayerMetaData) {
        //no active editing session.
        return;
      }

      var xmlString, layerToConfigure;
      //#snippet createSEPainter
      //#description Get the Symbology Encoding definition from a text editor and
      //#description use it to create a painter.
      xmlString = aEditor.getSession().getValue();
      currentLayerMetaData.seSource = xmlString;
      layerToConfigure = currentLayerMetaData.layer;

      SEPainterFactory.createPainterFromString(xmlString, {strict: true})
          .then(
          function(sePainter) {
            // add default selection styling to the generated painter
            // and configure it on the layer.
            layerToConfigure.painter = FeaturePainterUtil.addSelection(sePainter);
          },
          function(err) {
            errorDialog.set("content", err.message ? err.message : err);
            errorDialog.show();
          }
      );
      //#endsnippet createSEPainter
    }

    return editor;
  }

  /*
   Loads all the layer configurations.
   Display a progress bar until all layers are loaded.
   */
  function loadData() {
    var progressBar;
    var layerReadyCount = 0,
        index = 0,
        statusMessageBox = document.getElementById("statusMessage");

    initializeProgressBox();
    loadNextLayer();

    function initializeProgressBox() {
      progressBar = new ProgressBar({
        style: "width: 300px"
      });
      progressBar.placeAt(document.getElementById("progress"));
      progressBar.startup();
      document.getElementById("loadingMessage").innerHTML = "Loading Data";
      statusMessageBox.innerHTML = "Loading datasets...";
    }

    function loadNextLayer() {
      //load no more than 3 layers at the same time.
      if (index < layerMetaData.length && index - layerReadyCount < 3) {
        loadLayer(layerMetaData[index++]);
      } else if (index < layerMetaData.length) {
        setTimeout(loadNextLayer, 1000);
      }
    }

    function loadLayer(metaDataItem) {
      var store = new UrlStore({
            target: metaDataItem.dataUrl
          }),
          model = new FeatureModel(store, {
            reference: ReferenceProvider.getReference("CRS:84")
          });
      statusMessageBox.innerHTML = "Loading " + metaDataItem.name + "...";

      request(metaDataItem.seFile, {handleAs: "text"}).then(
          function(seSource) {
            //store the SE source as a string it the layer's metadata
            metaDataItem.seSource = seSource;
            SEPainterFactory.createPainterFromString(seSource, {strict: true})
                .then(configureLayerWithPainter, handleError);
          },
          handleError
      );

      function configureLayerWithPainter(sePainter) {
        var layer = new FeatureLayer(model, {
          label: metaDataItem.name,
          painter: FeaturePainterUtil.addSelection(sePainter),
          selectable: metaDataItem.selectable,
          visible: true
        });
        layer.workingSet.on("QueryFinished", function() {
          layerLoaded(metaDataItem);
        });
        // keep track of layer. We use the reference when toggling style-sheets based
        // on layer selection in the TOC.
        metaDataItem.layer = layer;
        map.layerTree.addChild(layer, "top");

        // populate layer selection menu
        var selectionItem = document.createElement("option");
        selectionItem.text = metaDataItem.name;
        layerSelector.insertBefore(selectionItem, layerSelector.childNodes[1]);
      }
    }

    function layerLoaded() {
      layerReadyCount += 1;

      //show percentage of loaded layers.
      progressBar.set("value", (layerReadyCount / layerMetaData.length) * 100);

      if (layerReadyCount === layerMetaData.length) {//all layers have been loaded.
        progressBar.destroy();
        statusMessageBox = null;
        dConstruct.destroy(document.getElementById("overlay"));
      } else {
        loadNextLayer();
      }
    }

    function handleError(err) {
      console.error("Error: " + err.message ? err.message : err);
      layerLoaded();
    }
  }

  bootstrap();
});
