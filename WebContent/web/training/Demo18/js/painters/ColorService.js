/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(
    [
      'lodash'
    ],
    function (_) {
      'use strict';
      
     var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.2)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.2)';
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.2)';
    var Verde = "rgba(50,230,50,0.2)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.2)", sVerdeClaro = "rgb( 134, 255, 132 )";
      /**
       * The SensorColorService controls the SensorColor on the timeline.
       * @name MapService
       * @class
       */
      var self = {};
      try{
        self.featureColor = {};
        }catch(e){
            console.log(e);
        }
        self.colorListSolid = [
            sBlanco, sRojo, sAzul, sAmarilloClaro, sNaranja, sNaranjaClaro, sVerde, sVerdeClaro, sMorado, sGris, sGrisOscuro
        ];
        self.colorListT = [
            Blanco, Rojo, Azul, AmarilloClaro, Naranja, NaranjaClaro, Verde, VerdeClaro, Morado, Gris, GrisOscuro
        ];
        self.colorIndex = 0;
        function generateColor() {
          return {
              selected: self.colorListSolid[self.colorIndex++ % self.colorListSolid.length],
              normal: self.colorListT[self.colorIndex++ % self.colorListT.length]
          };
        }
        return {
        

        getSensorColor: function (sensorId) {
          return self.featureColor[sensorId];
        },
        registerColor: function (id) {
            if(!self.featureColor[id]) {
                    self.featureColor[id] = generateColor();
            }
                
        }
      };

    }
);


