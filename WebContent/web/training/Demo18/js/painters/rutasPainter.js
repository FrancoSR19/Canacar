/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap',
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.2)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.2)';
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.2)';
    var Verde = "rgba(50,230,50,0.2)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.2)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var color = getColor(feature.id), dimension = 35, icon;
        
        console.log(feature.geometry.x + "\t id: "+ feature.id);
        if(feature.geometry.x || feature.geometry.x === 0) {
            if(state.selected) 
                icon = IconFactory.circle({stroke: sBlanco, fill: sRojo, width: dimension, height: dimension});
            else 
                icon = IconFactory.circle({stroke: sBlanco, fill: Azul, width: dimension, height: dimension});
            var regularStyle = {
                image: icon,
                width: dimension + "px",
                height: dimension + "px"
            };
            var id = Math.round(parseInt(feature.id));
            id = (parseInt(feature.id) - id) * 10;
            var name= (id ===1)? "A" : "B";
            //var id= (feature.id ===1 || feature.id === "1")? "A" : "B";
            var iconStyle = {
                url: "data/img/point"+name+".png",
                width: dimension + "px",
                height: dimension + "px",
                draped: false
            };
            geoCanvas.drawIcon(shape, iconStyle);
        }else {
            geoCanvas.drawShape(shape,{ 
                fill: {color: state.selected ? color.selected : color.normal},
                stroke: {
                    color: state.selected ? sBlanco : sAzul,
                    width: 7} 
            });
        }
    };
    function getColor(id) {
        switch(id) {
            case "0": return {normal: Azul, selected: sAzul};
            case "1": return {normal: Morado, selected: sMorado};
            case "2": return {normal: AmarilloClaro, selected: sAmarilloClaro};
            case "3": return {normal: Naranja, selected: sNaranja};
            case "5": return {normal: GrisOscuro, selected: sGrisOscuro};
            case "6": return {normal: NaranjaClaro, selected: sNaranjaClaro};
            case "7": return {normal: Rojo, selected: sRojo};
            case "8": return {normal: VerdeClaro, selected: sVerdeClaro};
            case "9": return {normal: GrisClaro, selected: sGrisClaro};
            case "10": return {normal: Verde, selected: sVerde};
            default: return {normal: Gris, selected: sGris};
        }
    }
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            for(var key in properties) {
                if(properties[key]) {
                if(i===0)
                    labelName = properties[key];
                i++;
                }
            }
        } else {
            labelName = feature.id;
        }
        
        label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 1) {     
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape.focusPoint, {});
        }
    };
 
    return layerPainter;
});



