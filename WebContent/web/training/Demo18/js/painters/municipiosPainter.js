/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap',
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.7)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.7)';
    var sAmarillo = "rgb( 244, 208, 63 )";
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.7)';
    var Verde = "rgba(50,230,50,0.7)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.7)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    var check = new Array(41);
    check = initArray(check);
    function initArray(array) {
        for(var i=0; i<array.length; i++) {
            array[i] = false;
        }
        return array;
    }
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var color;
        var nombres = [], ids = [];
        nombres = [
            "Alvaro Obregon","Azcapotzalco", "Benito Juarez", "Cuajimalpa de Morelos", "Cuauhtemoc", "Gustavo A. Madero", "Iztacalco", "Iztapalapa", "Miguel Hidalgo","Venustiano Carranza", 
            "Aculco", "Almoloya de Juarez" ,"Atizapan de Zaragoza","Atlacomulco","Coyotepec","Cuautitlan","Cuautitlan Izcalli","Ecatepec de Morelos","Ecatzingo","Huehuetoca","Huixquilucan","Jilotepec","Jocotitlan","Lerma","Naucalpan de Juarez","Nextlalpan","Nezahualcoyotl","Polotitlan","Soyaniquilpan de Juarez",
            "Tarandacuao","Jerecuaro","Apaseo el Alto","Apaseo el Grande",
            "Tepeji del Rio de Ocampo",
            "Contepec","Maravatio",
            "San Juan del Rio","Queretaro","Pedro Escobedo","El Marques","Corregidora"
        ];
        ids = [
            9,2,13,3,14,4,5,6,15,16,
            3,5,13,14,23,24,121,33,34,35,37,45,48,51,57,48,59,71,79,
            38,19,4,5,
            63,
            17,50,
            16,14,12,11,6
        ];
        if(!feature.properties.Insidentes) {
            var incidentes = getIncidentes();
            if(incidentes) {
                nombres = incidentes[0];
                ids = incidentes[1];
            var x = false;
            
            for(var i=0; i<nombres.length; i++) {
                if(feature.properties.NOMBRE === nombres[i]){
                    feature.properties.Insidentes = ids[i];
                    x = true;
                    check[i] = true;
                    //console.log("Encontrado "+nombres[i]);
                    break;
                }
            }
            if(!x) {
                feature.properties.Insidentes = 0;
            }
        }
        }
        var id = feature.properties.Insidentes;
        
        if(id===0 || !id) {
            color = getColor(0);
        }else {
        if(id < 10) {
            color = getColor(1);
        } else {
            if(id < 20) {
                color = getColor(2);
            } else {
                if(id < 45) {
                    color = getColor(3);
                } else {
                     color = getColor(4);
                }
            }
        }
        }
        
        if(feature.geometry.x) {
            var regularStyle = {
                url: "data/icons/icon2.png",
                width: "30px",
                height: "30px",
                draped: false
            };
            geoCanvas.drawIcon(shape.focusPoint, regularStyle);
        }else {
            geoCanvas.drawShape(shape,{ 
                fill: {color: state.selected ? color.selected : color.normal},
                stroke: {
                    color: state.selected ? sBlanco : color.selected,
                    width: 2} 
            });
        }
    };
    function getColor(id) {
        switch(id) {
            case 0: return {normal: VerdeClaro, selected: sVerdeClaro};
            case 1: return {normal: Verde, selected: sVerde};
            case 2: return {normal: AmarilloClaro, selected: sAmarilloClaro};
            case 3: return {normal: Naranja, selected: sNaranja};
            case 4: return {normal: Rojo, selected: sRojo};
            default: return {normal: Gris, selected: sGris};
        }
    }
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.NOMBRE) {
                labelName = properties.NOMBRE;
            }
            else {
            for(var key in properties) {
                if(i===0)
                    labelName = properties[key];
                i++;
            } 
            }
        } else {
            labelName = feature.id;
        }
        
        label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 1) {     
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape.focusPoint, {});
        }
    };
 
    return layerPainter;
});


