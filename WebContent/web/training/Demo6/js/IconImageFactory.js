define([], function() {
  var DEFAULT_WIDTH = 64,
      DEFAULT_HEIGHT = 64,
      DEFAULT_STROKESTYLE = "rgba(155,167,23,1)",
      DEFAULT_FILLSTYLE = "rgba(155,167,23,1)",
      DEFAULT_FADINGSTYLE = "rgba(155,167,23,0.1)",
      DEFAULT_STROKEWIDTH = 1;
  function makeContext(stroke, fill, strokeWidth, width, height) {
    var canvas = document.createElement("canvas"),
        context = canvas.getContext("2d");

    canvas.width = width || DEFAULT_WIDTH;
    canvas.height = height || DEFAULT_HEIGHT;

    context.strokeStyle = stroke || DEFAULT_STROKESTYLE;
    context.fillStyle = fill || DEFAULT_FILLSTYLE;
    context.lineWidth = strokeWidth || DEFAULT_STROKEWIDTH;

    return [canvas, context];
  }

  return {
    /**
     * Creates a circle icon that can be used as an image in {@link luciad.view.style.GeoCanvas#drawIcon}.
     * @example
     *  geoCanvas.drawIcon(
     *              shape,
     *              {
     *                width:"30px",
     *                height:"30px",
     *                image:IconFactory.circle({width:30, height:30, stroke:"#FF0000"})
     *              }
     *          );
     * @param options Icon options: width, height, stroke, fill, strokeWidth
     * @return Object
     */
    circle: function(options) {
      options = options || {};
      var ct = makeContext(options.stroke, options.fill, options.strokeWidth, options.width, options.height),
          canvas = ct[0],
          context = ct[1];

      var offset = (options.strokeWidth || DEFAULT_STROKEWIDTH),
          center = canvas.width / 2;

      var radius = (center - (offset));
      if(radius<=0){
        radius = 1;
      }
      context.arc(center, center, radius, 0, Math.PI * 2, false);
      context.fill();
      context.stroke();

      return canvas;
    },

    /**
     * Creates a circle icon will a radial gradient of 2 colors that can be used as an image
     * in {@link luciad.view.style.GeoCanvas#drawIcon}. The 2 colors are specified by the options.fill & option.fading
     * @example
     *  geoCanvas.drawIcon(
     *              shape,
     *              {
     *                width:"30px",
     *                height:"30px",
     *                image:IconFactory.gradientCircle({fill: "rgba(255, 255, 225, 1.0)",
     *                                                  fading: "rgba(225, 225, 225,0.2)",
     *                                                  width: 30,
     *                                                  height: 30})
     *              }
     *            );
     *
     *
     * @param options Icon options: width, height, fill, fading
     * @returns Object
     */
    gradientCircle: function(options) {
      var ct = makeContext(options.stroke, options.fill, options.strokeWidth, options.width, options.height),
          canvas = ct[0],
          context = ct[1];

      var offset = 1,
          center = canvas.width / 2;

      var radius = (center - (offset));
      if (radius <= 4) {
        radius = 4;
      }

      var smallestRadius = radius * 0.25;
      var smallerRadius = radius * 0.75;

      var grd = context.createRadialGradient(center, center, smallestRadius, center, center, smallerRadius);
      grd.addColorStop(0, options.fill || DEFAULT_FILLSTYLE);
      grd.addColorStop(1, options.fading || DEFAULT_FADINGSTYLE);

      context.fillStyle = grd;

      context.arc(center, center, radius, 0, Math.PI * 2, false);
      context.fill();

      return canvas;
    },
    /**
     * Creates a rectangle icon with Text inside that can be used as an image in {@link luciad.view.style.GeoCanvas#drawIcon}.
     * @example
     *  geoCanvas.drawIcon(
     *              shape,
     *              {
     *                width:"30px",
     *                height:"30px",
     *                image:IconFactory.rectangleText({width:30, height:30, stroke:"#FF0000", textColor: "white", textAlign: "left"})
     *              }
     *          );
     * @param options Icon options: width, height, stroke, fill, strokeWidth, textColor, textAlign
     * @return Object
     */
    rectangleText: function(text, options) {
      options = options || {};

      var ct = makeContext(options.stroke, options.fill, options.strokeWidth, options.width, options.height),
          canvas = ct[0],
          context = ct[1];

      var offset = (options.strokeWidth || DEFAULT_STROKEWIDTH) / 2.0;
      if (options.fill) {
        context.fillRect(offset, offset, canvas.width - 2*offset, canvas.height - 2*offset);
      }
      context.font = "14px Sans-Serif";
      context.fillStyle = options.textColor;
      context.textAlign = options.textAlign;
      var x = canvas.width/2; var y = canvas.height/2 + 16/2;
      if (options.textAlign=="left") x = 0;
      if (options.textAlign=="right") x = canvas.width - 0;
      context.strokeText(text, x, y);
      context.fillText(text, x, y);
      //  context.strokeRect(offset, offset, canvas.width - 2*offset, canvas.height - 2*offset);

      return canvas;
    },

    /**
     * Creates a rectangle icon that can be used as an image in {@link luciad.view.style.GeoCanvas#drawIcon}.
     * @example
     *  geoCanvas.drawIcon(
     *              shape,
     *              {
     *                width:"30px",
     *                height:"30px",
     *                image:IconFactory.rectangle({width:30, height:30, stroke:"#FF0000"})
     *              }
     *          );
     * @param options Icon options: width, height, stroke, fill, strokeWidth
     * @return Object
     */
    rectangle: function(options) {
      options = options || {};

      var ct = makeContext(options.stroke, options.fill, options.strokeWidth, options.width, options.height),
          canvas = ct[0],
          context = ct[1];

      var offset = (options.strokeWidth || DEFAULT_STROKEWIDTH) / 2.0;
      if (options.fill) {
        context.fillRect(offset, offset, canvas.width - 2*offset, canvas.height - 2*offset);
      }
      context.strokeRect(offset, offset, canvas.width - 2*offset, canvas.height - 2*offset);

      return canvas;
    },

    /**
     * Creates a text icon that can be used as an image in {@link luciad.view.style.GeoCanvas#drawIcon}. The text
     * is centered inside the icon.
     * @example
     *  geoCanvas.drawIcon(
     *              shape,
     *              {
     *                width:"30px",
     *                height:"30px",
     *                image:IconFactory.text("text", {width:30, height:30, fill:"#FF0000", font:"10pt Arial"})
     *              }
     *          );
     * @param text    the text string
     * @param options Icon options: width, height, fill
     * @return Object
     */
    text: function(text, options) {
      options = options || {};
      var ct = makeContext(options.stroke, options.fill, options.strokeWidth, options.width, options.height);
      var canvas = ct[0];
      var context = ct[1];

      context.textAlign = "center";
      context.textBaseline = "middle";
      context.font = options.font || "10pt Arial";
      context.fillText(text, options.width / 2, options.height / 2);

      return canvas;
    }
  };
});
