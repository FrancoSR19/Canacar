/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/view/feature/FeaturePainter",
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/shape/ShapeType",
    "luciad/view/style/PinEndPosition",
    
    
    "luciad/model/store/UrlStore",
    "luciad/model/codec/GeoJsonCodec",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "./IconImageFactory"
], function(FeaturePainter, ShapeFactory, ReferenceProvider, ShapeType, PinEndPosition,
        UrlStore, GeoJsonCodec, FeatureModel, FeatureLayer, IconImageFactory) {

    var data = Array();
    var j=0;
    function crearLayerBeneficiarios(reference,url, label, selectable)
    {
        var URL = new UrlStore({target: url});
        var modelo= getModel(reference, url);
        var painter;
        
        painter = new FeaturePainter();
        var detailLevels = [1 / 3000000, // show US routes
                        1 / 1500000, // show state routes
                        1 / 750000,
                        1 / 350000,
                        1 / 30000];
        painter.getDetailLevelScales = function() {
            return detailLevels;
        };
        painter.paintBody = getPaintBody();
        painter.paintLabel = getPaintLabel();
        painter.definePaintOnHover = getdefinePaintOnHover();
        painter.bigestShapeInShapeList = getbigestShapeInShapeList();
        //dLang.mixin(layerOptions, {painter: painter});
        return new FeatureLayer(modelo, {
            label: label,
            selectable: selectable,
            painter: painter}
        );
    }
    
    function getdefinePaintOnHover () {
        return function(geocanvas, feature, shape, paintState) {
            var color, colorSelected;
            if(feature.properties.Riesgo)  {
                color = "rgba(255,0,0, 0.7)";
                colorSelected = "rgba(255,0,0, 0.5)";
            }
            else {
                color = "rgba(214,137, 16, 0.3)";
                colorSelected = "rgba(43, 150, 33, 0.6)";
            }
            var style = {
                fill: {
                    color: (paintState.selected ? colorSelected : color)
                },
                stroke: {
                    color: (paintState.selected ? "rgb(255,255,255)" : "rgb(100,50,9)"),
                    width: 1
                }
            };
            
            if (typeof shape.pieChart == 'undefined' ) {
                var shape3d = ShapeFactory.createExtrudedShape(shape.reference, shape, 0, 500);
                shape.shape3d = shape3d;
                shape.pieChart = Array(2);
                shape.pieChartLabels = Array(2);
                var targetShape = this.bigestShapeInShapeList(shape);
                var radious = Math.min( targetShape.bounds.width, targetShape.bounds.height);
                var apoyo = feature.properties.ImporteApoyado;
                var ejercido = feature.properties.ImporteEjercido;
                var ratio = new Array(3);
                
                ratio[0] = 0;
                ratio[1] = ratio[0] + 360 * ejercido / apoyo;
                shape.pieChartLabels[0] = "Sobrante: $"+ (apoyo -ejercido);
                ratio[2] = ratio[0] + 360 * apoyo / apoyo;
                shape.pieChartLabels[1] = "Ejercido: $"+ apoyo;
                
                var i, max = feature.properties.maximumHeight, min = feature.properties.minimumHeight;
                for (i=0; i<2; ++i) {
                    var arcband = ShapeFactory.createArcBand( targetShape.reference, targetShape.focusPoint, radious * 0, radious * 15000, ratio[i]+180, ratio[i+1]-ratio[i] );
                    var extrudedSlice = ShapeFactory.createExtrudedShape(targetShape.reference, arcband, min + max, max*2 + min );
                    shape.pieChart[i] = extrudedSlice;
                }
                
                var stylePie = new Array(2);

                stylePie[0] = {fill: {color: "#2E2EFE"}, stroke: { color: "#08088A", width: 1}};
                stylePie[1] = {fill: {color: "#FE2E2E"}, stroke: { color: "#B40404", width: 1}};
                shape.paintOnHover = function (geocanvas, shape) {
                    var slice;
                    for  (slice=0; slice<2; ++slice) {
                        geocanvas.drawShape(shape.pieChart[slice], stylePie[slice]);
                    }
                    for(slice=0; slice<2; ++slice) {
                        var bounds = shape.pieChart[slice].bounds;
                        var z = bounds.z  + bounds.depth ;
                        var arcCoordinates = Array(6);
                        arcCoordinates[0] = bounds.x;  arcCoordinates[1] = bounds.width;
                        arcCoordinates[2] = bounds.y;      arcCoordinates[3] = bounds.height;
                        var arcBounds = ShapeFactory.createBounds(shape.pieChart[slice].reference, arcCoordinates);
                        var point = arcBounds.focusPoint;
                        var coordinatesIcon = Array(6);
                        coordinatesIcon[0] = point.x;  coordinatesIcon[1] = point.y; coordinatesIcon[2] = z * 1.05;
                        var pointIcon = ShapeFactory.createPoint(shape.pieChart[slice].reference, coordinatesIcon);
                        var icon = getText(shape.pieChartLabels[slice]);
                        geocanvas.drawIcon( pointIcon, {image: icon});
                    }
                    geocanvas.drawShape(shape.shape3d, style);
            };
            }
        };
    }
    
    function getText(text) {
            return IconImageFactory.rectangleText( text ,
                {width: 120, height: 25, fill: "rgba(224,32,32,0.0)", stroke: "black", textColor: "white", textAlign: "right"}); // Female
    }
    
    function getbigestShapeInShapeList() {
        return function(shape) {
        // Detects the biggest shape in a List of Shapes
        if (shape.type === ShapeType.SHAPE_LIST) {
            if (shape.shapeCount>1) {
                var i;
                var maxSize=0;
                var maxIndex=0;
                var targetShape;
                var size;
                for (i=0; i<shape.shapeCount; ++i)  {
                    targetShape = shape.geometries[i];
                    size = targetShape.bounds.width * targetShape.bounds.height;
                    if (size>maxSize) {
                        maxSize = size;
                        maxIndex = i;
                    }
                }
                return shape.geometries[maxIndex];
            } else
            return shape;
        } else return shape;
    };
    }
    
    function getModel(reference, url) {
      var store = new UrlStore({
        target: url,
        codec: new GeoJsonCodec({generateIDs: true})
      });
      return new FeatureModel(store,{
            reference: reference
        });
    }
    
    function getPaintLabel() {
        return function(labelCanvas, feature, shape, layer, map, paintState)
        {
            var label;
            if(paintState.level >=2) {
            if(feature.properties.name)
                label= "<span class='$Label' style='color: $color'>" + feature.properties.name + "</span>";
            else
                label = "<span class='$Label' style='color: $color'>" + feature.id + "</span>";
            if(paintState.selected)
                labelCanvas.drawLabel(label.replace("$color", "rgb(200,190,200").replace("$Label", "LabelSelected"),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", "rgb(252,255,255").replace("$Label", "Label"),shape.focusPoint, {});
            }
        
        };
    }
    
    function getPaintBody() {
        return function(geocanvas, feature, shape, layer, map, paintState)
        {
            data[j] = feature.properties;
            j++;
            var color, colorSelected;
            if(feature.properties.Riesgo)  {
                color = "rgba(255,0,0, 0.7)";
                colorSelected = "rgba(255,0,0, 0.5)";
            }
            else {
                color = "rgba(214,137, 16, 0.3)";
                colorSelected = "rgba(43, 150, 33, 0.7)";
            }
            var style = {
                fill: {
                    color: (paintState.selected ? colorSelected : color)
                },
                stroke: {
                    color: (paintState.selected ? "rgb(255,255,255)" : "rgb(100,50,9)"),
                    width: 1
                }
            };
            var max = feature.properties.maximumHeight + feature.properties.minimumHeight;
            var min = feature.properties.minimumHeight;
            var shape3d = ShapeFactory.createExtrudedShape(shape.reference, shape, 0, max);
            
            geocanvas.drawShape((paintState.selected ? shape3d : shape), style);
            
            this.definePaintOnHover(geocanvas, feature, shape, paintState);
        };
    }
    
    function getData(url, label, selectable)
    {
        var URL = new UrlStore({target: url});
        var modelo= getModel(reference, url);
        var painter;
        
        painter = new FeaturePainter();
        var detailLevels = [1 / 3000000, // show US routes
                        1 / 1500000, // show state routes
                        1 / 750000,
                        1 / 350000,
                        1 / 30000];
        painter.getDetailLevelScales = function() {
            return detailLevels;
        };
        painter.paintBody = getPaintBody();
    }
    
    return { 
        crearLayerBeneficiarios: crearLayerBeneficiarios,
        getData: getData}
});