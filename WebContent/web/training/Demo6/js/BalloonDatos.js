define([
    "./TablasEstadosMunicipios"
], function (TablasEstadosMunicipios) {
    
    function getBallon(feature) {
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<div class="form-inline">' +
                    //'<img src="data/img/perfilVacioB.png" WIDTH=100 HEIGHT=110>'+
                    '<h1 style="COLOR: #FFFFFF; BACKGROUND-COLOR: $COLOR">$NAME $ID</h1>'+
                '</div>' +
                '<table border=0 cellpadding=2 cellspacing=2  style="FONT-SIZE: 11px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;">' +
                
                '$DATOS' +
                '</table>' +
                '</div>';
        var etiquetaDatos = '<tr >' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                
                '</tr>';
        var ip = "../ejemploDatos/";  
        
        var properties = feature.properties;
        var balloon;
        var nombres = ["Productor:", "Id Productor: ","Municipio:","Nombre DDR:","Nombre Ejido:",
            "Importe Apoyado:", "Importe Ejercido:","Superficie Apoyada:","Nombre de Cultivo:",
            "Regimen:","Programa:", "Ejido", "En Riesgo:"];
        var name = properties.name;
        
        var Datos ='';
        var i;
        for(i = 0; i<nombres.length;i++) {
                Datos = Datos + etiquetaDatos.replace("$Propertie", nombres[i])
                        .replace("$Value", getDato(i, properties));
        }
        
        if(properties.Riesgo === true)
            balloon = etiqueta.replace("$COLOR", "#FF0000").replace('$NAME', name)
                .replace('$ID', properties.idProductor)
                .replace("$DATOS", Datos);
        else
            balloon = etiqueta.replace("$COLOR", "").replace('$NAME', name)
                .replace('$ID', properties.idProductor)
                .replace("$DATOS", Datos);
        //console.log(feature.properties.name);
        return balloon;
    }
    
    function getBallonEstados(feature) {
        if(feature.properties.name !== "Sinaloa")
            return null;
        
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<h1 style="COLOR: #FFFFFF; BACKGROUND-COLOR: $COLOR">Grafica de Impacto por Estado</h1>'+
                '<button id="botonBallon">Ver Grafica</button>'+
                //'<iframe style="width: 400px; height: 280px;" src="$URL" frameborder="0" height="500" scrolling="no" width="500">error</iframe>' +
                '<div id="graficaBallon">'+
                    '$Datos'+
                
                '</tr>';
                    
                    '</table>' +
                '</div>';
        var etiquetadatos = '<tr >' +
                '<td align="center">' +
                '<b>$Col1</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Col2</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Col3</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Col4</b>' +
                '</td >' +
                
                '</tr>';
        
        var datos = [
            ["Municipio","Superficie sembrada(Ha)",
                "Producción obtenida(Ton)","Valor de la producción(Miles de pesos)"],
            ["Badiraguato", 	10490,      15210.3,    52995.83],
            ["Concordia",  	16271.74,   89267.02,	191715.73],
            ["Cosalá",          9281.87,    34702.81,	57758.95],
            ["Culiacán",   	194704.53,  2391849.08,	7664931.19],
            ["Choix",      	23657,      33101.35,	203131.65],
            ["Elota",       	47311.53,   480326.09,	1838994.93],
            ["Mazatlán",   	27146.69,   227412.4,	518417.69],
            ["Rosario",    	30923.37,   311373.88,	876358.13],
            ["San Ignacio",	20576.96,   91836.46,	277479.37],
            ["Sinaloa",    	144096.85,  678897.16,	2566550.37]

        ];
        
        return {
            ballon: etiqueta,
            datos: datos
        };
    }
    
    
    function getDato(i, p) {
        switch(i) {
            case 0: return p.name;
            case 1: return p.idProductor;
            case 2: return p.Municipio;
            case 3: return p.NombreDDR;
            case 4: return p.NombreEjido;
            case 5: return p.ImporteApoyado;
            case 6: return p.ImporteEjercido;
            case 7: return p.SuperficieApoyada;
            case 8: return p.NombreCultivo;
            case 9: return p.Regimen;
            case 10: return p.Programa;
            case 11: return p.Ejido;
            case 12: 
                if(p.Riesgo === true)
                    return "En Riesgo";
                else
                    return "Sin Riesgo";
            default: return null;
        }
    }
    return {
        getBallon: getBallon,
        getBallonEstados: getBallonEstados
    };
});