//#snippet runrequire
require({
  baseUrl: "../../",

  packages: [
    {name: "dojo", location: "./dojo"},
    {name: "dojox", location: "./dojox"},
    {name: "dijit", location: "./dijit"},
    {name: "luciad", location: "./luciad"},
    
    {name: "demo", location: "./training/Demo6/js"},
    {name: "datos", location: "./samples/Demo6/data"},
    {name: "recursos", location: "./training/recursos"},
    
    {name: "template", location: "./samples/resources/template/js"},
    {name: "samplecommon", location: "./samples/common"},
    {name: "samples", location: "./samples"},
    {name: "navigation", location: "./samples/control/Navigation/js/navigation"},
    {name: "store", location: "./samples/cop/js/cop/store"},
    {name: "file", location: "./samples/model/File/js/file"},
    {name: "cop", location: "./samples/cop/js/cop"},
    {name: "d3", location: "./samples/lib/d3"},
  ]
//#endsnippet runrequire
  , cache: {}
//#snippet runrequirecont
}, ["demo"]);