define(
    [
      "dojo/_base/declare",
      "luciad/view/feature/FeaturePainter"
    ], function (declare, FeaturePainter) {
      var self = this;
      function SensorBucketPainter(SensorColorService) {
        self._labelStyle = {
          //positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        self.sensorColorService = SensorColorService;
      }

      SensorBucketPainter.prototype = new FeaturePainter();


      SensorBucketPainter.prototype.paintBody = function (geocanvas, feature, shape, layer, map, paintState) {
        //console.log("dibujando Capa");
          if (!paintState.selected) {
          var color = self.sensorColorService.getSensorColor(feature.properties.sensorId);
          if(!color || color.length<1){
            color = [120,160,210];
          }
          geocanvas.drawShape(shape, {
            stroke: {color: "rgb(13, 20, 28)", width: 3},
            fill: {color: "rgba("+color[0]+", "+color[1]+", "+color[2]+", 0.5)"}
          });
        } else {
          geocanvas.drawShape(shape, {
            stroke: {color: "rgb(13, 20, 28)", width: 3},
            fill: {color: "rgba(210, 160, 120, 0.8)"}
          });
        }
      };

      SensorBucketPainter.prototype.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
        if (paintState.selected) {
          var labelText = '<div class="name blueColorTick theader sensorLabel blueColorLabel">' + Math.floor(shape.bounds.height*100)/100 + '</div>';
          labelcanvas.drawLabel( labelText, shape, self._labelStyle );
        }
      };

      return SensorBucketPainter;
    }
);
