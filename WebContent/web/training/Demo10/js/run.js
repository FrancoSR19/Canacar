//#snippet runrequire
require({
  baseUrl: "../../",

  packages: [
    {name: "dojo", location: "./samples/lib/dojo"},
    {name: "dojox", location: "./samples/lib/dojox"},
    {name: "dijit", location: "./samples/lib/dijit"},
    {name: "luciad", location: "./luciad"},
    
    {name: "demo", location: "./training/Demo10/js"},
    {name: "datos", location: "./training/Demo10/data"},
    {name: "recursos", location: "./training/recursos"},
    
    {name: "template", location: "./samples/template"},
    {name: "samplecommon", location: "./samples/common"},
    {name: "samples", location: "./samples"},
    {name: "navigation", location: "./samples/control/Navigation/js/navigation"},
    {name: "file", location: "./samples/model/File/js/file"},
    {name: "cop", location: "./samples/cop/js/cop"}
  ],
  paths: {
      'lodash': './training/Demo10/bower_components/lodash/lodash',
      'angular': './training/Demo10/bower_components/angular/angular',
      'angular-mocks': './training/Demo10/bower_components/angular-mocks/angular-mocks',
      'text': './training/Demo10/bower_components/requirejs-text/text',
      'domReady': './training/Demo10/bower_components/requirejs-domready/domReady',
      'jquery': './training/Demo10/bower_components/jquery/jquery',
      'bootstrap': './training/Demo10/bower_components/bootstrap/bootstrap'
    },
    shim: {
      'angular': {
        exports: 'angular'
      },
      'jquery': {
        exports: 'jquery'
      },
      'bootstrap': {
        exports: 'bootstrap'
      }
    },

    waitSeconds: 15
//#endsnippet runrequire
  , cache: {}
//#snippet runrequirecont
}, ["demo"]);