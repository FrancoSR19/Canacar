/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    'luciad/view/LayerType',
    'luciad/view/feature/FeatureLayer',
  'luciad/reference/ReferenceProvider',
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/shape/ShapeFactory",
  "./time/TimeSlider",
  "./SensorPainter",
  "./SensorBucketPainter",
  "luciad/model/feature/Feature"
], function (LayerType, FeatureLayer, ReferenceProvider, FeatureModel, MemoryStore, 
        ShapeFactory, TimeSlider, SensorPainter, SensorBucketPainter, Feature) {
   
    function createTimeLineModel(sensor) {
      var data = sensor.properties.DATOS;
      var elements = [];
      for ( var i = 0; i < data.length; i++) {
        var timeInMillis = new Date(2017, 5, i + 1, 0, 0, 0, 0).getTime() / 1000;
        var firstTime = timeInMillis - 8 * 60 * 60;
        var secondTime = timeInMillis + 8 * 60 * 60;
        var p1 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, 0]);
        var p2 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, 0]);
        var p3 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, data[i]]);
        var p4 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, data[i]]);
        var shape = ShapeFactory.createPolygon(TimeSlider.REFERENCE, [p1, p2, p3, p4]);
        var properties = { sensorId:sensor.id };
        elements.push( new Feature(shape, properties, i));
      }
      return new FeatureModel(
          new MemoryStore( {data:elements}),
          {reference: TimeSlider.REFERENCE}
      );
    }
    
    return {
        createLayer: function(sensor, SensorColorService) {
            var model = createTimeLineModel( sensor );
            //console.log("creando Capa en TimeSlider");
            return new FeatureLayer(
                model, {
                    id: ""+sensor.id,
                    layerType: LayerType.STATIC,
                    selectable: true,
                    painter: new SensorBucketPainter(SensorColorService)
                });
        }
    };
});

