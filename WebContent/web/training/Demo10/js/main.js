var map, parcelas, precipitaciones, metaDatos, sensor, tablaSensores;
var municipiosChart, municipios, cultivosChart, displayTime;

define([
    "recursos/js/MapFactory",
    "recursos/js/BingMapsDataLoader",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "recursos/js/ElevationLayerService",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    
    //"samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    "./layerPainter",
    "./BalloonDatos",
    
    "luciad/view/controller/BasicCreateController",
    "luciad/view/controller/CreateController",
    "luciad/view/controller/EditController",
    "luciad/shape/ShapeType",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "recursos/js/CustomJsonCodec",
    
    "./painterEditable",
    "./TablasGraficas",
    "./time/TimeSlider",
    "./SensorsLayer",
    "./SensorColorService",
    
    "./TimeLineLayer",
    "luciad/view/LayerTreeVisitor",
    "luciad/view/LayerTreeNode",
    "luciad/shape/ShapeFactory",
    "./municipiosPainter",
    "template/sampleReady!"
], function (MapFactory, BingMapsDataLoader, GoogleMap, LayerFactory, ElevationLayerService, on, dom, query, 
        ReferenceProvider, LayerConfigUtil, layerPainter, BalloonDatos,
        BasicCreateController, CreateController, EditController, ShapeType, MemoryStore, FeatureModel, FeatureLayer, CustomJsonCodec,
        painterEditable, TablasGraficas, TimeSlider, SensorsLayer, SensorColorService,
        TimeLineLayer, LayerTreeVisitor, LayerTreeNode, ShapeFactory, municipiosPainter) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    var capaWMS, map15, map16;
    var selectedFeature = {geometry:null};
    var panelVisible =false, editable;
    var SelectedFeature = null;
    Start();
    
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
    */
    function Start() {
        MostrarOcultar(null, "panelTimeSlider");
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false, layerControlOpen:false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: true, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, newLayerControl: true, layerControlOpen:false, includeBingLayer: "Aerial"});
        } 
        CrearCapas();
        CapasBasicas();
    }
    
    function fit(bounds, coordenadas) {
        if(bounds === null || bounds === undefined) {
            map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, coordenadas), animate: true
            });
        }
        else map.mapNavigator.fit({ bounds: bounds, animate: true });
    }
    
    $("#opacity").on("change input", function() {
        var alpha = parseFloat($("#opacity").val());
        map16.rasterStyle = {alpha: alpha};
    });
    
    /*
     * dentro de esta funcion esta la parte de seleccion. despues de crear un poligono (linea ) y al seleccionarlo se crea el control de edicion para poder cambiar el poligono.
     */
    map.on("SelectionChanged", function (event) {
        selectedFeature = null;
        if (event.selectionChanges.length > 0 && event.selectionChanges[0].selected.length > 0 && event.selectionChanges[0].layer.id === 'editable') {
            selectedFeature = event.selectionChanges[0].selected[0];
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            var control = new EditController(event.selectionChanges[0].layer, selectedFeature, {finishOnSingleClick: true});
            control.onDraw = function (geoCanvas) {
                geoCanvas.drawShape(selectedFeature.shape,{ 
                    fill: {color: "rgba(70, 100, 230, 0.5)"},
                    stroke: {
                        color: 'rgb(255, 255, 255)',
                        width: 1} 
                });
                SelectedFeature=selectedFeature;
                filtroGeometria(selectedFeature.geometry);
                var period = periodFilterNode.selectedOptions[0].value;
                var cultivo = cultivoFilterNode.selectedOptions[0].value;
                /*
                 * TablasGraficas.actualizarGrafica: se encarga de actualizar una grafica existente, el primer parametro es una id para identificar la grafica.
                 */
                try{
                    TablasGraficas.actualizarGraficaUbicacionDatos(0, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Tipo de Cultivo", 2);
                    TablasGraficas.actualizarGraficaUbicacionDatos(1, metaDatos,SelectedFeature.geometry, period, cultivo, "piechart", "Municipios", 9);
                    TablasGraficas.actualizarGraficaLabelValorUbicacionDatos(2, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Area Apoyada", 2, 15);
                    /*
                TablasGraficas.actualizarGrafica(0, metaDatos, selectedFeature.geometry, "columnchart", "Tipo de Cultivo", 2);
                TablasGraficas.actualizarGrafica(1, metaDatos, selectedFeature.geometry, "piechart", "Municipios", 9);
                TablasGraficas.actualizarGraficaLabelValor(2, metaDatos, selectedFeature.geometry, "columnchart", "Area Apoyada", 2, 15);
                    */
                }catch(e) {console.log(e);}
            };
            map.controller = control;
            
        }
        handleSelectionEvent(event);
    });
    
    function CrearCapas() {
        municipios = LayerFactory.createFeatureLayer(referenceC, {label: "Municipios", selectable: false, editable:false, painter: new municipiosPainter()}, "data/municipiosChihuahua.json");
        map.layerTree.addChild(municipios);
        /*
         *parcelasPoint: son los puntos georeferenciados de parcelas, la informacion la saque del archivo de excel con el nombre  Chihuahua_predios_aserca.xls.
         *@type FeatureLayer
         */
        parcelas = LayerFactory.createFeatureLayer(referenceC, {label: "Parcelas", selectable:true, painter: new layerPainter(), editable: false}, "data/cultivos.json");
        map.layerTree.addChild(parcelas);
        parcelas.balloonContentProvider = function (feature) {
            return BalloonDatos.getBallon(feature);
        };
        
        
        
        sensor = SensorsLayer.createLayer(referenceC, map, SensorColorService);
        map.layerTree.addChild(sensor);
        /*
         * map16: Raster con la imagen del 2016 de las parcelas de chihuahua.
         * @type RasterTileSetLayer|RasterTileSetLayer
         */
        map16 = LayerFactory.createLayerImage(referenceC, "data/2016/2016.png", "Imagen 2016", {
            heigth: 2799, width: 2142,
            x:-109.8847226355625, 
            y: 27.768424650028937, 
            x2: -103.21008025581511, 
            y2: 32.87793299508983
        });
        map.layerTree.addChild(map16, "bottom");
        var alpha = parseFloat($("#opacity").val());
        map16.rasterStyle = {alpha: alpha};
        /*
         * map15: Raster con la imagen del 2015 de las parcelas de chihuahua.
         * @type RasterTileSetLayer|RasterTileSetLayer
         */
        map15 = LayerFactory.createLayerImage(referenceC, "data/2015/2015-2.png", "Imagen 2015", {
            heigth: 2799, width: 2142,
            x:-109.8847226355625, 
            y: 27.768424650028937, 
            x2: -103.21008025581511, 
            y2: 32.87793299508983
        });
        map.layerTree.addChild(map15, "bottom");
        
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/cultivos.json"
        }).done(function(data) {
                console.log("archivo leido");
                if(data.features) {
                    metaDatos = TablasGraficas.crearArreglo(data.features);
                    cultivosChart = TablasGraficas.crearGraficaIdTexto("graficaCultivos", metaDatos, "columnchart", "Tipos de Cultivos", 2);
                    municipiosChart = TablasGraficas.crearGraficaIdTexto("graficaMunicipio", metaDatos, "piechart", "Municipios", 9);
                    areaChart = TablasGraficas.crearGraficaLabelValor("graficaArea", metaDatos, "columnchart", "Area Apoyada", 2, 15);
                }
            }
        ).fail(function(errMsg) {
                console.log("no se leyo el archivo\n" + errMsg);
                return null;
        });
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/sensors.json"
        }).done(function(data) {
            console.log("archivo sensores leido");
            $.ajax({
                type:'Get',
                dataType: "json",
                url: "data/sensores16.json"
            }).done(function(data2) {
                console.log("archivo sensores16 leido");
                tablaSensores = TablasGraficas.crearArregloSensores(data.features, data2.features);
            }
            ).fail(function(errMsg) {
                console.log("no se leyo el archivo\n" + errMsg);
                return null;
            });
        }).fail(function(errMsg) {
                console.log("no se leyo el archivo\n" + errMsg);
                return null;
        });
        /*
         * editable: capa creada para la creacion de los poligonos que se usan para la seleccion de objetos.
         * @type FeatureLayer
         */
        editable = crearMemoryStoreLayer(referenceC, {label: "Seleccion", id:"editable", selectable: true, editable: true, painter: new painterEditable()});
        map.layerTree.addChild(editable, "top");
    }
    
    function CapasBasicas() {
        LayerConfigUtil.addLonLatGridLayer(map);
        
        var mexicoLayer = LayerFactory.createKmlLayer('../recursos/estados.kml', 'Mexico', false);
        map.layerTree.addChild(mexicoLayer);
        
        //var elevationLayer = ElevationLayerService.createRealElevationLayer();
        //map.layerTree.addChild(elevationLayer, "bottom");
    }
    
    /*
     * 
     * @param {Object} reference
     * @param {Object} options
     * @returns {mainL#36.FeatureLayer}
     */
    function crearMemoryStoreLayer(reference, options) {
        var codec = new CustomJsonCodec();
        var store = new MemoryStore({codec: codec});
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    
    /*
     * 
     * @param {type} map
     * @returns {mainL#36.fetchSelectedObject.mainAnonym$13}
     */
    function fetchSelectedObject( map ) {
        var objects = map.selectedObjects;
        for ( var idx = 0; idx < objects.length; idx++ ) {
            if ( objects[idx].selected.length > 0 && objects[idx].layer.editable && objects[idx].layer.id === "editable" ) {
                return {
                    layer: objects[idx].layer,
                    feature: objects[idx].selected[0]
                };
            }
        }
    }
    
    /*
     * linkCreateButton: encargado de crear los controles de creacion de poligonos nuevos para la capa de editable.
     * @param {Object} map
     * @param {String} domId
     * @param {int} shapeType
     * @returns {undefined}
     */
    function linkCreateButton(map, domId, shapeType) {
        try {
          on(dom.byId(domId), "click", function() {
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            query("#" + domId).addClass(stylePressed);
            map.selectObjects([]);
            var createController = new BasicCreateController(shapeType, {}, {finishOnSingleClick: true});
            
            createController.onObjectCreated = function() {
                BasicCreateController.prototype.onObjectCreated.apply(this, arguments);
                query(".pressed").removeClass(stylePressed);
                selectedFeature = arguments[2];
                SelectedFeature=selectedFeature;
                /*
                 * filtroGeometria: se encarga de filtrar los poligonos dentro del area de selectedFeature.
                 */
                filtroGeometria(SelectedFeature.geometry);
                var period = periodFilterNode.selectedOptions[0].value;
                var cultivo = cultivoFilterNode.selectedOptions[0].value;
                try {
                    TablasGraficas.actualizarGraficaUbicacionDatos(0, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Tipo de Cultivo", 2);
                    TablasGraficas.actualizarGraficaUbicacionDatos(1, metaDatos,SelectedFeature.geometry, period, cultivo, "piechart", "Municipios", 9);
                    TablasGraficas.actualizarGraficaLabelValorUbicacionDatos(2, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Area Apoyada", 2, 15);
                /*
                TablasGraficas.actualizarGrafica(0, metaDatos, selectedFeature.geometry, "columnchart", "Tipo de Cultivos", 2);
                TablasGraficas.actualizarGrafica(1, metaDatos, selectedFeature.geometry, "piechart", "Municipios", 9);
                TablasGraficas.actualizarGraficaLabelValor(2, metaDatos, selectedFeature.geometry, "columnchart", "Area Apoyada", 2, 15);
                    */
                }catch(e) { console.log(e);}
            };
            map.controller = createController;
          });
        }catch(e) {
            console.log("no se creo el control para el boton "+ domId);
            console.log(e);
        }
    }
    
    linkCreateButton(map, "createcirclecenterpoint", ShapeType.CIRCLE_BY_CENTER_POINT);
    
    var periodFilterNode = document.getElementById( "periodFilter" );
    var cultivoFilterNode = document.getElementById( "cultivoFilter" );
    var filtrar = function() {
        var period = periodFilterNode.selectedOptions[0].value;
        var cultivo = cultivoFilterNode.selectedOptions[0].value;
        if(SelectedFeature) {
        try{
            TablasGraficas.actualizarGraficaUbicacionDatos(0, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Tipo de Cultivo", 2);
            TablasGraficas.actualizarGraficaUbicacionDatos(1, metaDatos,SelectedFeature.geometry, period, cultivo, "piechart", "Municipios", 9);
            TablasGraficas.actualizarGraficaLabelValorUbicacionDatos(2, metaDatos,SelectedFeature.geometry, period, cultivo, "columnchart", "Area Apoyada", 2, 15);
        }catch(e) {console.log(e);}
        
            filtroGeometria(SelectedFeature.geometry);
        } else {
            TablasGraficas.actualizarGraficaDatos(0, metaDatos, period, cultivo, "columnchart", "Tipo de Cultivo", 2);
            TablasGraficas.actualizarGraficaDatos(1, metaDatos, period, cultivo, "piechart", "Municipios", 9);
            TablasGraficas.actualizarGraficaLabelValorDatos(2, metaDatos, period, cultivo, "columnchart", "Area Apoyada", 2, 15);
            filtroGeometria(null);
        }
            
        /*parcelas.filter = function(feature) {
          if ( (feature.properties.Municipio === period || period === "Todos" ) &&
               (feature.properties.Cultivo === cultivo || cultivo === "Todos") ){
            return true;
          }
          return false;
        };*/
      };
    periodFilterNode.addEventListener( "change", filtrar);
    cultivoFilterNode.addEventListener( "change", filtrar);
    
    //---------------------------------------------------------------------------------------------
    
    var x=1;
    ["mapas","parcelas","sensores", "delete", "paneles"].forEach(function (div) {
        $("#"+div).click(function () {
            switch(div) {
                case "mapas":
                    map15.visible = true;
                    map16.visible = true;
                    parcelas.visible = false;
                    municipios.visible = false;
                    sensor.visible = false;
                    editable.visible =false;
                    fit(map16.bounds);
                    MostrarOcultar(null, "panelControl");
                    MostrarOcultar(null, "panelTimeSlider");
                    MostrarOcultar(null, "panelComparacion");
                    MostrarOcultar("panelImagenes", null);
                    break;
                case "parcelas":
                    map15.visible = false;
                    map16.visible = false;
                    parcelas.visible = true;
                    municipios.visible = true;
                    sensor.visible = false;
                    editable.visible =true;
                    var coordenadas = [-106.1467383809857,1,26.30833317349467, 5];
                    fit(null, coordenadas);
                    MostrarOcultar("panelControl","panelTimeSlider");
                    MostrarOcultar(null, "panelComparacion");
                    MostrarOcultar(null, "panelImagenes");
                    break;
                case "sensores":
                    map15.visible = false;
                    map16.visible = false;
                    parcelas.visible = false;
                    municipios.visible = false;
                    sensor.visible = true;
                    editable.visible =false;
                    fit(sensor.bounds);
                    MostrarOcultar("panelTimeSlider","panelControl");
                    MostrarOcultar("panelComparacion", null);
                    MostrarOcultar(null, "panelImagenes");
                    break;
                case "delete":
                    try {
                        editable.model.remove(x);
                        x++;
                        SelectedFeature = {geometry:null};
                        var period = periodFilterNode.selectedOptions[0].value;
                        var cultivo = cultivoFilterNode.selectedOptions[0].value;
                        TablasGraficas.actualizarGraficaDatos(0, metaDatos, period, cultivo, "columnchart", "Tipo de Cultivo", 2);
                        TablasGraficas.actualizarGraficaDatos(1, metaDatos, period, cultivo, "piechart", "Municipios", 9);
                        TablasGraficas.actualizarGraficaLabelValorDatos(2, metaDatos, period, cultivo, "columnchart", "Area Apoyada", 2, 15);
                    }catch(e) {
                        console.log("no borrado");
                        console.log(e);
                    }
                    deleteFilters();
                    break;
                case "paneles":
                    if(panelVisible) {
                        MostrarOcultar(null, "panelControl");
                        panelVisible=false;
                    } else {
                        MostrarOcultar("panelControl", null);
                        panelVisible = true;
                    }
                    break;
            }
        });
    });
    
    var timeSlider;
    var layers = {};

    var startTime = new Date(2017, 5, 1).getTime() / 1000;
    var endTime = new Date(2017, 5, 30).getTime() / 1000;
    
    timeSlider = new TimeSlider("timeslider");
    timeSlider.setValidRange(startTime, endTime, 0, 100);
    
    var self = this;

    function handleSelectionEvent(event) {
        for(var i in event.selectionChanges) {
            var selection = event.selectionChanges[i];
            if (selection.layer.label === "Sensors") {
                for(var j in selection.selected) {
                    var selectedSensor = selection.selected[j];
                    displayTimeLineForSensor(selectedSensor);
                    //console.log("selected") ;
                    //console.log(selectedSensor);
                    var e = selectedSensor.properties.ESTACION;
                    TablasGraficas.graficasSensores(tablaSensores,e );
                }
                for(var k in selection.deselected) {
                    var deselected = selection.deselected[k];
                    clearTimeLine(deselected.id);
                    TablasGraficas.graficasSensores(tablaSensores,"total" );
                }
            }
        }
    };
    
    function clearTimeLine(sensorId) {
        if(layers[""+sensorId])
            timeSlider.layerTree.findLayerTreeNodeById(""+sensorId).visible = false;
    };
    //displayTime = displayTimeLineForSensor;
    function displayTimeLineForSensor(sensor) {
          var sensorId = sensor.id;
          if(!layers[""+sensorId]){
            layers[""+sensorId] = TimeLineLayer.createLayer(sensor, SensorColorService);
            timeSlider.layerTree.addChild(layers[""+sensorId], "top");
          }
          layers[""+sensorId].visible = true;

          if (!!layers[""+sensorId].bounds) {
            /**
             * LayerTree Children visitor used to fit the bounds ont the bounds of visible layers.
             * @type {{foundLayer: null, visitLayer: findQueryableLayerVisitor.visitLayer, visitLayerGroup: findQueryableLayerVisitor.visitLayerGroup}}
             */
            var visibleLayerVisitor = {
              bounds:{},
              visitLayer: function(layer) {
                if(layer.visible === true) {
                  this.bounds.setTo3DUnion(layer.bounds);
                }
                return LayerTreeVisitor.ReturnValue.CONTINUE;
              },
              visitLayerGroup: function(layerGroup) {
                layerGroup.visitChildren(this, LayerTreeNode.VisitOrder.TOP_DOWN);
              }
            };
            visibleLayerVisitor.bounds = layers["" + sensorId].bounds.copy();
            timeSlider.layerTree.visitChildren(visibleLayerVisitor, LayerTreeNode.VisitOrder.TOP_DOWN);
            try{
                timeSlider.mapNavigator.fit(
                {
                  bounds: visibleLayerVisitor.bounds,
                  animation: {duration: 10000},
                  allowWarpXYAxis: true,
                  fitMargin: "5%"
                });
            }catch(e) {
                console.log(e);
            }
          }
    };
        /*
    * Se encarga de filtrar las figuras seleccionadas por medio de un poligono
    * @param {Object} geometry
    * @returns {undefined}
    */
    function filtroGeometria(geometry) {
    /*
     * calcula el centro del poligono que en nuestro caso siempre es un circulo creado desde un centro, calcula tambien el radio
     * y un rango de alcance, maxX y maxY
     */
    deleteFilters();
    if(geometry) {
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var radio = h/2;
    
        
        if (parcelas.visible) {
        /*
         * el filtro de encargara de mostrar solo los polignos dentro del rango del circulo dibujuado
         */
            parcelas.filter = function (feature) {
                var fx = feature.geometry.x? feature.geometry.x: feature.geometry.focusPoint.x;
                var fy = feature.geometry.y? feature.geometry.y: feature.geometry.focusPoint.y;
                var period = periodFilterNode.selectedOptions[0].value;
                var cultivo = cultivoFilterNode.selectedOptions[0].value;
                var distancia = Math.sqrt(Math.pow(x-fx,2) + Math.pow(y-fy,2));
                if(distancia <= radio && ( (feature.properties.Municipio === period || period === "Todos" ) &&
               (feature.properties.Cultivo === cultivo || cultivo === "Todos") ) )
                    return true;
                else
                    return false;
            };
        }
    } else {
        parcelas.filter = function (feature) {
                var period = periodFilterNode.selectedOptions[0].value;
                var cultivo = cultivoFilterNode.selectedOptions[0].value;
                if( (feature.properties.Municipio === period || period === "Todos" ) &&
               (feature.properties.Cultivo === cultivo || cultivo === "Todos") )
                    return true;
                else
                    return false;
            };
    }
    }
});

/*
 * 
 * @param {String} I
 * @param {String} O
 * @returns {undefined}
 */
function MostrarOcultar(I,O) {
    $("#"+O).fadeOut("slow");
    $("#"+I).fadeIn("slow");
}

/*
 * 
 * @returns {undefined}
 */
function deleteFilters() {
    parcelas.filter = null;
    sensor.filter = null;
    //precipitaciones.filter = null;
}




/*
 * @param {String} label
 * @returns {undefined}
 */
function filtrarTexto(label) {
    deleteFilters();
    if (parcelas.visible) {
        parcelas.filter = function (feature) {
            switch(label) {
                case feature.properties.Municipio: return true;
                case feature.properties.Cultivo: return true;
                case feature.properties.Ciclo: return true;
                case "Todos": return true;
                default: return false;
            }
        };
    }
}

