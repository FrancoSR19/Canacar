define(
    [
      "dojo/_base/declare",
      "luciad/view/feature/FeaturePainter",
      'samplecommon/IconFactory',
      "luciad/view/style/PointLabelPosition"
    ], function (declare, FeaturePainter, IconFactory, PointLabelPosition) {

      function SensorPainter(SensorColorService) {

        this._sensorColorService = SensorColorService;
        var size = 64;
        this._clusterStyle = {
          draped: true,
          width: size + "px",
          height: size + "px",
          image: IconFactory.circle({
            width: size,
            height: size,
            fill: "rgba(255, 255, 255,1)",
            stroke: "rgba(0, 78, 146, 1)",
            strokeWidth: size / 8
          }),
          zOrder: 3
        };

        this._activeStyle = {
          url: IconFactory.circle( {
            width: 64,
            height: 64,
            stroke: "rgba(255,255,255,1)",
            fill: "rgba(71,255,116,0.9)",
            strokeWidth: 3,
            zOrder:1
          } )
        };

        this._nonActiveStyle = {
          image: IconFactory.circle( {
            width: 64,
            height: 64,
            stroke: "rgba(255,255,255,1)",
            fill: "rgba(255,0,0,0.5)",
            strokeWidth: 3,
            zOrder:1
          } )
        };
        this._labelStyle = {
          positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        this._labelClusterStyle = {
          positions: (PointLabelPosition.CENTER),
          priority : 1
        };


      }

      SensorPainter.prototype = new FeaturePainter();

      SensorPainter.prototype.paintBody = function (geocanvas, feature, shape, layer, map, paintState) {
        var color = this._sensorColorService.getSensorColor(feature.id);
        if(!color || color.length<1){
          color = [120,160,210];
        }

        if(!feature.properties.clusteredElements){
          if(paintState.selected){
            var style = {
              url: IconFactory.circle( {
                width: 64,
                height: 64,
                stroke: "rgba(255,255,255,1)",
                fill: "rgba("+color[0]+","+color[1]+","+color[2]+",0.9)",
                strokeWidth: 3,
                zOrder:1
              } )
            };
            var size = "40px";
            style.width = size;
            style.height = size;

            geocanvas.drawIcon( shape, style );
          }
          else{
            var style = feature.properties.isActive ? this._activeStyle : this._nonActiveStyle;
            var size = "20px";

            style.width = size;
            style.height = size;

            geocanvas.drawIcon( shape, style );
          }

        }
        else {
          var style = this._clusterStyle;
          style.width = "40px";
          style.height = "40px";
          geocanvas.drawIcon( shape, style );
        }
      };

      SensorPainter.prototype.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
        if(!feature.properties.clusteredElements){

          var consumerName = feature.properties.ESTACION;
          var active = feature.properties.isActive ? "OK": "NOK";
          if(paintState.selected){
            var labelTemplate = '<div class="labelwrapper">' +
                                '<div class="sensorLabel blueColorLabel">' +
                                '<div class="theader">' +
                                '<div class="leftTick blueColorTick"></div>' +
                                '<div class="rightTick blueColorTick"></div>' +
                                '<div class="name">'+consumerName+'</div>' +
                                '</div>' +
                                '<div class="type">CLAVE : '+feature.properties.CLAVE+'</div>' +
                                '<div class="type">ESTADO : '+feature.properties.ESTADO+'</div>' +
                                '<div class="type">SUMA : '+feature.properties.SUMA+'</div>' +
                                '</div>' +
                                '</div>';
            labelcanvas.drawLabel( labelTemplate, shape, this._labelStyle );
          }
          else{
            var labelText = '<div class="name blueColorTick theader sensorLabel blueColorLabel">' + consumerName + '</div>';
            labelcanvas.drawLabel( labelText, shape, this._labelStyle );
          }
        }else{
          labelcanvas.drawLabel("<span style='color: rgb(0, 78, 146);'>" + feature.properties.clusteredElements.length + "</span>", shape.focusPoint, {
            positions: PointLabelPosition.CENTER
          });
          // var labelText = '<div class="label_sensor_cluster">' + feature.properties.clusteredElements.length + '</div>';
          // labelcanvas.drawLabel( labelText, shape, this._labelClusterStyle );
        }
      };

      return SensorPainter;
    }
);
