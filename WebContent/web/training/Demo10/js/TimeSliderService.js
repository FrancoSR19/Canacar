define(
    [
      'angular',
      'lodash',
      "./time/TimeSlider",
      "luciad/view/LayerTreeVisitor",
      "luciad/view/LayerTreeNode",
      "./TimeLineLayer",
      "dojo/dom"
    ],
    function (angular,_,TimeSlider, LayerTreeVisitor, LayerTreeNode, TimeLineLayer) {
      'use strict';
      var timeSlider;
      var layers = {};

      var startTime = new Date(2017, 5, 1).getTime() / 1000;
      var endTime = new Date(2017, 5, 30).getTime() / 1000;

      /**
       * The time slider service provides visualization of overlay contents in the map.
       * @name MapService
       * @class
       */
      function TimeSliderService(MapService, LayerFactory) {
        var self = this;

        var handleSelectionEvent = function (event) {
          angular.forEach(event.selectionChanges, function (selection) {
            if (selection.layer.label === "Sensors") {
              angular.forEach(selection.selected, function (selectedSensor) {
                self.displayTimeLineForSensor(selectedSensor);
              });
              angular.forEach(selection.deselected, function (deselected) {
                self.clearTimeLine(deselected.id);
              });
            }
          });
        };

        MapService.registerSelectionListener(handleSelectionEvent, self);

        TimeSliderService.prototype.createTimeSlider = function (domElementId) {
          timeSlider = new TimeSlider(domElementId);
          timeSlider.setValidRange(startTime, endTime, 0, 300);
          return timeSlider;
        };

        TimeSliderService.prototype.displayTimeLineForSensor = function (sensor) {
          var sensorId = sensor.id;
          if(!layers[""+sensorId]){
            layers[""+sensorId] = TimeLineLayer.createLayer(sensor);
            timeSlider.layerTree.addChild(layers[""+sensorId], "top");
          }
          layers[""+sensorId].visible = true;

          if (!!layers[""+sensorId].bounds) {
            /**
             * LayerTree Children visitor used to fit the bounds ont the bounds of visible layers.
             * @type {{foundLayer: null, visitLayer: findQueryableLayerVisitor.visitLayer, visitLayerGroup: findQueryableLayerVisitor.visitLayerGroup}}
             */
            var visibleLayerVisitor = {
              bounds:{},
              visitLayer: function(layer) {
                if(layer.visible === true) {
                  this.bounds.setTo3DUnion(layer.bounds);
                }
                return LayerTreeVisitor.ReturnValue.CONTINUE;
              },
              visitLayerGroup: function(layerGroup) {
                layerGroup.visitChildren(this, LayerTreeNode.VisitOrder.TOP_DOWN);
              }
            };
            visibleLayerVisitor.bounds = layers["" + sensorId].bounds.copy();
            timeSlider.layerTree.visitChildren(visibleLayerVisitor, LayerTreeNode.VisitOrder.TOP_DOWN);
            timeSlider.mapNavigator.fit(
                {
                  bounds: visibleLayerVisitor.bounds,
                  animation: {duration: 10000},
                  allowWarpXYAxis: true,
                  fitMargin: "5%"
                });
          }
        };
        TimeSliderService.prototype.clearTimeLine = function (sensorId) {
          timeSlider.layerTree.findLayerTreeNodeById(""+sensorId).visible = false;
        };
      }
      return ['mapService', 'layerFactory', TimeSliderService];
    }
);
