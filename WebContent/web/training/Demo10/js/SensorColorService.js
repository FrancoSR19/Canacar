define(
    [
      'lodash'
    ],
    function (_) {
      'use strict';

      /**
       * The SensorColorService controls the SensorColor on the timeline.
       * @name MapService
       * @class
       */
      var self = {};
      try{
        self.featureColor = {};
    }catch(e){
        console.log(e);
    }
        self.colorList = [
          [255, 100, 100],
          [100, 255, 100],
          [100, 100, 255],
          [255, 255, 100],
          [255, 100, 255],
          [100, 255, 255],
          [255,100,0],
          [0,255,0],
          [0,100,255],
          [255,0,100],
          [100,255,0],
          [100,0,255],
          [255,0,0],
          [0,255,100],
          [0,0,255]
        ];
        self.colorIndex = 0;
        function generateColor() {
          return self.colorList[self.colorIndex++ % self.colorList.length];
        }
        return {
        

        getSensorColor: function (sensorId) {
          return self.featureColor[sensorId];
        },
        registerForMap: function (map) {
          var handleSelectionEvent = function (event) {
            _.forEach(event.selectionChanges, function (selection) {
              if (selection.layer.label === "Sensors") {
                _.forEach(selection.selected, function (selectedSensor) {
                  if(!self.featureColor[selectedSensor.id]) {
                    self.featureColor[selectedSensor.id] = generateColor();
                  }
                });
              }
            });
          };
          map.on("SelectionChanged", handleSelectionEvent, self);
        }
      };

    }
);
