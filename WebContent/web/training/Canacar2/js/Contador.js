/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([],function () {
    function contadorPuntos(puntos, Capas) {
        //var puntos = Puntos.model.query().array;
        var np = puntos.length, nc = Capas.length;;
        
        console.log("Comenzar conteo");
        try{
        for(var i=0; i<np; i++) {
            var punto = puntos[i].shape;
            for(var j=0; j<nc;j++) {
                var capaActual = Capas[j];
                var boundCapaActual = capaActual.bounds;
                if(boundCapaActual) {
                    if(contains2DBounds(punto, boundCapaActual)) {
                        var features = capaActual.model.query().array;
                        var nf = features.length;
                        //feature.properties.Incidentes = 0;
                        for(var k=0;k<nf;k++) {
                            var feature = features[k];
                            if(!feature.properties.Incidentes){
                                feature.properties.Incidentes = 0;
                            }
                            var bounds = feature.geometry.bounds;
                            if(contains2DBounds(punto, bounds)) {
                                feature.properties.Incidentes ++;
                                break;
                            }
                            capaActual.model.put(feature);
                            
                        }//FIN DEL DO DE FEATURES

                    }
                }
            }//FIN DEL FOR DE LAS CAPAS
        }// FIN DEL FOR POR PUNTOS
        }catch(e) {
            console.log(e);
        }
    }
    
    function contains2DBounds(point, bounds) {
        try{
        var px = point.x, py = point.y;
        var x1 = bounds.x, y1 = bounds.y;
        var h = bounds.height, w = bounds.width;
        var x2 = x1 + h, y2 = y1 + w;
        //console.log("px "+px+" py "+py+" x2 "+x2+" y2 "+y2);
        if((px>x1 && px<x2) && (py>y1 && py<y2)) {
            return true;
        } else {
            return false;
        }
        } catch(e) {
            return false;
        }
    }
    
    function reestartLayers(Layers) {
        var nc = Layers.length;
        for(var i=0;i<nc;i++) {
            var layer = Layers[i];
            var features = layer.model.query().array;
            for(var j=0;j<features.length;j++) {
                var feature = features[j];
                feature.properties.Incidentes = 0;
                Layers[i].model.put(feature);
            }
        }
                    
    }
    
    function filtrarConteo(Punto, Capas) {
        var nc = Capas.length;;
        
        //console.log("Comenzar conteo");
        try{
            var punto = Punto.shape;
            for(var j=0; j<nc;j++) {
                var capaActual = Capas[j];
                var boundCapaActual = capaActual.bounds;
                if(boundCapaActual) {
                    if(contains2DBounds(punto, boundCapaActual)) {
                        var features = capaActual.model.query().array;
                        var nf = features.length;
                        //feature.properties.Incidentes = 0;
                        for(var k=0;k<nf;k++) {
                            var feature = features[k];
                            if(!feature.properties.Incidentes){
                                feature.properties.Incidentes = 0;
                            }
                            var bounds = feature.geometry.bounds;
                            if(contains2DBounds(punto, bounds)) {
                                feature.properties.Incidentes ++;
                                break;
                            }
                            capaActual.model.put(feature);
                            
                        }//FIN DEL DO DE FEATURES

                    }
                }
            }//FIN DEL FOR DE LAS CAPAS
        }catch(e) {
            console.log(e);
        }
    }
    
    function contadorMunicipios(puntos, Capas) {
        var nc = Capas.length;
        var np = puntos.length;
        for(var i=0;i<nc;i++) {
            var layer = Capas[i];
            var features = layer.model.query().array;
            for(var j=0;j<features.length;j++) {
                var feature = features[j];
                var bounds = feature.geometry.bounds;
                for(var k=0;k<np;k++) {
                    var punto = puntos[k].shape;
                    if(!feature.properties.Incidentes){
                        feature.properties.Incidentes = 0;
                    }
                    if(contains2DBounds(punto, bounds)) {
                        feature.properties.Incidentes ++;
                    }
                }
                Capas[i].model.put(feature);
            }
        }
                    
    }
    
    return {
        contadorPuntos: contadorPuntos,
        reestartLayers: reestartLayers,
        filtrarConteo: filtrarConteo,
        contadorMunicipios: contadorMunicipios
    };
});
