/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    
    function getNewFeatures(newFeatures, oldFeatures) {
        var nL = newFeatures.length, oL = oldFeatures.length;
        if(nL === oL) {
            alert("No hay nuevos incidentes");
            return [];
        } else {
            if(oL > nL) {
                var aux = oldFeatures;
                oldFeatures = newFeatures;
                newFeatures = aux;
                aux = null;
                nL = newFeatures.length;
                oL = oldFeatures.length;
            }
            var features = [], j=0;
            
            for(var i=oL; i<nL; i++) {
                features[j] = newFeatures[i];
                j++;
            }
            
            return features;
        }
    }
    
    function updateFeatures(newFeatures,oldFeatures) {
        
    }
    
    
    return {
        getNewFeatures: getNewFeatures,
        updateFeatures: updateFeatures
    };
    
});
