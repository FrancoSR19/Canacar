/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/model/store/UrlStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "luciad/view/feature/FeaturePainter",
    "luciad/model/codec/GeoJsonCodec",
    "luciad/shape/ShapeFactory",
    "luciad/view/style/PointLabelPosition"
], function(UrlStore,FeatureModel, FeatureLayer,FeaturePainter,GeoJsonCodec, ShapeFactory,
    PointLabelPosition) {
    
    function crearCapa(reference,url, label, selectable)
    {
        var id= Array(32);
        var URL = new UrlStore({
            target: url, 
            codec:new GeoJsonCodec({generateIDs: true})
        });
        var modelo = new FeatureModel(URL, {
            reference: reference
        });
        var painter = new FeaturePainter();
        var detailLevels = [1 / 3000000, // show US routes
                        1 / 1500000, // show state routes
                        1 / 750000,
                        1 / 350000,
                        1 / 30000];
        painter.getDetailLevelScales = function() {
            return detailLevels;
        };
        painter.paintBody = getPaintBody();
        painter.paintLabel = function(labelCanvas, feature, shape, layer, map, paintState)
        {
            var labelStyle = {group: 'DECLUTTERED',
                padding: 3,
                positions: (PointLabelPosition.CENTER )
            };
            if(feature.properties.name)
                label= "<span class='$Label' style='color: $color'>" + feature.properties.name + "</span>";
            else
                label = "<span class='$Label' style='color: $color'>" + feature.id + "</span>";
            if(paintState.selected )
                labelCanvas.drawLabel(label.replace("$color", "rgb(200,190,200")
                    .replace("$Label", "LabelSelected"),shape.focusPoint, labelStyle);
            else
                labelCanvas.drawLabel(label.replace("$color", "rgb(252,255,255")
                    .replace("$Label", "Label2"),shape.focusPoint, labelStyle);
            
        };
        //dLang.mixin(layerOptions, {painter: painter});
        return new FeatureLayer(modelo, {
            label: label,
            selectable: selectable,
            painter: painter}
        );
    }
    
    function getPaintBody() {
        return function(geocanvas, feature, shape, layer, map, paintState) {
            var shape3d = ShapeFactory.createExtrudedShape(shape.reference, shape, 0, 80000);
            var colorFill = "rgba(50,80,170, 0.5)", colorLine = "rgb(50,80,170)";
            var color = {fill: colorFill, stroke: colorLine};
            color = getColor(feature.id);
            var style = {
                fill: {
                    color: (paintState.selected ? "rgba(43, 43, 43, 0.6)" : color.fill)
                },
                stroke: {
                    color: (paintState.selected ? "rgb(255,255,255)" : color.stroke),
                    width: 1
                }
            };
            //geocanvas.drawShape((paintState.selected ? shape3d : shape), style);
            geocanvas.drawShape(shape, style);
        };
    }
    
    function getColor(id) {
        switch(id) {
            case 0: return {fill:"rgba(46,254,100, 0.4)", stroke:"rgb(46,254,100)"}; break;
            case 1: return {fill:"rgba(255,41,130,0.4)", stroke:"rgb(255,41,130)"}; break;
            case 2: return {fill:"rgba(255,41,231,0.4)", stroke:"rgb(255,41,231)"}; break;
            case 3: return {fill:"rgba(255,41,255,0.4)", stroke:"rgb(255,41,255)"}; break;
            case 4: return {fill:"rgba(191,41,255,0.4)", stroke:"rgb(191,41,255)"}; break;
            case 5: return {fill:"rgba(94,41,255,0.4)", stroke:"rgb(94,41,255)"}; break;
            case 6: return {fill:"rgba(41,152,255,0.4)", stroke:"rgb(41,152,255)"}; break;
            case 7: return {fill:"rgba(41,234,255,0.4)", stroke:"rgb(41,234,255)"}; break;
            case 8: return {fill:"rgba(41,152,255,0.4)", stroke:"rgb(41,152,255)"}; break;
            case 9: return {fill:"rgba(46,254,100, 0.4)", stroke:"rgb(46,254,100)"}; break;
            case 10: return {fill:"rgba(41,255,255,0.4)", stroke:"rgb(41,255,255)"}; break;
            case 11: return {fill:"rgba(41,255,213,0.4)", stroke:"rgb(41,255,213)"}; break;
            case 12: return {fill:"rgba(41,255,145,0.4)", stroke:"rgb(41,255,145)"}; break;
            case 13: return {fill:"rgba(41,255,91,0.4)", stroke:"rgb(41,255,91)"}; break;
            case 14: return {fill:"rgba(41,255,41,0.4)", stroke:"rgb(41,255,41)"}; break;
            case 15: return {fill:"rgba(91,255,41,0.4)", stroke:"rgb(91,255,41)"}; break;
            case 16: return {fill:"rgba(159,255,41,0.4)", stroke:"rgb(159,255,41)"}; break;
            case 17: return {fill:"rgba(199,255,41,0.4)", stroke:"rgb(199,255,41)"}; break;
            case 18: return {fill:"rgba(234,255,41,0.4)", stroke:"rgb(234,255,41)"}; break;
            case 19: return {fill:"rgba(255,255,41,0.4)", stroke:"rgb(255,255,41)"}; break;
            case 20: return {fill:"rgba(255,206,41,0.4)", stroke:"rgb(255,206,41)"}; break;
            case 21: return {fill:"rgba(255,166,41,0.4)", stroke:"rgb(255,166,41)"}; break;
            case 22: return {fill:"rgba(255,127,41,0.4)", stroke:"rgb(255,127,41)"}; break;
            case 23: return {fill:"rgba(255,255,41,0.4)", stroke:"rgb(255,255,41)"}; break;
            case 24: return {fill:"rgba(244,250,88, 0.4)", stroke:"rgb(244,250,88)"}; break;
            case 25: return {fill:"rgba(255,80,41,0.4)", stroke:"rgb(255,80,41)"}; break;
            case 26: return {fill:"rgba(255,41,41,0.4)", stroke:"rgb(255,41,41)"}; break;
            case 27: return {fill:"rgba(255,41,130,0.4)", stroke:"rgb(255,41,130)"}; break;
            case 28: return {fill:"rgba(255,41,231,0.4)", stroke:"rgb(255,41,231)"}; break;
            case 29: return {fill:"rgba(255,41,255,0.4)", stroke:"rgb(255,41,255)"}; break;
            case 30: return {fill:"rgba(191,41,255,0.4)", stroke:"rgb(191,41,255)"}; break;
            case 31: return {fill:"rgba(94,41,255,0.4)", stroke:"rgb(94,41,255)"}; break;
            default: return {fill:"rgba(50,80,170, 0.4)", stroke:"rgb(50,80,170)"}; break;
        }
    }
    
    return {
        crearCapaEstados: crearCapa
    };
});

