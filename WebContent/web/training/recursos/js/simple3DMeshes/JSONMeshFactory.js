define(['luciad/util/Promise',
        "luciad/geometry/mesh/MeshFactory",
        'luciad/error/ProgrammingError'],

    function(Promise, MeshFactory, ProgrammingError) {

      /**
       * Creates a mesh from a simple JSON formatted file.
       *
       * If you have LuciadLightspeed,
       * the sample in samples.lightspeed.meshencoder demonstrates how you can convert LuciadLightspeed shapes to this format.
       *
       * An example of the format can be found in the resources directory of the samples under "samples/resources/data/gltf/shape/shape.json".
       *
       * An minimal example of the contents of such a JSON file:
       *
       * <pre class="code">
       *   {
       *     "location": [6, 50, 100],
       *     "positions": [0, 0, 0,   //Vertex 0 at position 0,0,0
       *                   10, 0, 0,  //Vertex 1 at position 10,0,0
       *                   0, 10, 0], //Vertex 2 at position 0,10,0
       *     "indices": [0, 1, 2]     //Triangle formed by vertices with index 0 - 1 - 2
       *   }
       * </pre>
       *
       * @param {String} url a URL to the JSON resource file to be decoded
       *
       * @returns A promise that resolves into a mesh
       */
      var createMeshFromUrl = function(url) {
        return new Promise(function(resolve, reject) {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', url);
          xhr.onload = function() {
            if (this.status >= 200 && this.status < 300) {
              resolve(createMeshFromString(xhr.response));
            } else {
              reject({
                status: this.status,
                statusText: xhr.statusText
              });
            }
          };
          xhr.onerror = function() {
            reject({
              status: this.status,
              statusText: xhr.statusText
            });
          };
          xhr.send();
        });
      };

      /**
       * Creates a mesh from a simple JSON formatted mesh string.
       *
       * If you have LuciadLightspeed,
       * the sample in samples.lightspeed.meshencoder demonstrates how you can convert LuciadLightspeed shapes to this format.
       *
       * An example of the format can be found in the resources directory of the samples under "samples/resources/data/gltf/shape/shape.json".
       *
       * A minimal example of the contents of such a JSON file:
       *
       * <pre class="code">
       *   {
       *     "location": [6, 50, 100],
       *     "positions": [0, 0, 0,   //Vertex 0 at position 0,0,0
       *                   10, 0, 0,  //Vertex 1 at position 10,0,0
       *                   0, 10, 0], //Vertex 2 at position 0,10,0
       *     "indices": [0, 1, 2]     //Triangle formed by vertices with index 0 - 1 - 2
       *   }
       * </pre>
       *

       * @param {String} meshString the string containing the mesh
       **/
      var createMeshFromString = function(meshString) {
        var mesh = JSON.parse(meshString);
        if (!mesh.positions || mesh.positions.length == 0) {
          throw new ProgrammingError("Positions are mandatory");
        }
        if (!mesh.indices || mesh.indices.length == 0) {
          throw new ProgrammingError("Indices are mandatory");
        }
        var options = {};
        if (mesh.colors && mesh.colors.length != 0) {
          options.colors = mesh.colors;
        }
        return MeshFactory.create3DMesh(mesh.positions, mesh.indices, options);
      };

      return {
        createMeshFromUrl: createMeshFromUrl,
        createMeshFromString: createMeshFromString
      };
    });