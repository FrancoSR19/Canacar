define([
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/LayerType",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/view/feature/ShapeProvider",
  "luciad/model/tileset/FusionTileSetModel"
], function (ReferenceProvider, ShapeFactory, RasterTileSetLayer, FeatureLayer, LayerType, FeatureModel, UrlStore,
             GeoJsonCodec, ShapeProvider, FusionTileSetModel) {

  function createRealElevationLayer() {
    var elevationReference = ReferenceProvider.getReference("EPSG:4326");
    var elevationModel = new FusionTileSetModel({
      url: "https://sampleservices.luciad.com/lts",
      coverageId: "world_elevation_6714a770-860b-4878-90c9-ab386a4bae0f",
      reference: elevationReference,
      bounds: ShapeFactory.createBounds(elevationReference, [-180, 360, -90, 180]),
      level0Columns: 4,
      level0Rows: 2,
      tileWidth: 81,
      tileHeight: 81,
      dataType: FusionTileSetModel.DataType.ELEVATION,
      samplingMode: FusionTileSetModel.SamplingMode.POINT

    });

    return new RasterTileSetLayer(elevationModel, {
      label: "Real elevation",
      layerType: LayerType.BASE
    });
  }


  return {
    createRealElevationLayer: createRealElevationLayer
  };

});
