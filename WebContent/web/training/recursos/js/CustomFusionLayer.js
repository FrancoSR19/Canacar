/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    
  "dojo/request",
  "luciad/model/feature/Feature",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/model/store/UrlStore",
  "luciad/model/tileset/RasterDataType",
  "luciad/model/tileset/RasterSamplingMode",
  "luciad/model/tileset/BingMapsTileSetModel",
  "luciad/model/tileset/FusionTileSetModel",
  "luciad/model/tileset/UrlTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/format/LonLatPointFormat",
  "luciad/shape/ShapeFactory",
  "luciad/util/Promise",
  "luciad/view/controller/EditController",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/grid/GridLayer",
  "luciad/view/grid/LonLatGrid",
  "luciad/view/LayerType",
  "luciad/view/style/PinEndPosition",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/feature/transformation/ClusteringTransformer",
  "samples/common/IconFactory",
  "samples/common/ShapePainter",
  
  "./LuciadFusionDataLoader",
  "./WFSDataLoader",
  "./WMSDataLoader",
  "./WMTSDataLoader",
], function (dRequest, Feature, FeatureModel, MemoryStore, UrlStore, RasterDataType, RasterSamplingMode,
            BingMapsTileSetModel, FusionTileSetModel, UrlTileSetModel, ReferenceProvider, LonLatPointFormat,
            ShapeFactory, Promise, EditController, FeatureLayer, FeaturePainter, GridLayer, LonLatGrid, LayerType,
            PinEndPosition, RasterTileSetLayer, ClusteringTransformer, IconFactory, ShapePainter,
            LuciadFusionDataLoader, WFSDataLoader, WMSDataLoader, WMTSDataLoader) {
    
    
    var PUBLIC_LUCIADFUSION_URL = window.location.protocol + "//localhost:8081";
    var publicLuciadFusionServerPromise = null;
    
    function isPublicLuciadFusionServerReachable(url) {
        return publicLuciadFusionServerPromise || (publicLuciadFusionServerPromise = dRequest(PUBLIC_LUCIADFUSION_URL + url ));
    }
    
    //computes the resources directory
    var RESOURCE_DIRECTORY = (function() {
        var rp, url, matches, relsmpl, relpath;
        rp = new RegExp(/^.*\/samples\//);
        url = window.location.href;
        matches = rp.exec(url);
        if (matches) {
            relsmpl = url.replace(matches[0], "");
            matches = relsmpl.match(/\//g);
            relpath = matches.map(function(ignore) {
                return "..";
            }).join("/");
            return relpath + "/resources";
        } else {
            return '/samples/resources';
        }
    }());

    var CONTEXT_MENU_IDS = {
        EDIT_ID: "EDIT_ID",
        DELETE_ID: "DELETE_ID"
    };

    function formatResourceData(data) {

        //Inspect the response
        var resourceSet;
        var resource = data;
        if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' && data.resourceSets.length > 0) {
            resourceSet = data.resourceSets[0];
            if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' &&
                resourceSet.resources.length > 0) {
                resource = resourceSet.resources[0];
            }
        }

        resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;
        return resource;
    }
  
  
    function LTSFusionLayer(reference, url, id, label) {
        var imageryParameters = {
            reference: reference,
            level0Columns: 4,
            level0Rows: 2,
            levelCount: 24,
            bounds: ShapeFactory.createBounds(reference, [-180, 360, -90, 180]),
            url: PUBLIC_LUCIADFUSION_URL + url,//"/ogc/wms/earthimagery2",
            coverageId: id,
            tileWidth: 256,
            tileHeight: 256
        };
        var model = new FusionTileSetModel(imageryParameters);
        var layer =new RasterTileSetLayer(model,
            {label: label, layerType: LayerType.BASE});
        return layer;
    }
    
    function WMSFusionLayer(reference, url, id, label) {
        var imageryParameters = {
            reference: reference,
            bounds: ShapeFactory.createBounds(reference, [-180, 360, -90, 180]),
            url: PUBLIC_LUCIADFUSION_URL + url,//"/ogc/wms/earthimagery2",
            coverageId: id
        };
        var model;
        model = new FusionTileSetModel(imageryParameters);
        //model = new RasterTileSetModel();
        var layer = new RasterTileSetLayer(model,
            {label: label, layerType: LayerType.BASE});
        return layer;
    }
    
    
    
    function createFusionBackgroundLayer() {
        var tileSetReference = ReferenceProvider.getReference("EPSG:4326");
        var url = "http://localhost:8081/lts/earthimageryhdr"; //"/ogc/wmts/earthimageryhdr2";
        var imageryParameters = {
            reference: tileSetReference,
            level0Columns: 4,
            level0Rows: 2,
            levelCount: 24,
            bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
            url: url,//"/ogc/wms/earthimagery2",
            coverageId: "2",//"earthimageryhdr",
            tileWidth: 256,
            tileHeight: 256
        };
        var layer =new RasterTileSetLayer(new FusionTileSetModel(imageryParameters),
            {label: "Earth Layer Fusion", layerType: LayerType.BASE});
        //return isPublicLuciadFusionServerReachable(url).then(function() {
        return layer;
        //});
    }
    
    
    function createFusionElevationLayer() {
        var tileSetReference = ReferenceProvider.getReference("EPSG:4326");
        var url = "/lts/earthelevationdata";
        var elevationParameters = {
            reference: tileSetReference,
            level0Columns: 4,
            level0Rows: 2,
            levelCount: 24,
            bounds: ShapeFactory.createBounds(tileSetReference, [-180, 360, -90, 180]),
            url: PUBLIC_LUCIADFUSION_URL + url,
            coverageId: "3",//"earth_elevation",
            tileWidth: 91,
            tileHeight: 91,
            dataType: FusionTileSetModel.DataType.ELEVATION, //RasterDataType.ELEVATION,
            samplingMode: FusionTileSetModel.SamplingMode.POINT //RasterSamplingMode.POINT
        };
        var layer = new RasterTileSetLayer(new FusionTileSetModel(elevationParameters), {label: "Elevation Fusion "});
        //return isPublicLuciadFusionServerReachable(url).then(function() {
            return layer;
        //});
    }
    
    
    var util = {
        PUBLIC_LUCIADFUSION_URL: PUBLIC_LUCIADFUSION_URL,
      
        createFusionBackgroundLayer: createFusionBackgroundLayer,
        createFusionElevationLayer: createFusionElevationLayer,
        WMSFusionLayer: WMSFusionLayer,
        LTSFusionLayer: LTSFusionLayer
    };
  
  
  return util;
});
