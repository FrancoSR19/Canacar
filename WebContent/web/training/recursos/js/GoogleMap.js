define([
    "luciad/view/google/GoogleLayer",
    "template/sample",
    "luciad/reference/ReferenceProvider",
    "dojo/query",
    "dojo/dom-attr"
    //"dojo/domReady!"
], function(GoogleLayer, templateSample, ReferenceProvider, query, domAttr)
{
	
    function crearMapaGoogle(node, maptype){
        var credentials=false;
        var map= templateSample.makeMap({
                reference: ReferenceProvider.getReference("EPSG:900913")
            }, {includeBackground: false});
            
        function startup(){
            if (credentials){
                var types = ["satellite", "hybrid", "roadmap", "terrain"];
                if(maptype === "alltypes") {
                    for(var type in types) {
                        var googlelayer = new GoogleLayer({mapType: type, visible:true});
                        map.layerTree.addChild(googlelayer, "bottom");
                        query("button[data-maptype*=goog_]").forEach(function(node) {
                            var val = domAttr.get(node, "data-maptype");
                            var type = val.replace("goog_", "");
                            on(node, "click", function() {
                                googlelayer.mapType = type;
                            });
                        });
                    }
                } else {
                    var googlelayer = new GoogleLayer({mapType: maptype, visible:true});
                    map.layerTree.addChild(googlelayer, "bottom");
                    query("button[data-maptype*=goog_]").forEach(function(node) {
                        var val = domAttr.get(node, "data-maptype");
                        var type = val.replace("goog_", "");
                        on(node, "click", function() {
                            googlelayer.mapType = type;
                        });
                    });
                }
            }
            delete window.luciad_samples_googlelayer_startup;
        }
        
        (function(){
            function getParameterByName(name) {
      		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
      		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          		results = regex.exec(location.search);
      		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
            //var key = getParameterByName("key");
            var key = "AIzaSyDNGZMfA3xyQVX076ATsjssz1IDGSSzIyQ";
            var api = "https://maps.googleapis.com/maps/api/js?v=3&callback=luciad_samples_googlelayer_startup";
            if (key){
      		api += "&key=" + key;
      		credentials = true;
            } else {
      		var message = 'Sample cannot be loaded without credentials.<br>You have to pass in your Google Application Key by appending a "key"-parameter to the URL.  e.g. ?key=MYKEY. ';
      		console.log(message);
      		templateSample.error("google", message);
            }

            var scriptTag = document.createElement("script");
            scriptTag.src = api;
            scriptTag.type = "text/javascript";
            document.head.appendChild(scriptTag);
        }());
    
        window.luciad_samples_googlelayer_startup = startup;
        return map;
    }

    return{
        crearMapaGoogle: crearMapaGoogle
    };
});