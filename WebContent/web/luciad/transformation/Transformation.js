define(["../dojo/_base/declare","../reference/ReferenceType","../shape/LLHBounds","../shape/LLHPoint","../shape/XYZBounds","../shape/XYZPoint","../error/NotImplementedError","../util/Lang"],function(a,b,c,d,e,f,g,h){var i=a(null,{constructor:function(){},transform:function(a,c){if("object"!==typeof c||null===c){var e=this.outputReference;if(e&&e.TYPE===b.GEODETIC)c=new d(e);else c=new f(e)}this._forward(a,c);return c},transformBounds:function(a,d){if("undefined"===typeof d||null===d){var f=this.outputReference;if(f&&f.TYPE===b.GEODETIC)d=new c(f);else d=new e(f)}this._forwardBoundsCoords(a,d);return d},_forward:function(a,b){throw new g("Transformation::_inverse. Child needs to implement this method.")},_inverse:function(a,b){throw new g("Transformation::_inverse. Child needs to implement this method.")},_forwardBoundsCoords:function(a,b){throw new g("Transformation::_forwardBoundsCoords. Child needs to implement this method.")},_inverseBoundsCoords:function(a,b){throw new g("Transformation::_inverseBoundsCoords. Child needs to implement this method.")},_getType:function(){throw new g("Transformation::_getType.  Child needs to implement this method")},_getActualTransformation:function(){return this}});h.defineProperty(i.prototype,"inputReference",{enumerable:false,get:function(){return this._inputReference}});h.defineProperty(i.prototype,"outputReference",{enumerable:false,get:function(){return this._outputReference}});h.defineProperty(i.prototype,"inverseTransformation",{enumerable:false,get:function(){throw new g("Child must implement inverseTransformation property")}});return i});