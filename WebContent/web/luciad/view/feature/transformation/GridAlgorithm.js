define(["./Clustered","./ClusteredPoint","../../../shape/XYZBounds","../../../util/Cartesian","../../../util/Map"],function(a,b,c,d,e){var f=Math.pow(2,31)-1;function g(){this.fGrid=new e;this.fGridBounds=null;this.fGridCellWidth=0;this.fGridCellHeight=0;this.fCellCountX=0}g.prototype.cluster=function(a,b,e){var g=this._splitTooSmallPreviousClusters(b,e);this.fGrid.clear();var h=this.getBounds(a);this.fGridBounds=new c(null,[h.x,1.00001*h.width,h.y,1.00001*h.height]);var i=e.clusterSize;var j=e.clusterSize;this.fCellCountX=Math.floor(d.clamp(h.width/i,1,f));var k=Math.floor(d.clamp(h.height/j,1,f));this.fGridCellWidth=this.fGridBounds.width/this.fCellCountX;this.fGridCellHeight=this.fGridBounds.height/k;for(var l=0;l<a.length;l++){var m=a[l];if(null!==m.cluster)continue;this.add(m)}return this.getClusters(g,e)};g.prototype._splitTooSmallPreviousClusters=function(a,b){var c=[],d,e;for(d=0;d<a.length;d++){var f=a[d];if(f.getPointCount()<b.minimumPoints){var g=f.getPoints();for(e=0;e<g.length;e++)g[e].cluster=null}else c.push(f)}return c};g.prototype.getBounds=function(a){var b=new c;if(a.length>0){var d=a[0];b.setTo2D(d.viewLocation.x,0,d.viewLocation.y,0)}for(var e=1;e<a.length;e++){var f=a[e];b.setToIncludePoint2D(f.viewLocation)}return b};g.prototype.add=function(c){var d=this.index(this.gridX(c.viewLocation.x),this.gridY(c.viewLocation.y));var e=this.fGrid.get(d);if(e instanceof a)e.addPoint(c);else if(e instanceof b){var f=new a;this.fGrid.set(d,f);f.addPoint(e);f.addPoint(c)}else this.fGrid.set(d,c)};g.prototype.gridX=function(a){if(0==this.fGridCellWidth)return 0;var b=a-this.fGridBounds.x;return Math.floor(b/this.fGridCellWidth)};g.prototype.gridY=function(a){if(0==this.fGridCellHeight)return 0;var b=a-this.fGridBounds.y;return Math.floor(b/this.fGridCellHeight)};g.prototype.index=function(a,b){return b*this.fCellCountX+a};g.prototype.getClusters=function(b,c){var d=[];this.fGrid.forEach(function(b){var e=b;if(e instanceof a){var f=e;var g=f.getPoints();if(g.length>=c.minimumPoints)d.push(f);else for(var h=0;h<g.length;h++){var i=g[h];i.cluster=null}}});d=d.concat(b);return d};return g});